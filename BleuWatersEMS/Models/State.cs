﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class State
    {
        [Required]
        public int ID { get; set; }
        [Required]
        public string StateName { get; set; }
        [Required]
        public int CountryId { get; set; }
        public virtual Country Countries { get; set; }
    }
}
