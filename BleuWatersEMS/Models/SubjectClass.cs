﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class SubjectClass
    {
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        public int ClassId { get; set;}
        public Class Class { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }

        public int TeacherId { get; set; }
    }
}
