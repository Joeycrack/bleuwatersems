﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Session
    {
        public Session()
        {
            Terms = new HashSet<Term>();
        }

        public int ID { get; set; }
        [Required]
        public string SessionName { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public bool IsCurrentSession { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }

        public ICollection<Term> Terms { get; set; }
    }
}
