﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Country
    {
        public Country()
        {
            States = new HashSet<State>();
        }

        public int ID { get; set; }
        [Required]
        public string CountryName { get; set; }
        public ICollection<State> States { get; set; }

    }
}
