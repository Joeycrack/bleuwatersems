﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class StudentAssessment
    {
        public int ID { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int CAScore { get; set; }
        public int ExamsScore { get; set; }
        public int TenantId { get; set; }
        public int GradeClassId { get; set; }
        public GradeClass GradeClass { get; set; }
        public int ClassId { get; set; }
        public Class Class { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int TermId { get; set; }
        public Term Term { get; set; }
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        public int RecordedBy { get; set; }
        public int? TotalScore { get; set; }
        public int? ScoreGradeId { get; set; }
        public bool Submitted { get; set; }
        public bool Approved { get; set; }
        public string AdminRemarks { get; set; }

    }
}
