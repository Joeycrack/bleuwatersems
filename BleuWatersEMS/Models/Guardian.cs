﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Guardian
    {
        public Guardian()
        {
            Students = new HashSet<Student>();
        }

        public int ID { get; set; }
        public int? SalutationId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string OfficeAddress { get; set; }
        public string PhoneNo { get; set; }
        public string HomeAddress { get; set; }
        public string EmailAddress { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
