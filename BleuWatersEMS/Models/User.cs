﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class User
    {

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ApplicationUserId { get; set; }
        public string MaritalStatus { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber { get; set; }
        public int? CountryId { get; set; }
        public virtual Country Countries { get; set; }
        public string Email { get; set; }
        public int? StateId { get; set; }
        public virtual State States { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsNonAcademicStaff { get; set; }
        public bool IsAcademicStaff { get; set; }
        public bool IsStudent { get; set; }
        public bool IsParent { get; set; }
        public bool IsNewUser { get; set; }
        public bool IsApproved { get; set; }
        public bool IsTeacher { get; set; }
        public int? SalutationID { get; set; }
        public bool Activated { get; set; }
        public virtual Salutation Salutation { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
