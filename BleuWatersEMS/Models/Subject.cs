﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Subject
    {
        public Subject()
        {
            SubjectGradeClasses = new HashSet<SubjectGradeClass>();
            SubjectClasses = new HashSet<SubjectClass>();
            StudentSubjects = new HashSet<StudentSubject>();
        }

        public int ID { get; set; }
        [Required]
        public string subjectName { get; set; }
        [Required]
        public bool Status { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }

        public ICollection<SubjectGradeClass> SubjectGradeClasses { get;set;}
        public ICollection<SubjectClass> SubjectClasses { get; set; }
        public ICollection<StudentSubject> StudentSubjects { get; set; }
    }
}
