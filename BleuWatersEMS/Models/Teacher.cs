﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Teacher
    {

        public Teacher()
        {
            Subjects = new HashSet<Subject>();
            StudentSubjects = new HashSet<StudentSubject>();
        }

        [Required]
        public int ID { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public string ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        public string MaritalSatus { get; set; }
        [Required]
        public bool Status { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<Subject> Subjects { get; set; }
        public ICollection<StudentSubject> StudentSubjects { get; set; }
    }
}
