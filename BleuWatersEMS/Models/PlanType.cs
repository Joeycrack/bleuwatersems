﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class PlanType
    {
        public PlanType()
        {
            PlanTypeInterfaces = new HashSet<PlanTypeInterface>();
            Tenants = new HashSet<Tenant>();
        }
        public int ID { get; set; }
        public string PlanName { get; set; }
        public ICollection<PlanTypeInterface> PlanTypeInterfaces { get; set; }
        public ICollection<Tenant> Tenants { get; set; }
    }
}
