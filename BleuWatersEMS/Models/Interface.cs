﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Interface
    {
        public Interface()
        {
            Children = new HashSet<InterfaceChildren>();
            PlanTypeInterfaces = new HashSet<PlanTypeInterface>();
            RoleInterfaces = new HashSet<RoleInterface>();
        }

        public int ID { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public bool divider { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        [Required]
        public bool SuperAdmin { get; set; }
        public ICollection<InterfaceChildren> Children { get; set; }
        public ICollection<PlanTypeInterface> PlanTypeInterfaces { get; set; }
        public ICollection<RoleInterface> RoleInterfaces { get; set; }

    }
}
