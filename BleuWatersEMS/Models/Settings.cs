﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Settings
    {
        public int ID { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int TermId { get; set; }
        public Term Term { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
