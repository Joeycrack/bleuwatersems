﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Class
    {
        public Class()
        {
            SubjectClasses = new HashSet<SubjectClass>();
           Students = new HashSet<Student>();
        } 

        public int ID { get; set; }
        [Required]
        public string ClassName { get; set; }
        [Required]
        public int GradeClassId { get; set; }
        public GradeClass GradeClass { get; set; }
        
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        [Required]
        public int MaxNumber { get; set; }
        [Required]
        public bool Status { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<SubjectClass> SubjectClasses { get; set; }
        public ICollection<Student> Students { get; set; }

        
    }
}
