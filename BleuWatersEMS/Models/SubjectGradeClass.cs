﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class SubjectGradeClass
    {
        [Required]
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        [Required]
        public int GradeClassId { get; set; }
        public GradeClass GradeClass { get; set;}
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
