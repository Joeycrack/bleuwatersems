﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class StudentSubject
    {
        [Required]
        public int StudentId { get; set; }
        public Student Student { get; set; }
        [Required]
        public int SubjectId { get; set; }
        public Subject Subject { get; set; }
        [Required]
        public int TeacherId { get; set; }
        public Teacher Teacher { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
