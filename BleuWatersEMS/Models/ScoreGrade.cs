﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class ScoreGrade
    {
        public int ID { get; set; }
        public int StartScore { get; set; }
        public int EndScore { get; set; }
        public int TenantId { get; set; }
        public string ScoreGradeName { get; set; }
        public string Remark { get; set; }
    }
}
