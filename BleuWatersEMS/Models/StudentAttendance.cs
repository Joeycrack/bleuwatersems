﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class StudentAttendance
    {
        public int ID { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
        public int GradeClassId { get; set; }
        public GradeClass GradeClass { get; set; }
        public int ClassId { get; set; }
        public Class Class { get; set; }
        public DateTime Date { get; set; }
        public bool Present { get; set; }
        public int TakenBy { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int TermId { get; set; }
        public Term Term { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
