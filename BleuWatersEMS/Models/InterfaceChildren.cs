﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class InterfaceChildren
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string icon { get; set; }
        public int InterfaceId { get; set; }
        public Interface Interface { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public bool SuperAdmin { get; set; }

    }
}
