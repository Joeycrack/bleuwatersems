﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Term
    {
        public int ID { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required]
        public int SessionId { get; set; }
        [Required]
        public string TermName { get; set; }
        [Required]
        public bool IsCurrent { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
    }
}
