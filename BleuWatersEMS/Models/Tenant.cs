﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Tenant
    {
        public Tenant()
        {
            Students = new HashSet<Student>();
            Teachers = new HashSet<Teacher>();
            GradeClasses = new HashSet<GradeClass>();
            Salutations = new HashSet<Salutation>();
            StudentAttendances = new HashSet<StudentAttendance>();
            StudentSubjects = new HashSet<StudentSubject>();
            InterfaceChildrens = new HashSet<InterfaceChildren>();
            PlanTypeInterfaces = new HashSet<PlanTypeInterface>();
            Subjects = new HashSet<Subject>();
            SubjectClasses = new HashSet<SubjectClass>();
            Classes = new HashSet<Class>();
            SubjectGradeClasses = new HashSet<SubjectGradeClass>();
            Sessions = new HashSet<Session>();
            Terms = new HashSet<Term>();
            Guardians = new HashSet<Guardian>();
            Parents = new HashSet<Parent>();
            ApplicationUsers = new HashSet<ApplicationUser>();

        }
        public int ID { get; set; }
        public string SchoolName { get; set; }
        public int CountryId { get; set; }
        public Country Country { get; set; }
        public int StateId { get; set; }
        public State State { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneNumber2 { get; set; }
        public string BadgeUrl { get; set; }
        public string Motto { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public int PlanTypeId { get; set; }
        public PlanType PlanType { get; set; }
        public ICollection<Student> Students { get; set; }
        public ICollection<Teacher> Teachers { get; set; }
        public ICollection<GradeClass> GradeClasses { get; set; }
        public ICollection<Salutation> Salutations { get; set; }
        public ICollection<StudentAttendance> StudentAttendances { get; set; }
        public ICollection<StudentSubject> StudentSubjects { get; set; }
        public ICollection<InterfaceChildren> InterfaceChildrens { get; set; }
        public ICollection<PlanTypeInterface> PlanTypeInterfaces { get; set; }
        public ICollection<Subject> Subjects { get; set; }
        public ICollection<SubjectClass> SubjectClasses { get; set; }
        public ICollection<Class> Classes { get; set; }
        public ICollection<SubjectGradeClass> SubjectGradeClasses { get; set; }
        public ICollection<Session> Sessions { get; set; }
        public ICollection<Term> Terms { get; set; }
        public ICollection<Guardian> Guardians { get; set; }
        public ICollection<Parent> Parents { get; set; }
        public ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}

