﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Parent
    {
        public Parent()
        {
            Students = new HashSet<Student>();
        }

        public int ID { get; set; }
        [Required]
        public string MotherFirstName { get; set; }
        [Required]
        public int FatherSalutationId { get; set; }
        [Required]
        public int MotherSalutationId { get; set; }
        public Salutation Salutation { get; set; }
        [Required]
        public string MotherLastName { get; set; }
        [Required]
        public string FatherFirstName { get; set; }
        [Required]
        public string FatherLastName { get; set; }
        [Required]
        public string FatherOccupation { get; set; }
        [Required]
        public string FatherReligion { get; set; }
        [Required]
        public string MotherReligion { get; set; }
        [Required]
        public int FatherCountryId { get; set; }
        [Required]
        public int MotherCountryId { get; set; }
        public Country Country { get; set; }
        [Required]
        public int MotherStateId { get; set; }
        [Required]
        public int FatherStateId { get; set; }
        public State State { get; set; }
        public string MotherLocalGovt { get; set; }
        public string FatherLocalGovt { get; set; }
        [Required]
        public string FatherPhoneNo { get; set; }
        public string FatherOfficePhoneNo { get; set; }
        [Required]
        public string MotherOccupation { get; set; }
        public string MotherofficePhoneNo { get; set; }
        public string MotherOfficeAddress { get; set; }
        public string FatherOfficeAddress { get; set; }
        [Required]
        public string MotherPhoneNo { get; set; }
        [Required]
        public bool ParentTogether { get; set; }
        public string FatherAddress { get; set; }
        public string MotherAddress { get; set; }
        [Required]
        public string MotherEmailAddress { get; set; }
        public string FatherEmailAddress { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<Student> Students { get; set; }
    }
}
