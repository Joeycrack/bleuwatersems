﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class RoleInterface
    {
        [Required]
        public int RoleId { get; set; }
        [Required]
        public int InterfaceId { get; set; }
        public Interface Interface { get; set; }
        [Required]
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        
    }
}
