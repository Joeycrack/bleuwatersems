﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class GradeClass
    {
        public GradeClass ()
        {
            SubjectGradeClasses = new HashSet<SubjectGradeClass>();
            //Students = new HashSet<Student>();
        }

        public int ID { get; set; }
        [Required]
        public string GradeName { get; set; }
        [Required]
        public int MaxNumber { get; set; }
        [Required]
        public bool Status { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<SubjectGradeClass> SubjectGradeClasses { get; set; }
        //public ICollection<Student> Students { get; set; }
    }
}
