﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class PlanTypeInterface
    {
        [Required]
        public int PlanTypeId { get; set; }
        public PlanType PlanType { get; set; }
        [Required]
        public int InterfaceId { get; set; }
        public Interface Interface { get; set; }
        
    }
}
