﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Models
{
    public class Student
    {
        public Student()
        {
            StudentSubjects = new HashSet<StudentSubject>();
            StudentAssessments = new HashSet<StudentAssessment>();
            StudentAttendances = new HashSet<StudentAttendance>();
        }
        
        public int ID { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string PlaceOfBirth { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string MiddleName { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        public string Religion { get; set; }
        public string LocalGovt { get; set; }
        [Required]
        public int CountryId { get; set; }
        public Country Country { get; set; }
        [Required]
        public int StateId { get; set; }
        public State State { get; set; }
        [Required]
        public int StartingSession { get; set; }
        public Session Session { get; set; }
        public string SchoolLastAttended { get; set; }
        [Required]
        public int PreviousGradeClass { get; set; }
        [Required]
        public string Genotype { get; set; }
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Required]
        public string Allegies { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public int GradeClassId { get; set; }
        [Required]
        public int ClassId { get; set; }
        public Class Class { get; set; }
        [Required]
        public int ParentId { get; set; }
        public Parent Parent { get; set; }
        public int? GuardianId  { get; set; }
        public int TenantId { get; set; }
        public Tenant Tenant { get; set; }
        public ICollection<StudentSubject> StudentSubjects { get; set; }
        public ICollection<StudentAssessment> StudentAssessments { get; set; }
        public ICollection<StudentAttendance> StudentAttendances { get; set; }
    }
}
