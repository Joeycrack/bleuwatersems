﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.OWin;
using Microsoft.Owin;

namespace BleuWatersEMS.App_Start
{
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>(context.Get<ApplicationDbContext>()));

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"))
                {
                    TokenLifespan = TimeSpan.FromHours(24)
                };
            }
            return manager;
        }
    }

    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(IRoleStore<ApplicationRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            return new ApplicationRoleManager(new ApplicationRoleStore(context.Get<ApplicationDbContext>()));
        }
    }


    public class ApplicationDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            //SeedSuperAdmin(context);
            //context.ApplicationClients.AddOrUpdate(BuildClientsList());
            //context.SaveChanges();
            base.Seed(context);
        }

        private ApplicationClient[] BuildClientsList()
        {
            List<ApplicationClient> ClientsList = new List<ApplicationClient>
            {
                new ApplicationClient
                {
                    Id = "ngCuzaApp",
                    Secret= Helper.GetHash("<cuza></cuza>"),
                    Name="AngularJS front-end Application",
                    ApplicationType =  ApplicationTypes.JavaScript,
                    Active = true,
                    RefreshTokenLifeTime = 7200
                },
                new ApplicationClient
                {
                    Id = "mobileCuzaApp",
                    Secret= Helper.GetHash("<cuza-mobile></cuza-mobile>"),
                    Name="Mobile Application",
                    ApplicationType =  ApplicationTypes.NativeConfidential,
                    Active = true,
                    RefreshTokenLifeTime = 7200
                }
            };
            return ClientsList.ToArray();
        }

        void SeedSuperAdmin(ApplicationDbContext db)
        {
            var userManager = HttpContext.Current
                .GetOwinContext().GetUserManager<ApplicationUserManager>();

            var roleManager = HttpContext.Current
                .GetOwinContext().Get<ApplicationRoleManager>();

            //// Create Role SuperAdmin if it does not exist
            //const string roleName = "SuperAdmin";
            //var role = roleManager.FindByName(roleName);
            //if (role == null)
            //{
            //    role = new ApplicationRole(roleName);
            //    role.Default = false;
            //    role.IsStatic = true;
            //    // Set the new custom property:
            //    role.Description = "For super admin users";
            //    var roleresult = roleManager.Create(role);
            //}

            //var user = userManager.FindByName(roleName);
            //if (user == null)
            //{
            //    user = new ApplicationUser {
            //        UserName = roleName,

            //    };

            //    // Set the new custom properties:
            //    //user.Address = address;
            //    //user.City = city;
            //    //user.State = state;
            //    //user.PostalCode = postalCode;

            //    var result = userManager.Create(user, password);
            //    result = userManager.SetLockoutEnabled(user.Id, false);
            //}

            //// Add user admin to Role Admin if not already added
            //var rolesForUser = userManager.GetRoles(user.Id);
            //if (!rolesForUser.Contains(role.Name))
            //{
            //    var result = userManager.AddToRole(user.Id, role.Name);
            //}
        }
    }
}
