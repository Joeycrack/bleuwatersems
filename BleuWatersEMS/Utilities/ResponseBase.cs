﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Utilities
{
    public class ResponseBase<T> : ResponseBase
    {
        public T Payload
        {
            get;
            set;
        }
    }

    public class ResponseBase
    {
        /// <summary>
        /// Use on the frontend to distinguish api response and other responses
        /// </summary>
        public bool IsAPI
        {
            get
            {
                return true;
            }
        }

        public string ErrorDetails
        {
            get;
            set;
        }

        public bool IsSuccess
        {
            get;
            set;
        }

    }
}
