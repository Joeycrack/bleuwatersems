﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Utilities
{
    public class IdNamePair
    {
        public int Id { get; set; }
        public string Name { set; get; }
    }

    public class RoleIdNamePair
    {
        public string Id { get; set; }
        public string Name { set; get; }
    }
}
