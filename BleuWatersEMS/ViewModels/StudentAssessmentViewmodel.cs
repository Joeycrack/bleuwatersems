﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class StudentAssessmentViewmodel
    {
        public int ClassId { get; set; }
        public string SubjectName { get; set; }
        public int SubjectId { get; set; }
        public int StudentId { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set; }
    }

    public class AssesmentRetunViewModel
    {
        public string ClassName { get; set; }
        public string SubjectName { get; set; }
        public IEnumerable<AssessmentViewModel> Assesments { get; set; }
    }

    public class AssessmentViewModel
    {
        public string StudentName { get; set; }
        public string ClassName { get; set; }
        public int ID { get; set; }
        public int StudentId { get; set; }
        public int CAScore { get; set; }
        public int ExamsScore { get; set; }
        public int TenantId { get; set; }
        public int GradeClassId { get; set; }
        public int ClassId { get; set; }
        public int SessionId { get; set; }
        public int TermId { get; set; }
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public string SessionTerm { get; set; }
        public int RecordedBy { get; set; }
        public bool Submitted { get; set; }
        public string AdminRemarks { get; set; }
        public int? TotalScore { get; set; }
    }
}
