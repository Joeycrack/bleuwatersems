﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class ClassSectionViewModel
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public int GradeClassId { get; set; }
        public int ClassTeacherId { get; set; }
        public int MaxNumber { get; set; }
        public bool Status { get; set; }
        public List<SubjectClass> SubjectClass { get; set; }
    }

    public class GetClassSectionsViewModel
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string ClassTeacherName { get; set; }
        public int ClassTeacherId { get; set; }
        public int MaxNumber { get; set; }
        public bool Status { get; set; }
        public int GradeClassId { get; set; }
        //public List<SubjectClass> SubjectClasses { get; set; }
    }

    public class TeacherClassSectionsViewModel
    {
        public int Id { get; set; }
        public string ClassName { get; set; }
        public string ClassTeacherName { get; set; }
        public int ClassTeacherId { get; set; }
        public int NumOfStudents { get; set; }
        public int MaxNumber { get; set; }
        public bool Status { get; set; }
        public int GradeClassId { get; set; }
        public string SessionTerm { get; set; }

        //public List<SubjectClass> SubjectClasses { get; set; }
    }
}
