﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class AccountViewModel
    {
    }

    public class LoginRequestData
    {
        public string username { get; set; }
        public string password { get; set; }
        public string clientId { get; set; }
        public bool RememberMe { get; set; }
    }

    public class LoginResponseData
    {
        public List<PermissionViewModel> AllPermissions { get; set; }
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public int TeacherId { get; set; }
        public bool IsTeacher { get; set; }
        public bool IsAdmin { get; set; }
        public DateTime Expires { get; set; }
        public DateTime Issued { get; set; }
        public User User { get; set; }
        public string Token { get; set; }
        public int TenantId { get; set; }
    }

    public class UserTeacherViewModel
    {
        public User User { get; set; }
        public Teacher Teacher { get; set; }
    }

    public class RegistrationViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int CountryId { get; set; }
        public int StateId { get; set; }
        [Required]
        public string Username { get; set; }
        [DataType(DataType.Password)]
        public string Password { get; set; }
        public string MaritalStatus { get; set; }
        public string Sex { get; set; }
        public bool IsStaff { get; set; }
        public bool IsNonAcademicStaff { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsTeacher { get; set; }

    }
}
