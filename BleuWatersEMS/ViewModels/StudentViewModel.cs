﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class StudentRegistrationViewModel
    {
        public int ID { get; set; }
        
        public string StudentFirstName { get; set; }
        
        public string PlaceOfBirth { get; set; }
        
        public string StudentLastName { get; set; }
        
        public string StudentMiddleName { get; set; }
        
        public string Sex { get; set; }
        public int TenantId { get; set; }
        
        public string Religion { get; set; }
        public string StudentLocalGovt { get; set; }
        
        public int StudentNationalityId { get; set; }
        
        public int StudentStateId { get; set; }
        
        public int StartingSessionId { get; set; }
        public string SchoolLastAttended { get; set; }
        
        public int PreviousGradeClassId { get; set; }
        
        public string Genotype { get; set; }
        public string BloodGroup { get; set; }


        public DateTime DateOfBirth { get; set; }
        
        public string Allergies { get; set; }
        
        public bool Active { get; set; }
        
        public int GradeClassId { get; set; }
        
        public int ClassSectionId { get; set; }
        
        public int ParentId { get; set; }
        public int? GuardianId { get; set; }

        // Guardian 

        public int? GuardianSalutationId { get; set; }
        public string GuardianLastName { get; set; }
        public string GuardianFirstName { get; set; }
        public string GuardianOfficeAddress { get; set; }
        public string GuardianPhoneNo { get; set; }
        public string GuardianHomeAddress { get; set; }
        public string GuardianEmailAddress { get; set; }
        //public int? startingSessionId { get; set; }

        // parents
        public string MotherFirstName { get; set; }
        
        public int FatherSalutationId { get; set; }
        
        public int MotherSalutationId { get; set; }
        
        public string MotherLastName { get; set; }
        
        public string FatherFirstName { get; set; }
        
        public string FatherLastName { get; set; }
        
        public string FatherOccupation { get; set; }
        
        public string FatherReligion { get; set; }
        
        public string MotherReligion { get; set; }
        
        public int FatherNationalityId { get; set; }
        
        public int MotherNationalityId { get; set; }
        
        public int MotherStateId { get; set; }
        
        public int FatherStateId { get; set; }
        public string MotherLocalGovt { get; set; }
        public string FatherLocalGovt { get; set; }
        
        public string FatherPhoneNo { get; set; }
        public string FatherOfficePhoneNo { get; set; }
        
        public string MotherOccupation { get; set; }
        public string MotherofficePhoneNo { get; set; }
        public string MotherOfficeAddress { get; set; }
        public string FatherOfficeAddress { get; set; }
        
        public string MotherPhoneNo { get; set; }
        
        public bool ParentTogether { get; set; }
        public string FatherAddress { get; set; }
        public string MotherAddress { get; set; }
        
        public string MotherEmailAddress { get; set; }
        public string FatherEmailAddress { get; set; }
    }

    public class StudentSubjectViewModel
    {
        public int SubjectId { get; set; }
        public int StudentId { get; set; }
        public int TeacherId { get; set; }
        public int ClassId { get; set; }

    }

    public class AddStudentInitialList
    {
        public IEnumerable<GradeClass> GradeClasses { get; set; }
        public IEnumerable<Session> sessions { get; set; }
        public IEnumerable<Country> countries { get; set; }
        public IEnumerable<State> states { get; set; }
        public IEnumerable<Salutation> salutations { get; set; }
        public IEnumerable<GetClassSectionsViewModel> classSections { get; set; }

    }

    public class StudentAttendanceViewModel
    {
        public string StudentName { get; set; }
        public int StudentId { get; set; }
        public int GradeClassId { get; set; }
        public string GradeClassName { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public string Sex { get; set; }
        public int CurrentSessionId { get; set; }
        public string CurrentSessionName { get; set; }
        public int CurrentTermId { get; set; }
        public int TeacherId { get; set; }
        public string CurrentTermName { get; set; }
        public DateTime Date { get; set; }
        public bool Present { get; set; }
        public bool IsToday { get; set; }
        public string ClassSectionName { get; set; }
        public int TenantId { get; set; }
        public DateTime DateOfBirth { get; set; }

    }

    public class TeacherStudentViewModel
    {
        public string StudentName { get; set; }
        public string Sex { get; set; }
        public string ClassName { get; set; }
        public int StudentId { get; set; }
        public int GradeClassId { get; set; }
        public int ClassId { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string TermSession { get; set; }     
        public int Age { get; set; }

    }

    public class StudentViewModel
    {
        public string StudentName { get; set; }
        public string Sex { get; set; }
        public string ClassName { get; set; }
        public int StudentId { get; set; }
        public int GradeClassId { get; set; }
        public int ClassId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string ClassTeacherName { get; set; }
        public int Age { get; set; }
        public DateTime DateOfBirth { get; set; }

    }
}
