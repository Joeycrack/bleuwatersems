﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class GradeClassAndTeacherViweModel
    {
        public IEnumerable<GradeClass> GradeClasses { get; set; }
        public IEnumerable<TeacherViewModel> Teachers { get; set; }
    }
}
