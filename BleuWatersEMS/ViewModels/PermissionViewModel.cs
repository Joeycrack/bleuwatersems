﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class PermissionViewModel
    {
        public int permissionID { get; set; }
        public string name { get; set; }
        public int? parentid { get; set; }
        public string ParentName { get; set; }
        public int? level { get; set; }
        public string displayname { get; set; }

        public List<int> BusinessCategoryIds { get; set; } = new List<int>();

    }
}
