﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        public string subjectName { get; set; }
        public bool Status { get; set; }
        public List<SubjectGradeClass> SubjectGradeClass { get; set; }
    }

    public class SubjectGradeClassViewModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int GradeClassId { get; set; }
        public string GradeClassName { get; set; }
    }

    public class SubjectTeacherViewModel
    {
        public int SubjectId { get; set; }
        public string SubjectName { get; set; }
        public int NumOfStudent { get; set; }
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public int TermId { get; set; }
        public int SessionId { get; set; }
        public string SessionTerm { get; set; }
    }
}
