﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class IntegerViewModel
    {
        public int Input { get; set; }
    }
}
