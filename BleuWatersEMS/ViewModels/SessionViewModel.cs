﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class SessionViewModel
    {
       
        public int ID { get; set; }
        public string SessionName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsCurrentSession { get; set; }

        public bool FirstTermStatus { get; set; }
        public bool SecondTermStatus { get; set; }
        public bool ThirdTermStatus { get; set; }
        public List<Term> Terms { get; set; }
    }
}
