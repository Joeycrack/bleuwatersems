﻿using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.ViewModels
{
    public class TeacherViewModel
    {
        public int Id { get; set; }
        public string PhoneNumber { get; set; }
        public string FullName { get; set; }
        public string Sex { get; set; }
        
        public string MaritalSatus { get; set; }
        
        public bool Status { get; set; }
    }

    public class TeacherClassViewModel
    {
        public int ClassId { get; set; }
        public string ClassName { get; set; }
        public IEnumerable<Student> Students { get; set; }
        public IEnumerable<ClassSectionViewModel> Classes { get; set; }
    }
}
