interface NavAttributes {
  [propName: string]: any;
}
interface NavWrapper {
  attributes: NavAttributes;
  element: string;
}
interface NavBadge {
  text: string;
  variant: string;
}
interface NavLabel {
  class?: string;
  variant: string;
}

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  role?: boolean;
  badge?: NavBadge;
  title?: boolean;
  children?: NavData[];
  variant?: string;
  attributes?: NavAttributes;
  divider?: boolean;
  class?: string;
  label?: NavLabel;
  wrapper?: NavWrapper;
}

export const navItems: NavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Manage Students',
    url: '/students',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Attendance',
        url: '/students/attendance',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Students',
        url: '/students/students-list',
        icon: 'icon-layers',
      },
    ]
  },
  {
    name: 'Teachers Desk',
    url: '/teacher',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Dashboard',
        url: '/teacher/teacher-dashboard',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Attendance',
        url: '/teacher/attendance',
        icon: 'icon-pie-chart'
      },
      {
        name: 'My Classes',
        url: '/teacher/my-classes',
        icon: 'icon-cursor'
      },
      {
        name: 'My Students',
        url: '/teacher/my-students',
        icon: 'icon-cursor'
      },
      {
        name: 'My Subjects',
        url: '/teacher/my-subjects',
        icon: 'icon-cursor'
      },
      {
        name: 'Record Student Assesment',
        url: '/teacher/record-assesment',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Results',
        url: '/teacher/results',
        icon: 'icon-cursor'
      },
    ]
  },
  {
    name: 'Human Resource Desk (HR)',
    url: '/human-resource',
    icon: 'icon-puzzle',
    children: [
      //{
      //  name: 'Dashboard',
      //  url: '/human-resource/dashboard',
      //  //icon: 'icon-puzzle'
      //},
      {
        name: 'Staff Attendance',
        url: '/human-resource/attendance',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Teacher Performance',
        url: '/human-resource/teacher-performance',
        icon: 'icon-pie-chart'
      },
      {
        name: 'Announcement',
        url: '/human-resource/announcements',
        icon: 'icon-bell',
      },
      {
        name: 'Manage Staff Query',
        url: '/human-resource/staff-query',
        icon: 'icon-ban',
      },
      {
        name: 'Staff Profile Management',
        url: '/human-resource/Staff-Profile',
        icon: 'icon-drop'
      },
      {
        name: 'Manage Staff Leave',
        url: '/human-resource/staff-leave',
        icon: 'icon-drop'
      },
    ]
  },

  //{
  //  title: true,
  //  name: 'Theme'
  //},
  //{
  //  name: 'Colors',
  //  url: '/theme/colors',
  //  icon: 'icon-drop'
  //},
  //{
  //  name: 'Typography',
  //  url: '/theme/typography',
  //  icon: 'icon-pencil'
  //},
  //{
  //  title: true,
  //  name: 'Components'
  //},
  //{
  //  name: 'Base',
  //  url: '/base',
  //  icon: 'icon-puzzle',
  //  children: [
  //    {
  //      name: 'Cards',
  //      url: '/base/cards',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Carousels',
  //      url: '/base/carousels',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Collapses',
  //      url: '/base/collapses',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Forms',
  //      url: '/base/forms',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Pagination',
  //      url: '/base/paginations',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Popovers',
  //      url: '/base/popovers',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Progress',
  //      url: '/base/progress',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Switches',
  //      url: '/base/switches',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Tables',
  //      url: '/base/tables',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Tabs',
  //      url: '/base/tabs',
  //      icon: 'icon-puzzle'
  //    },
  //    {
  //      name: 'Tooltips',
  //      url: '/base/tooltips',
  //      icon: 'icon-puzzle'
  //    }
  //  ]
  //},
  //{
  //  name: 'Buttons',
  //  url: '/buttons',
  //  icon: 'icon-cursor',
  //  children: [
  //    {
  //      name: 'Buttons',
  //      url: '/buttons/buttons',
  //      icon: 'icon-cursor'
  //    },
  //    {
  //      name: 'Dropdowns',
  //      url: '/buttons/dropdowns',
  //      icon: 'icon-cursor'
  //    },
  //    {
  //      name: 'Brand Buttons',
  //      url: '/buttons/brand-buttons',
  //      icon: 'icon-cursor'
  //    }
  //  ]
  //},
  //{
  //  name: 'Charts',
  //  url: '/charts',
  //  icon: 'icon-pie-chart'
  //},
  //{
  //  name: 'Icons',
  //  url: '/icons',
  //  icon: 'icon-star',
  //  children: [
  //    {
  //      name: 'CoreUI Icons',
  //      url: '/icons/coreui-icons',
  //      icon: 'icon-star',
  //      badge: {
  //        variant: 'success',
  //        text: 'NEW'
  //      }
  //    },
  //    {
  //      name: 'Flags',
  //      url: '/icons/flags',
  //      icon: 'icon-star'
  //    },
  //    {
  //      name: 'Font Awesome',
  //      url: '/icons/font-awesome',
  //      icon: 'icon-star',
  //      badge: {
  //        variant: 'secondary',
  //        text: '4.7'
  //      }
  //    },
  //    {
  //      name: 'Simple Line Icons',
  //      url: '/icons/simple-line-icons',
  //      icon: 'icon-star'
  //    }
  //  ]
  //},
  //{
  //  name: 'Notifications',
  //  url: '/notifications',
  //  icon: 'icon-bell',
  //  children: [
  //    {
  //      name: 'Alerts',
  //      url: '/notifications/alerts',
  //      icon: 'icon-bell'
  //    },
  //    {
  //      name: 'Badges',
  //      url: '/notifications/badges',
  //      icon: 'icon-bell'
  //    },
  //    {
  //      name: 'Modals',
  //      url: '/notifications/modals',
  //      icon: 'icon-bell'
  //    }
  //  ]
  //},
  //{
  //  name: 'Widgets',
  //  url: '/widgets',
  //  icon: 'icon-calculator',
  //  badge: {
  //    variant: 'info',
  //    text: 'NEW'
  //  }
  //},
  {
    divider: true
  },
  {
    title: true,
    name: 'Settings',
  },
  {
    name: 'School Settings',
    url: '/settings',
    icon: 'icon-star',
    children: [
      {
        name: 'Academic settings',
        url: '/settings/academic',
        icon: 'icon-star',

      },
      {
        name: 'User settings',
        url: '/settings/users',
        icon: 'icon-star',

      }
    ]
  },
  {
    name: 'User Settings',
    url: '/pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: '/login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: '/register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '/404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '/500',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Disabled',
    url: '/dashboard',
    icon: 'icon-ban',
    badge: {
      variant: 'secondary',
      text: 'NEW'
    },
    attributes: { disabled: true },
  },
  {
    name: 'Download CoreUI',
    url: 'http://coreui.io/angular/',
    icon: 'icon-cloud-download',
    class: 'mt-auto',
    variant: 'success',
    attributes: { target: '_blank', rel: 'noopener' }
  },
  {
    name: 'Try CoreUI PRO',
    url: 'http://coreui.io/pro/angular/',
    icon: 'icon-layers',
    variant: 'danger',
    attributes: { target: '_blank', rel: 'noopener' }
  }
];

export const TeachersNav: NavData[] = [
  {
    name: 'Teachers Desk',
    url: '/teacher',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Dashboard',
        url: '/teacher/teacher-dashboard',
        //icon: 'icon-puzzle'
      },
      {
        name: 'Attendance',
        url: '/teacher/attendance',
        //icon: 'icon-puzzle'
      },
      {
        name: 'My Classes',
        url: '/teacher/my-classes',
        //icon: 'icon-puzzle'
      },
      {
        name: 'My Students',
        url: '/teacher/my-students',
        //icon: 'icon-puzzle'
      },
      {
        name: 'My Subjects',
        url: '/teacher/my-subjects',
        //icon: 'icon-puzzle'
      },
      {
        name: 'Record Student Assesment',
        url: '/teacher/record-assesment',
        //icon: 'icon-puzzle'
      },
      {
        name: 'Results',
        url: '/teacher/results',
        //icon: 'icon-puzzle'
      },
    ]
  },
];

export const NoNav: NavData[] = [
  {
   
  },
];

