import { Component, OnDestroy, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { navItems, TeachersNav, NoNav } from '../../_nav';
import { AuthenticationService } from '../../Services/authentication.service';
import { User } from '../../_models/UserModel';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy, OnInit {
  CurrentUser: User = this.authenticationService.currentUserValue;
  public navItems = navItems;
  public teachersNav = TeachersNav;
  public noNav = [];
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  isTeacher: boolean = false;
  isAdmin: boolean = false;
  isNone: boolean = false;
  error = '';
  loading: boolean = false

  constructor(private authenticationService: AuthenticationService, private router: Router, private toastr: ToastrService, @Inject(DOCUMENT) _document?: any, ) {
    

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  SetInterface() {
    if (this.CurrentUser == null || this.CurrentUser.isAdmin === false && this.CurrentUser.isAdmin === false) {
      this.isNone = true;
    } else {
      this.isAdmin = this.CurrentUser.isAdmin;
      this.isTeacher = this.CurrentUser.isTeacher;
    }
  }

  ngOnInit() {
    this.SetInterface();

  }

  LogOut() {
    this.authenticationService.logout()
      .subscribe(
        data => {
          if (data.isSuccess == true) {
            this.toastr.success("you've been logged out", "Success!")
            
            this.router.navigate(['/login']);
          } else if (data.isSuccess == false) {
            this.toastr.error(data.errorDetails, "Error!");
            return;
          }
          //this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }
}
