export class ClassSectionModel {
  ID: number;
  ClassName: string;
  GradeClassId: number;
  ClassTeacherId: number;
  MaxNumber: number;
  Status: boolean;
  gradeClassId: string;
};

export class TeacherClassSectionModel {
  ID: number;
  ClassName: string;
  SessionTerm: string;
  NumOfStudents: number
  GradeClassId: number;
  ClassTeacherId: number;
  MaxNumber: number;
  Status: boolean;
  gradeClassId: string;
};
