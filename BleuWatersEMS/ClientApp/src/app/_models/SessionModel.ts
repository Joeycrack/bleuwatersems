export class SessionModel
{
  [x: string]: any;
  id?: number;
  SessionName: string;
  StartDate: Date;
  EndDate: Date;
  IsCurrentSession: boolean;
  isSuccess: boolean;
}
