export class SubjectGradeClassModel {
  GradeClassId: number;
  SubjectId: number;
}

export class SubjectModel {
  id: number;
  subjectName: string;
  Status: boolean
}

export class SubjectTeacherModel {
  id: number;
  fullName: string;
  Status: boolean
  subjectName: string;
}
