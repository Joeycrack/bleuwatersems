export class SubjectClass
{
  TeacherId: number;
  SubjectId: number;
  ClassSectionId: number;
  fullName: string;
  subjectName: string;
}
