export class TermModel
{
  TermName: string;
  TermStartDate: Date;
  IsCurrent: boolean;
  TermEndDate: Date;
  TermStatus: boolean;
}
