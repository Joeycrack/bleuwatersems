export class TeacherSubjectModel {
  SubjectId: number;
  SubjectName: string;
  NumOfStudent: number;
  ClassId: number;
  ClassName: string;
  SessionTerm: string;
}
