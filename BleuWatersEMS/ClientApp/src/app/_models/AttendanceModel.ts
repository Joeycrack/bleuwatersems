export class AttendanceModel {
  classId: number;
  className: string;
  classSectionName: string;
  currentSessionId: number;
  currentSessionName: string;
  currentTermId: number;
  currentTermName: string;
  date: Date;
  gradeClassId: number;
  gradeClassName: string;
  present: boolean;
  sex: string;
  studentId: number;
  studentName: string;
  isToday: boolean;
  TeacherId: number;
};
