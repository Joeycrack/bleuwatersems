export class User {
  id: number;
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  authdata?: string;
  payload: payload;
  teacherId: number;
  isTeacher: boolean;
  isAdmin: boolean;
  token: string;
  tenantId: number;
  
}

export class payload {
  teacherId: number;
  isAdmin: boolean;
  isTeacher: boolean;
}
