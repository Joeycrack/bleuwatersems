export class AssessMentModel {

  studentName: string;
  className: string;
  id: number;
  studentId: number;
  cAScore: number;
  examsScore: number;
  tenantId: number;
  gradeClassId: number;
  classId: number;
  sessionId: number;
  termId: number;
  subjectId: number;
  subjectName: number;
  sessionTerm: number;
  RecordedBy: number;
}

export class AssessmentReturnModel {
  subjectName: string;
  className: string;
  assesments: AssessMentModel[];
}

