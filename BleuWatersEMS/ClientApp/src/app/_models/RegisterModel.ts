export class RegisterModel {
  StateId: number;
  SchoolName: string;
  Address: string;
  PhoneNumber: string;
  PhoneNumber2: string;
  CountryId: number;
  Email: string;
  Website: string
  Motto: string;
  Logo: string;
  PlanTypeId: number;
  BadgeUrl: number;
}
