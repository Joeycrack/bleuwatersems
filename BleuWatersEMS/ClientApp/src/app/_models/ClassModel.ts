export class ClassModel {
  [x: string]: any;
  id?: number;
  GradeName: string;
  MaxNumber: number;
  Status: boolean;
  isSuccess: boolean;
}
