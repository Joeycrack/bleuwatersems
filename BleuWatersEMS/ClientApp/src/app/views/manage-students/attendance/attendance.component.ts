import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../Services/settings.service';
import { AttendanceService } from '../../../Services/attendance.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from 'underscore';
import { AttendanceModel } from '../../../_models/AttendanceModel';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {
  Attendance: AttendanceModel[] = [];
  loading = false;
  error = '';
  _attendance: {} = {};
  p1;

  constructor(private attendanceService: AttendanceService, private toastr: ToastrService) { }

  GetStudentsForAttendance() {
    return this.attendanceService.GetTodayAttendance().subscribe(
      data => {
        if (data.isSuccess == true) {
          this.Attendance = data.payload;
          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }
  m: AttendanceModel;
 
  OnChange(input) {
    this.m = Object.assign(this._attendance, input);
    let a = !this.m.present;
    this.m.present = a;

    this.attendanceService.AddAttendance(this.m).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.toastr.success("Great!", "Success!");
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
    
  }

  ngOnInit() {
    this.GetStudentsForAttendance();

  }

}
