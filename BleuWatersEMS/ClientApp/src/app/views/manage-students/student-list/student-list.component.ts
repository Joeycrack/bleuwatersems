import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription, Subject, never } from 'rxjs';
import { DataTablesModule } from 'angular-datatables';
import { NgxPaginationModule } from 'ngx-pagination';
import * as _ from 'underscore';
import { StudentDetailsComponent } from '../student-details/student-details.component';
import { StudentService } from '../../../Services/student.service';
import { AuthenticationService } from '../../../Services/authentication.service';
import { User } from '../../../_models/UserModel';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  //styleUrls: ['./student-list.component.scss']
})
export class StudentListComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(private modalService: BsModalService, private studentService: StudentService, private toastr: ToastrService, private authService: AuthenticationService ) { }

  CurrentUser: User = this.authService.currentUserValue;
  StudentList: [] = [];
  error: Error;
  loading;

  addStudentModal() {
    this.bsModalRef = this.modalService.show(StudentDetailsComponent, Object.assign({}, { class: 'modal-lg', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  };

  GetStudents() {
    return this.studentService.GetStudents().subscribe(
      data => {
        if (data.isSuccess == true) {
          this.StudentList = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  ngOnInit() {
    this.GetStudents();

  }

}
