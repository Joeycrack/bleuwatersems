import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NgxPaginationModule } from 'ngx-pagination';


import { StudentDetailsComponent } from './student-details/student-details.component';
import { StudentListComponent } from './student-list/student-list.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { ManageStudentRoutingModule } from './manage-students-routing.module';



@NgModule({
  declarations: [StudentDetailsComponent, StudentListComponent, AttendanceComponent],
  imports: [
    CommonModule,
    ManageStudentRoutingModule,
    BsDatepickerModule,
    ModalModule,
    BsDropdownModule,
    ReactiveFormsModule,
    FormsModule,
    TabsModule,
    NgxPaginationModule
  ],

  entryComponents: [StudentDetailsComponent],
})
export class ManageStudentsModule { }
