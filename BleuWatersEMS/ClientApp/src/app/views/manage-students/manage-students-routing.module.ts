import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentListComponent } from './student-list/student-list.component';
import { AttendanceComponent } from './attendance/attendance.component';
import { AdminGuard } from '../../Services/AuthGuard/role-guard.service';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Manage Students',
      isAdmin: true
    },
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        redirectTo: 'students-list',
        pathMatch: 'full',
      },
      {
        path: 'students-list',
        component: StudentListComponent,
        data: {
          title: 'Student List'
        }
      },
      {
        path: 'attendance',
        component: AttendanceComponent,
        data: {
          title: 'Student Attendance'
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageStudentRoutingModule { }
