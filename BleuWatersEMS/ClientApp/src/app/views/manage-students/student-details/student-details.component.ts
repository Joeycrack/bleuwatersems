import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { StudentService } from '../../../Services/student.service';
import { StateModel } from '../../../_models/StateModel';
import { ClassModel } from '../../../_models/ClassModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import * as _ from 'underscore';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.scss']
})
export class StudentDetailsComponent implements OnInit {
  //bsModalRef: BsModalRef;
  AddStudentForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddStudent = false;
  Age: number;
  ClassSections: [];
  gradeClasses: [];
  Salutations: [];
  Sessions: [];
  Countries: [];
  State: any[];
  StudentCountryStates: any;
  gradeSections: ClassSectionModel[];
  FatherCountryStates: any;
  MotherCountryStates: any;

  constructor(private bsModalRef: BsModalRef, private formBuilder: FormBuilder, private studentService: StudentService, private toastr: ToastrService)
  {
    this.AddStudentForm = new FormGroup({
      // start student information
      StudentFirstName: new FormControl(''), StudentLastName: new FormControl(''), StudentMiddleName: new FormControl(''),
      DateOfBirth: new FormControl(''), Sex: new FormControl(''), Religion: new FormControl(''), FatherReligion: new FormControl(''),
      StudentNationalityId: new FormControl(''), StudentStateId: new FormControl(''), StudentLocalGovt: new FormControl(''),
      PlaceOfBirth: new FormControl(''), SchoolAttendedLastSession: new FormControl(''), PreviousGradeClassId: new FormControl(''),
      Genotype: new FormControl(''), BloodGroup: new FormControl(''), Allergies: new FormControl(''), StudentHomeAddress: new FormControl(''),
      StartingSessionId: new FormControl(0), GradeClassId: new FormControl(0), ClassSectionId: new FormControl(0), Active: new FormControl(true), 

      // start Parent information
      FatherSalutationId: new FormControl(''), FatherFirstName: new FormControl(''), FatherLastName: new FormControl(''),
      FatherOccupation: new FormControl(''), FatherOfficeAddress: new FormControl(''), FatherOfficePhoneNo: new FormControl(''),
      FatherPhoneNo: new FormControl(''), FatherNationalityId: new FormControl(''), FatherStateId: new FormControl(''), FatherEmailAddress: new FormControl(''),
      FatherLocalGovt: new FormControl(''), MotherSalutationId: new FormControl(''), MotherFirstName: new FormControl(''),
      MotherLastName: new FormControl(''), MotherOccupation: new FormControl(''), MotherOfficeAddress: new FormControl(''),
      MotherOfficePhoneNo: new FormControl(''), MotherPhoneNo: new FormControl(''), MotherEmailAddress: new FormControl(''), MotherNationalityId: new FormControl(true),
      MotherStateId: new FormControl(''), MotherLocalGovt: new FormControl(''), MotherReligion: new FormControl(''),
      // general info
      MotherAddress: new FormControl(''), FatherAddress: new FormControl(''), ParentTogether: new FormControl(false),

      // start guardian infomation
      GuardianSalutationId: new FormControl(''), GuardianLastName: new FormControl(''), GuardianFirstName: new FormControl(''),
      GuardianHomeAddress: new FormControl(''), GuardianOfficeAddress: new FormControl(''), GuardianPhoneNo: new FormControl(''),
      GuardianEmailAddress: new FormControl(''), GuardianOccupation: new FormControl('')

    });

    this.formError = {
      'SessionName': '',
      'StartDate': '',
    }

    this.validationMessages = {
      'SessionName': {
        'required': 'Session name is required',
        'minlength': 'Session name must be at least 3 characters.',
        'maxlength': 'Session name is too long.'
      },
      'StartDate': {
        'required': 'session start date is required',
      },

    };
  }
  

  CalculateAge(input) {
      var timeDiff = Math.abs(Date.now() - input.getTime());
      this.Age = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      return this.Age;
  }

  OnStudentCountryChange(input) {
    //var countryState
    this.StudentCountryStates = _.filter(this.State, function (element) {
      return element.countryId == input;
      //return this.State.indexOf(element.countryId) == input;
    })
  }

  OnFatherCountryChange(input) {
    //var countryState
    this.FatherCountryStates = _.filter(this.State, function (element) {
      return element.countryId == input;
      //return this.State.indexOf(element.countryId) == input;
    })
  }

  OnMotherCountryChange(input) {
    //var countryState
    this.MotherCountryStates = _.filter(this.State, function (element) {
      return element.countryId == input;
      //return this.State.indexOf(element.countryId) == input;
    })
  }

  OnGradeClassChange(input) {
    this.gradeSections = _.filter(this.ClassSections, function (element: ClassSectionModel) {
      return element.gradeClassId == input;
    });

    //this.gradeSections = Object.assign({}, {}, grade);
  }

  AddFormInitial() {
    return this.studentService.StudentInitialLists().subscribe(
      data => {
        this.ClassSections = data.payload.classSections;
        this.gradeClasses = data.payload.gradeClasses;
        this.Sessions = data.payload.sessions;
        this.Countries = data.payload.countries;
        this.State = data.payload.states;
        this.Salutations = data.payload.salutations;
      });
  }

  ngOnInit() {
    this.AddFormInitial();
    
    this.AddStudentForm = this.formBuilder.group({
      StudentFirstName: ['', Validators.required], StudentLastName: ['', Validators.required],
      StudentMiddleName: ['', Validators.required], DateOfBirth: ['', Validators.required], Religion: [''],
      StudentNationalityId: [0, Validators.required], StudentStateId: [0, Validators.required], StudentLocalGovt: ['', Validators.required],
      Sex: ['', Validators.required], PlaceOfBirth: ['', Validators.required], SchoolAttendedLastSession: ['', Validators.required],
      Genotype: ['', Validators.required], StartingSessionId: [0, Validators.required], BloodGroup: ['', Validators.required],
      PreviousGradeClassId: [0, Validators.required], GradeClassId: [0, Validators.required], Allergies: ['', Validators.required],
      ClassSectionId: [0, Validators.required], Active: [true, Validators.required], StudentHomeAddress: ['', Validators.required],

      FatherSalutationId: [0, Validators.required], FatherFirstName: ['', Validators.required],
      FatherLastName: ['', Validators.required], FatherOfficeAddress: ['', Validators.required], FatherOfficePhoneNo: [''],
      FatherOccupation: ['', Validators.required], FatherPhoneNo: ['', Validators.required], FatherNationalityId: [0, Validators.required],
      FatherStateId: [0, Validators.required], FatherLocalGovt: ['', Validators.required], MotherSalutationId: [0, Validators.required], FatherEmailAddress: [''],
      MotherFirstName: ['', Validators.required], MotherLastName: ['', Validators.required], MotherOccupation: ['', Validators.required],
      MotherOfficeAddress: ['', Validators.required], MotherOfficePhoneNo: ['', Validators.required], MotherPhoneNo: ['', Validators.required],
      MotherEmailAddress: ['', Validators.required], MotherNationalityId: ['', Validators.required], MotherStateId: ['', Validators.required],
      MotherLocalGovt: ['', Validators.required], MotherAddress: [''], FatherAddress: [''],
      ParentTogether: ['', Validators.required], MotherReligion: ['', Validators.required], FatherReligion: ['', Validators.required],

      GuardianSalutationId: [''], GuardianLastName: [''], GuardianFirstName: [''], GuardianEmailAddress: [''],
      GuardianOfficeAddress: [''], GuardianPhoneNo: [''], GuardianHomeAddress: [''], GuardianOccupation: ['']
    })


  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddStudentForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddStudentForm.value);

    this.studentService.AddStudent(m)
      .subscribe(data => {
        if (data.isSuccess == true) {
          this.toastr.success("Student Registration was Successfully", "Success!")
          this.AddStudent = true
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
          return;
        }

      },
        error => {
          this.error = error;
          this.toastr.error("There was an error completing this request please contact an Admin", "Error!");
          this.loading = false;
          return;
        });

    this.loading = false;
  }

  CloseModal() {
    this.bsModalRef.hide();
    //this.studentService.onAddClassSection(this.AddStudent);
  }
}
