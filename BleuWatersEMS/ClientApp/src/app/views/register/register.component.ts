import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../Services/authentication.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {

  RegisterForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    // redirect to home if already logged in

    // TODO I nee to come back to this
    //if (this.authenticationService.currentUserValue) {
    //  this.router.navigate(['/']);
    //}

    this.RegisterForm = new FormGroup({
      CountryId: new FormControl(''),
      PhoneNumber2: new FormControl(''),
      SchoolName: new FormControl(''),
      Address: new FormControl(''),
      PhoneNumber: new FormControl(''),
      StateId: new FormControl(''),
      Website: new FormControl(''),
      Motto: new FormControl(''),
      Logo: new FormControl(''),
      PlanTypeId: new FormControl('')

    });

    this.formError = {
      'username': '',
      'password': '',
      'RepeatPassord': '',
      'Firstname': '',
      'LastName': '',
      'PhoneNumer': '',
      'sex': '',
      'MaritalStatus': ''
    }

    this.validationMessages = {
      'username': {
        'required': 'Article title is required',
        'minlength': 'Article title must be at least three characters.',
        'maxlength': 'Article title cannot exceed 100 characters.'
      },
      'password': {
        'required': 'password is required',
      },

    };
  }

  ngOnInit() {
    this.RegisterForm = this.formBuilder.group({
      StateId: ['', Validators.required],
      SchoolName: ['', Validators.required],
      Address: ['', Validators.required],
      PhoneNumber: ['', Validators.required],
      PhoneNumber2: ['', Validators.required],
      CountryId: ['', Validators.required],
      Email: ['', Validators.required],
      Website: ['', Validators.required],
      Motto: ['', Validators.required],
      Logo: ['', Validators.required],
      PlanTypeId: ['', Validators.required], 
    });
    // get return url from route parameters or default to '/'
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.RegisterForm.invalid) {
      return;
    }
    

    this.loading = true;

    var m = Object.assign({}, {}, this.RegisterForm.value);
    m.BadgeUrl = m.Logo;
    this.authenticationService.register(m)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
  




}
