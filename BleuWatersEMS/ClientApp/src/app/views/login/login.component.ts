import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../Services/authentication.service';
import { first } from 'rxjs/operators';
import { LoginModel } from '../../_models/LoginModel';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService
  ) {
    // redirect to home if already logged in

    // TODO I nee to come back to this
    //if (this.authenticationService.currentUserValue) {
    //  this.router.navigate(['/']);
    //}

    this.loginForm = new FormGroup({
      Username: new FormControl(''),
      Password: new FormControl(''),
    });

    this.formError = {
      'username': '',
      'password': '',
    }

    this.validationMessages = {
      'username': {
        'required': 'Article title is required',
        'minlength': 'Article title must be at least three characters.',
        'maxlength': 'Article title cannot exceed 100 characters.'
      },
      'password': {
        'required': 'password is required',
      },
      
    };
  }
 

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      Username: ['', Validators.required],
      Password: ['', Validators.required]
    });

   

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.loginForm.value);
    
    this.authenticationService.login(m)
      .pipe(first())
      .subscribe(
      data => {
        if (data.isSuccess == true) {
          this.toastr.success("Login Successful", "Success!")
          if (this.returnUrl == '/login' || this.returnUrl == '/register') {
            if (data.payload.isTeacher = true) {
              this.router.navigate(['/teacher/teacher-dashboard']);
            }
          }
          //this.router.navigate(this.route.snapshot.queryParams['/teacher/teacher-dashboard']);
          if (this.returnUrl == '/' && data.payload.isAdmin == false || data.payload.isAdmin == null) {
            if (data.payload.isTeacher == true) {
              this.router.navigate(['/teacher/teacher-dashboard']);
            }
          } else {

            this.router.navigate([this.returnUrl]);
          }
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
          return;
        }
          //this.router.navigate([this.returnUrl]);
        },
        error => {
          this.error = error;
        this.loading = false;
      });
  }

  LogOut() {
    this.authenticationService.logout();
  }

  ngOnDestroy(): void {
    //this.authenticationService.login.unsubscribe();
  }

}
