import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { AddUserComponent } from './add-user/add-user.component';
import { SettingsService } from '../../../Services/settings.service';
import { UserDetailsComponent } from './user-details/user-details.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  UserList: [];
  bsModalRef: BsModalRef;
  AddUserSub;
  p1;
  constructor(private modalService: BsModalService, private settingService: SettingsService) {

    this.AddUserSub = this.settingService.UserAdd$.subscribe(
      data => {
        if (data == true) {
          this.getUsers();
        }
      });
  }
  
  addUserModal() {
    
    this.bsModalRef = this.modalService.show(AddUserComponent, Object.assign({}, { class: 'modal-md', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  };
  

  ViewUserModal(input) {
    this.bsModalRef = this.modalService.show(UserDetailsComponent, { class: 'modal-md', backdrop: true, ignoreBackdropClick: true, initialState: input });
    this.bsModalRef.content.closeBtnName = 'Close';
  };

  getUsers() {
    this.AddUserSub = this.settingService.GetUsers().subscribe(
      data => {
        this.UserList = data.payload;
      });
  }
  


  ngOnInit() {

    this.getUsers();
  }

}
