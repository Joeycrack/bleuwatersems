import { Component, OnInit, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../Services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { SettingsService } from '../../../../Services/settings.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {


  UserDetailForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddUser = false;

  activated: true
  applicationUserId: ""
  countries: null
  countryId: null
  email: ""
  firstName: ""
  id: 0
  isAcademicStaff: false
  isAdmin: false
  isApproved: false
  isNewUser: true
  isNonAcademicStaff: false
  isParent: false
  isStudent: false
  isTeacher: false
  lastName: "James"
  maritalStatus: "Single"
  phoneNumber: ""
  phoneNumber2: null
  salutation: null
  salutationID: null
  sex: ""
  stateId: null

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private settingService: SettingsService,
  ) {
    // redirect to home if already logged in

    // TODO I nee to come back to this
    //if (this.authenticationService.currentUserValue) {
    //  this.router.navigate(['/']);
    //}

    this.UserDetailForm = new FormGroup({
      Username: new FormControl(''),
      Password: new FormControl(''),
      RepeatPassord: new FormControl(''),
      FirstName: new FormControl(''),
      LastName: new FormControl(''),
      PhoneNumber: new FormControl(''),
      sex: new FormControl(''),
      MaritalStatus: new FormControl(''),

    });

    this.formError = {
      'Firstname': '',
      'LastName': '',
      'PhoneNumer': '',
      'sex': '',
      'MaritalStatus': ''
    }

    this.validationMessages = {

    };
  }

  ngOnInit() {

    this.UserDetailForm = this.formBuilder.group({
      FirstName: [this.firstName, Validators.required],
      LastName: [this.lastName, Validators.required],
      PhoneNumber: [this.phoneNumber, Validators.required],
      sex: [this.sex, Validators.required],
      MaritalStatus: [this.maritalStatus, Validators.required],
      Email: [this.email, Validators.required],
      IsAdmin: [this.isAdmin, Validators.required],
      IsTeacher: [this.isTeacher, Validators.required],
      IsNonAcademicStaff: [this.isNonAcademicStaff, Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onAddUser(this.AddUser);
  }

}
