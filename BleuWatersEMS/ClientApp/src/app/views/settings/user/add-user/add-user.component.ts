import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../../Services/authentication.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';
import { SettingsService } from '../../../../Services/settings.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  AddUserForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddUser = false;

  edit: boolean = false


  activated: true
  applicationUserId: "0b1bf18b-9f34-46ae-9191-776d41c01194"
  countries: null
  countryId: null
  email: ""
  firstName: ""
  id: 0
  isAcademicStaff: false
  isAdmin: false
  isApproved: false
  isNewUser: true
  isNonAcademicStaff: false
  isParent: false
  isStudent: false
  isTeacher: false
  lastName: "James"
  maritalStatus: "Single"
  phoneNumber: ""
  phoneNumber2: null
  salutation: null
  salutationID: null
  sex: ""
  stateId: null

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private settingService: SettingsService,
  ) {
    // redirect to home if already logged in

    // TODO I nee to come back to this
    //if (this.authenticationService.currentUserValue) {
    //  this.router.navigate(['/']);
    //}

    if (this.id > 0) {
      this.edit = true
    }

    this.AddUserForm = new FormGroup({
      Email: new FormControl(''),
      IsAdmin: new FormControl(''),
      IsTeacher: new FormControl(''),
      IsNonAcademicStaff: new FormControl(''),
      FirstName: new FormControl(''),
      LastName: new FormControl(''),
      PhoneNumber: new FormControl(''),
      sex: new FormControl(''),
      MaritalStatus: new FormControl(''),
    });

    this.formError = {
      'Firstname': '',
      'LastName': '',
      'PhoneNumer': '',
      'sex': '',
      'MaritalStatus': ''
    }

    this.validationMessages = {

    };
  }

  ngOnInit() {

    this.AddUserForm = this.formBuilder.group({
      FirstName: [this.firstName, Validators.required],
      LastName: [this.lastName, Validators.required],
      PhoneNumber: [this.phoneNumber, Validators.required],
      sex: [this.sex, Validators.required],
      MaritalStatus: [this.maritalStatus, Validators.required],
      Email: [this.email, Validators],
      IsAdmin: [false],
      IsTeacher: [true],
      IsNonAcademicStaff: [false]
    });


    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddUserForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddUserForm.value);

    this.authenticationService.addUser(m)
      .pipe(first())
      .subscribe(
      data => {
        if (data.isSuccess == true) {
          this.toastr.success("User resgistration was Successfully", "Success!")
          this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
          this.error = error;
          this.loading = false;
      });
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onAddUser(this.AddUser);
  }

}
