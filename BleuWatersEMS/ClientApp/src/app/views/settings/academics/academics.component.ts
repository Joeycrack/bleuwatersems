import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { SettingsService } from '../../../Services/settings.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription, Subject, never } from 'rxjs';
import { DataTablesModule } from 'angular-datatables';
import { NgxPaginationModule } from 'ngx-pagination';
import { ClassModel } from '../../../_models/ClassModel';
import * as _ from 'underscore';
import { SubjectGradeClassModel } from '../../../_models/SubjectGradeClassModel';
import { SubjectClass } from '../../../_models/SubjectClassModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import { TermModel } from '../../../_models/TermModel';


@Component({
  selector: 'app-academics',
  templateUrl: './academics.component.html',
  styleUrls: ['./academics.component.scss']
})
export class AcademicsComponent implements OnInit, OnDestroy {

  bsModalRef: BsModalRef;

  @Input() AddClass: boolean;
  AddClassSub: Subscription;
  AddSessionSub: Subscription;
  AddSubjectSub: Subscription;
  AddClassSectionSub: Subscription;
  classList: [];
  sessionList = [];
  subjectList = [];
  classSectionList: [];
  dtOptions: any = {};
  p1; p2; p3; p4;


  constructor(private modalService: BsModalService, private settingService: SettingsService) {

    this.AddClassSub = this.settingService.ClassAdd$.subscribe(
      data => {
        if (data == true) {
          this.getClasses();
        }
      });

    this.AddSessionSub = this.settingService.SessionAdd$.subscribe(
      data => {
        if (data == true) {
          this.getSessions();
        }
      });

    this.AddSubjectSub = this.settingService.SubjectAdd$.subscribe(
      data => {
        if (data == true) {
          this.getSubjects();
        }
      });

    this.AddClassSectionSub = this.settingService.ClassSectionAdd$.subscribe(
      data => {
        if (data == true) {
          this.getClassSections();
        }
      });
  }

  addSessionModal() {
   
    this.bsModalRef = this.modalService.show(AddSessionModal, Object.assign({}, { class: 'modal-lg', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  };

  addClassModal() {
    
    this.bsModalRef = this.modalService.show(AddClassModal, Object.assign({}, { class: 'modal-sm', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  addSubjectModal() {

    this.bsModalRef = this.modalService.show(AddSubjectModal, Object.assign({}, { class: 'modal-md', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  addClassSectionModal() {

    this.bsModalRef = this.modalService.show(AddClassSectionModal, Object.assign({}, { class: 'modal-md', backdrop: true, ignoreBackdropClick: true }))
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getClasses() {
    this.AddClassSub = this.settingService.GetGradeClasses().subscribe(
      data => {
        this.classList = data.payload;
      });
  }

  getSessions() {
    return this.settingService.GetSession().subscribe(
      //this.AddSessionSub
      data => {
        this.sessionList = data.payload;
        
      });
  }

  getSubjects() {
    this.AddSubjectSub = this.settingService.GetSubject().subscribe(
      data => {
        this.subjectList = data.payload;

      });
  }

  getSessionsN() {
    return this.settingService.GetSession().subscribe(
      data => {
        this.sessionList = data.payload;

      });
  }

  getClassSections() {
    return this.settingService.GetClassSections().subscribe(
      data => {
        this.classSectionList = data.payload;

      });
  }


  ngOnInit() {

    this.getClasses();
    this.getSessions();
    this.getSubjects();
    this.getClassSections();

  }

  ngOnDestroy() {
    this.AddClassSub.unsubscribe();
    this.AddSessionSub.unsubscribe();
    this.AddSubjectSub.unsubscribe();
  }

}

///TODO THIS IS THE BEGINING OF ADD SESSION MODAL COMPONENT
@Component({
  selector: 'add-session-modal',
  templateUrl: './addSession.component.html',
  styleUrls: ['./academics.component.scss']
})
export class AddSessionModal implements OnInit {
  bsValue = new Date();
  AddSessionForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddSession = false;
  p5;
  newTerm: TermModel;
  Terms: TermModel[] = [];

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private settingService: SettingsService,
    private toastr: ToastrService
  ) {
    this.AddSessionForm = new FormGroup({
      SessionName: new FormControl(''),
      StartDate: new FormControl(''),
      EndDate: new FormControl(''),
      IsCurrentSession: new FormControl(''),
      TermName: new FormControl(''),
      TermStatus: new FormControl(''),
      TermStartDate: new FormControl(''),
      TermEndDate: new FormControl(''),
    });

    this.formError = {
      'SessionName': '',
      'StartDate': '',
    }

    this.validationMessages = {
      'SessionName': {
        'required': 'Session name is required',
        'minlength': 'Session name must be at least 3 characters.',
        'maxlength': 'Session name is too long.'
      },
      'StartDate': {
        'required': 'session start date is required',
      },

    };
  }

  ngOnInit() {
    this.AddSessionForm = this.formBuilder.group({
      SessionName: ['', Validators.required],
      StartDate: [new Date(), Validators.required],
      EndDate: [''],
      IsCurrentSession: [true],
      TermName: [''],
      TermStatus: [true],
      TermStartDate: [''],
      TermEndDate: [''],
    })

  }

  AddTerm(input) {
    if (this.Terms.length >= 3) {
      this.toastr.error('You cannot add more than 3 terms for a session ', 'Error');
      this.AddSessionForm.value.TermEndDate = '';
      this.AddSessionForm.value.TermStartDate = '';
      this.AddSessionForm.value.TermName = '';
      this.AddSessionForm.value.TermStatus = false;
      return;
    }

    if (input.TermName == '' || input.TermStartDate == '') {
      this.toastr.error('Enter Term details properly', 'Error');
      return;
    }

    if (input.TermStatus == true) {
      var Check = this.Terms.find(function (element) {
        return element.TermStatus == true;
      });

      if (Check) {
        this.toastr.error('You cannot have more than one term active', 'Error');
        this.AddSessionForm.value.TermEndDate = '';
        this.AddSessionForm.value.TermStartDate = ''; 
        this.AddSessionForm.value.TermName = '';
        this.AddSessionForm.value.TermStatus = false;

        return;
      }
    }

    let m = Object.assign({}, input);

    this.Terms.push(m);
    Object.assign(m, {});
    this.AddSessionForm.value.TermEndDate = '';
    this.AddSessionForm.value.TermStartDate = '';
    this.AddSessionForm.value.TermName = '';
    this.AddSessionForm.value.TermStatus = false;
    
  }

  DelTerm(input) {
    var deleted = this.Terms.splice(input, 1);

  }

  onSubmit(input) {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddSessionForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddSessionForm.value);
    m.Terms = [];
    let onlyTerm: any = []
    var _term = {};
    _.each(this.Terms, function (value, key) {
        _term = {
          TermName: value.TermName,
          IsCurrent: value.TermStatus,
          StartDate: value.TermStartDate,
          EndDate: value.TermEndDate,
       };

      onlyTerm.push(_term);
    })

    m.Terms = onlyTerm;
    this.settingService.AddSession(m)
      .subscribe(data => {
        if (data.isSuccess == true) {
          this.toastr.success("Academic session Has been added Successfully", "Success!")
          this.AddSession = true
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }

      },
        error => {
          this.error = error;
          this.toastr.error("There was an error completing this request please contact an Admin", "Error!");
          this.loading = false;
        });

    this.loading = false;
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onAddSesion(this.AddSession);
  }
}

///TODO THIS IS THE BBEGINING OF ADD CLASS MODAL COMPONENT
@Component({
  selector: 'add-class-modal',
  templateUrl: './addClass.component.html',
  styleUrls: ['./academics.component.scss']
})
export class AddClassModal implements OnInit {

  AddClassForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddClass = false;

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private settingService: SettingsService,
    private toastr: ToastrService

  ) {
    this.AddClassForm = new FormGroup({
      GradeName: new FormControl(''),
      MaxNumber: new FormControl(''),
      Status: new FormControl(true),
    });

    this.formError = {
      'GradeName': '',
      'MaxNumber': '',
    }

    this.validationMessages = {
      'GradeName': {
        'required': 'Class name is required',
        'minlength': 'Class name must be at least three characters.',
        'maxlength': 'Class name cannot exceed 100 characters.'
      },
      'MaxNumber': {
        'required': 'Max No. of students in a class must be entered',
      },

    };
  }


  ngOnInit() {

    this.AddClassForm = this.formBuilder.group({
      GradeName: ['', Validators.required],
      MaxNumber: ['', Validators.required],
      Status: [true],
    })
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddClassForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddClassForm.value);

    this.settingService.AddClass(m)
      .subscribe(data => {
        if (data.isSuccess == true) {
          this.toastr.success("Student Class Has been added Successfully", "Success!")
          this.AddClass = true
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error");
        }

      },
        error => {
          this.error = error;
          this.toastr.error("There was an error completing this request please contact an Admin", "Error");
          this.loading = false;
      });

    this.loading = false;
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onClassChange(this.AddClass);
  }
}

///TODO THIS IS THE BEGINING OF ADD SUBJECT COMPONENT
@Component({
  selector: 'add-subject-modal',
  templateUrl: './addSubject.component.html',
  styleUrls: ['./academics.component.scss']
})
export class AddSubjectModal implements OnInit {
  bsValue = new Date();
  AddSubjectForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddSubject = false;
  GradeClass: {};
  SubGradeClass: ClassModel[] = [];
  SubjectGradeClass2: SubjectGradeClassModel;
  
  class_: any = {};
  Classes: [];
  p5;

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private settingService: SettingsService,
    private toastr: ToastrService
  ) {
    this.AddSubjectForm = new FormGroup({
      SubjectName: new FormControl(''),
      //GradeId: new FormControl(''),
      Status: new FormControl(''),
    });

    this.formError = {
      'SubjectName': '',
    }

    this.validationMessages = {
      'SubjectName': {
        'required': 'Subject name is required',
        'minlength': 'Subject name must be at least 3 characters.',
        'maxlength': 'Subject name is too long.'
      },
    };
  }

  GradeClases() {
    return this.settingService.GetGradeClasses().subscribe(
      data => {
        this.Classes = data.payload;
      });
  }

  onGradeClassChange(input) {
    //let class_: {}
    var plan = this.Classes.find(function (element: ClassModel) {
      return element.id == input;
    });
    
    this.class_ = Object.assign({}, {}, plan)
  }

  // fires when the add class buton is clicked
  // it adds the selected class to the table below
  addGradeClass(input) {
    var Check = this.SubGradeClass.find(function (element) {
      return element.id == input;
    });
    if (!Check) {
      this.SubGradeClass.push(this.class_);
    } else {
      this.toastr.error('This class has already been added', 'Error!')
      this.class_ = {};
      return;
    }
  }

  DelClass(input) {
    var deleted = this.SubGradeClass.splice(input, 1);
    
  }


  ngOnInit() {
    this.GradeClases();

    this.AddSubjectForm = this.formBuilder.group({
      SubjectName: ['', Validators.required],
      //GradeId: ['', Validators.required],
      Status: [true]
    });


  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddSubjectForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddSubjectForm.value);

    m.SubjectGradeClass = [];
    _.each(this.SubGradeClass, function (e, i) {

      m.SubjectGradeClass.push({
        GradeClassId: e.id
      });
      
    });

    
    this.settingService.AddSubject(m)
      .subscribe(data => {
        if (data.isSuccess == true) {
          this.toastr.success("Subject Has been added Successfully", "Success!")
          this.AddSubject = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }

      },
        error => {
          this.error = error;
          this.toastr.error("There was an error completing this request please contact an Admin", "Error!");
          this.loading = false;
        });

    this.loading = false;
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onAddSubject(this.AddSubject);
  }
}


///TODO THIS IS THE BEGINING OF ADD CLASS SECTION COMPONENT
@Component({
  selector: 'add-classsection-modal',
  templateUrl: './addClassSection.component.html',
  styleUrls: ['./academics.component.scss']
})
export class AddClassSectionModal implements OnInit {
  AddClassSectionForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  formError: { [id: string]: string };
  private validationMessages: { [id: string]: { [id: string]: string } };
  AddClassSection = false;
  GradeClass: {};
  GradeClassSubjects: [];
  //SubjectClass: {};
  ClassSection: SubjectClass[] = [];
  p7;

  // holds the list containing teacher name
  // and subject name added for the class
  subClassDetails: any = {};

  // holds permanently holds a list of all teacher and subject Id
  // that has been added it will be eventually be sent to the
  // backend and saved to subject class table
  SubClass: SubjectClass[] = [];
  SubjectGradeClass2: SubjectGradeClassModel;

  // temporily holds teacherId and subjectId
  class_: any = {};


  Classes: [];
  Teachers: [];

  constructor(
    public bsModalRef: BsModalRef,
    private formBuilder: FormBuilder,
    private settingService: SettingsService,
    private toastr: ToastrService
  ) {
    this.AddClassSectionForm = new FormGroup({
      ClassName: new FormControl(''),
      GradeClassId: new FormControl(''),
      Status: new FormControl(''),
      MaxNumber: new FormControl(''),
      ClassTeacherId: new FormControl(''),
    });

    this.formError = {
      'ClassName': '',
      'GradeClassId': '',
      'MaxNumber': '',
    }

    //this.SubjectClass = {
    //  TeacherId: 0,
    //  SubjectId: 0,
    //  ClassId: 0
    //}

    this.validationMessages = {
      'ClassName': {
        'required': 'Subject name is required',
        'minlength': 'Subject name must be at least 3 characters.',
        'maxlength': 'Subject name is too long.'
      },
      'GradeClassId': {
        'required': 'Subject name is required',
      },
      'MaxNumber': {
        'required': 'Subject name is required',
      },
    };
  }

  // getting the list of active grade classes
  // And Teachers in one server call
  GradeClaseAndTeachers() {
    return this.settingService.GetGradeClassAndTeachers().subscribe(
      data => {
        this.Classes = data.payload.gradeClasses;
        this.Teachers = data.payload.teachers;
      });
  }

  //LoadTeachers() {
  //  return this.settingService.GetGradeClasses().subscribe(
  //    data => {
  //      this.Classes = data.payload;
  //    });
  //}

  // getting the list of active subjects 
  // offered by the selected grade class
  
  GradeClasesSubjects(input) {
    return this.settingService.GetGradeClassSubjects(input).subscribe(
      data => {
        this.GradeClassSubjects = data.payload;
      });
  }

  // fires when the add Subject buton is clicked
  // it adds the selected class to the table below
  addClassSectionSubject(teacherId, subjectId) {
    var Check = this.ClassSection.find(function (element) {
      return element.SubjectId == subjectId;
    });
    if (!Check) {
      this.class_ = {};

      var _teacher: SubjectClass
      _teacher = this.Teachers.find(function (element: any) {
        return element.id == teacherId;
      });

      var _subject: SubjectClass
      _subject = this.GradeClassSubjects.find(function (element: any) {
        return element.subjectId == subjectId;
      });

      this.subClassDetails.TeacherName = _teacher.fullName;
      this.subClassDetails.SubjectName = _subject.subjectName;
      this.subClassDetails.TeacherId = teacherId;
      this.subClassDetails.SubjectId = subjectId;

      this.ClassSection.push(this.subClassDetails);

      this.subClassDetails = {}

      //this.class_.TeacherId = teacherId;
      //this.class_.SubjectId = subjectId;
      //this.SubClass.push(this.class_);
    } else {
      this.toastr.error('This class has already been added', 'Error!')
      this.class_ = {};
      return;
    }
  }

  DelClass(input) {
    var deleted = this.ClassSection.splice(input, 1);

  }


  ngOnInit() {
    this.GradeClaseAndTeachers();

    this.AddClassSectionForm = this.formBuilder.group({
      ClassName: ['', Validators.required],
      GradeClassId: ['', Validators.required],
      Status: [true],
      MaxNumber: ['', Validators.required],
      ClassTeacherId: ['']
    });


  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.AddClassSectionForm.invalid) {
      return;
    }

    this.loading = true;

    var m = Object.assign({}, {}, this.AddClassSectionForm.value);

    m.SubjectClass = [];
    _.each(this.ClassSection, function (e, i) {

      m.SubjectClass.push({
        SubjectId: e.SubjectId,
        ClassSectionId: 0,
        TeacherId: e.TeacherId

      });

    });


    this.settingService.AddClassSection(m)
      .subscribe(data => {
        if (data.isSuccess == true) {
          this.toastr.success("Class Section Has been added Successfully", "Success!")
          this.AddClassSection = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }

      },
        error => {
          this.error = error;
          this.toastr.error("There was an error completing this request please contact an Admin", "Error!");
          this.loading = false;
        });

    this.loading = false;
  }

  CloseModal() {
    this.bsModalRef.hide();
    this.settingService.onAddClassSection(this.AddClassSection);
  }
}

