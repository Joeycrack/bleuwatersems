import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcademicsComponent } from './academics/academics.component';
import { UserComponent } from './user/user.component';
import { AdminGuard } from '../../Services/AuthGuard/role-guard.service';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Settings',
      isAdmin: true
    },
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        redirectTo: 'academic',
        pathMatch: 'full',
      },
      {
        path: 'academic',
        component: AcademicsComponent,
        data: {
          title: 'Academic Settings'
        }
      },
      {
        path: 'users',
        component: UserComponent,
        data: {
          title: 'User Settings'
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SettingsRoutingModule { }
