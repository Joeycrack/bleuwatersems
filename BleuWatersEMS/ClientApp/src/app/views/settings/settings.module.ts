import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule, DatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap';
import { BsDropdownModule } from 'ngx-bootstrap';

import { SettingsRoutingModule } from './settings-routing.module';
import { AcademicsComponent, AddClassModal, AddSubjectModal, AddClassSectionModal } from './academics/academics.component';
import { UserComponent } from './user/user.component';
import { AddSessionModal } from './academics/academics.component';
import { ReactiveFormsModule, FormsModule  } from '@angular/forms';
import { DataTableModule } from 'ng-angular8-datatable';
import { DataTablesModule } from 'angular-datatables';
import { NgxPaginationModule } from 'ngx-pagination';
import { AddUserComponent } from './user/add-user/add-user.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';

@NgModule({
  declarations: [AcademicsComponent, AddSessionModal, AddClassModal, AddSubjectModal, AddClassSectionModal, UserComponent, AddUserComponent, UserDetailsComponent],
  imports: [
    CommonModule,
    SettingsRoutingModule,
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(), 
    ModalModule.forRoot(),
    ReactiveFormsModule,
    DataTableModule,
    DataTablesModule,
    NgxPaginationModule,
    BsDropdownModule,
    FormsModule 

   
  ],

  entryComponents: [AddSessionModal, AddClassModal, AddSubjectModal, AddClassSectionModal, AddUserComponent, UserDetailsComponent],
})
export class SettingsModule { }
