import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeachersDeskRoutingModule } from './teachers-desk-routing.module';
import { MyStudentsComponent } from './my-students/my-students.component';
import { MyClassesComponent } from './my-classes/my-classes.component';
import { MySubjectsComponent } from './my-subjects/my-subjects.component';
import { StudentResultsComponent } from './student-results/student-results.component';
import { RecordAssesmentComponent } from './record-assesment/record-assesment.component';
import { TeacherAttendanceComponent } from './attendance/teacher-attendance.component';
import { TeacherDashboardComponent } from './dashboard/teacher-dashboard.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { SubjectAssessmentComponent } from './record-assesment/subject-assessment/subject-assessment.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [MyStudentsComponent, MyClassesComponent, TeacherAttendanceComponent, MySubjectsComponent, StudentResultsComponent, RecordAssesmentComponent, TeacherDashboardComponent, SubjectAssessmentComponent],
  imports: [
    CommonModule,
    TeachersDeskRoutingModule,
    BsDropdownModule,
    BsDatepickerModule,
    NgxPaginationModule,
    FormsModule
  ],
  entryComponents: [SubjectAssessmentComponent],
})
export class TeachersDeskModule { }
