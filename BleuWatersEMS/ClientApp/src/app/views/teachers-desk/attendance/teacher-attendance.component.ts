import { Component, OnInit } from '@angular/core';
import { AttendanceModel } from '../../../_models/AttendanceModel';
import { AttendanceService } from '../../../Services/attendance.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { Observable } from 'rxjs';
import { User } from '../../../_models/UserModel';
import { ClassModel } from '../../../_models/ClassModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import * as _ from 'underscore';
import { GradeClassService } from '../../../Services/grade-class.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './teacher-attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class TeacherAttendanceComponent implements OnInit {
  Attendance: AttendanceModel[] = [];
  ClassSections: ClassSectionModel[] = [];
  loading = false;
  error = '';
  _attendance: {} = {};
  p1;
  constructor(private gradeClassService: GradeClassService, private attendanceService: AttendanceService, private toastr: ToastrService, private authService: AuthenticationService) { }

  CalculateAge(input) {
    input = 5;
  }

  CurrentUser: User = this.authService.currentUserValue;
  
  TeacherAttendanceToday() {
    let _user: User = this.CurrentUser;
    return this.attendanceService.TeacherAttendanceToday(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.Attendance = data.payload;
          //_.each(this.Attendance, function (value, key) {

          //  //if (new Date(value.date.getDate()) == new Date(new Date().getDate())) {
          //  value.isToday = true;
          //  //}
          //})

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  GetClasses() {
    let _user: User = this.CurrentUser;
    return this.gradeClassService.GetClasses(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSections = data.payload;
         
          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  ClassStudentsAttendance(input) {
    return this.attendanceService.ClassStudentsAttendance(input.id).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.Attendance = [];
          this.Attendance = data.payload;
          //_.each(this.Attendance, function (value, key) {

          //  if (new Date(value.date.getDate()) == new Date(new Date().getDate())) {
          //    value.isToday = true;
          //  }
          //})
          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  m: AttendanceModel;

  OnChange(input) {
    this.m = Object.assign(this._attendance, input);
    let a = !this.m.present;
    this.m.present = a;
    this.m.TeacherId = this.CurrentUser.teacherId;


    this.attendanceService.AddAttendance(this.m).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.toastr.success("Great!", "Success!");
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });

  }

  ngOnInit() {
    this.TeacherAttendanceToday();
    this.GetClasses();

  }
}
