import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyClassesComponent } from './my-classes/my-classes.component';
import { TeacherAttendanceComponent } from './attendance/teacher-attendance.component';
import { MyStudentsComponent } from './my-students/my-students.component';
import { RecordAssesmentComponent } from './record-assesment/record-assesment.component';
import { MySubjectsComponent } from './my-subjects/my-subjects.component';
import { StudentResultsComponent } from './student-results/student-results.component';
import { TeacherDashboardComponent } from './dashboard/teacher-dashboard.component';
import { TeacherGuard } from '../../Services/AuthGuard/role-guard.service';


const routes: Routes = [

  {
    path: '',
    data: {
      title: 'Teachers Desk',
      isTeacher: true
    },
    canActivate: [TeacherGuard],
    children: [
      {
        path: '',
        redirectTo: 'teacher-dashboard',
        pathMatch: 'full',
      },
      {
        path: 'teacher-dashboard',
        component: TeacherDashboardComponent,
        data: {
          title: 'My Dashboard'
        }
      },
      {
        path: 'my-classes',
        component: MyClassesComponent,
        data: {
          title: 'My Classes'
        }
      },
      {
        path: 'attendance',
        component: TeacherAttendanceComponent,
        data: {
          title: 'My Student Attendance'
        }
      },
      {
        path: 'my-students',
        component: MyStudentsComponent,
        data: {
          title: 'My Student List'
        }
      },
      {
        path: 'my-subjects',
        component: MySubjectsComponent,
        data: {
          title: 'My List of Sujects'
        }
      },
      {
        path: 'record-assesment',
        component: RecordAssesmentComponent,
        data: {
          title: 'Record Student Assesment'
        }
      },
      {
        path: 'results',
        component: StudentResultsComponent,
        data: {
          title: 'My Student Results'
        }
      },
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeachersDeskRoutingModule { }
