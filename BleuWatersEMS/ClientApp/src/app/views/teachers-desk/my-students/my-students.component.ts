import { Component, OnInit } from '@angular/core';
import { GradeClassService } from '../../../Services/grade-class.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { User } from '../../../_models/UserModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import { TeacherService } from '../../../Services/teacher.service';

@Component({
  selector: 'app-my-students',
  templateUrl: './my-students.component.html',
  styleUrls: ['./my-students.component.scss']
})
export class MyStudentsComponent implements OnInit {

  constructor(private gradeClassService: GradeClassService, private teacherService: TeacherService, private toastr: ToastrService, private authService: AuthenticationService) { }

  CurrentUser: User = this.authService.currentUserValue;
  ClassSections: ClassSectionModel[] = [];
  Students: [] = [];
  error: Error;
  loading;

  GetClasses() {
    let _user: User = this.CurrentUser;
    return this.gradeClassService.GetClasses(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSections = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  GetStudents(input) {
    let _user: User = this.CurrentUser;
    return this.teacherService.GetTeacherStudents(input.id, _user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.Students = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
    

  }

  ngOnInit() {
    this.GetClasses();

  }

}
