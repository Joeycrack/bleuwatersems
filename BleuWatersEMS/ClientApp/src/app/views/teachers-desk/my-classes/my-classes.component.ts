import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { User } from '../../../_models/UserModel';
import { ClassSectionModel, TeacherClassSectionModel } from '../../../_models/ClassSectionModel';
import { GradeClassService } from '../../../Services/grade-class.service';
import { TeacherService } from '../../../Services/teacher.service';

@Component({
  selector: 'app-my-classes',
  templateUrl: './my-classes.component.html',
  styleUrls: ['./my-classes.component.scss']
})
export class MyClassesComponent implements OnInit {

  constructor(private teacherServices: TeacherService, private toastr: ToastrService, private authService: AuthenticationService) { }

  CurrentUser: User = this.authService.currentUserValue;
  ClassSections: TeacherClassSectionModel[] = [];
  error: Error;
  loading;

  GetTeacherClasses() {
    let teacherId = this.CurrentUser.teacherId;

    return this.teacherServices.GetTeacherClasses(teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSections = data.payload;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });

  }

  ngOnInit() {
    this.GetTeacherClasses();
    
  }

}
