import { Component, OnInit } from '@angular/core';
import { User } from '../../../../_models/UserModel';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../../Services/authentication.service';
import { AssessmestsService } from '../../../../Services/assessmests.service';
import { AssessMentModel, AssessmentReturnModel } from '../../../../_models/AssessmentModel';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-subject-assessment',
  templateUrl: './subject-assessment.component.html',
  styleUrls: ['./subject-assessment.component.scss']
})
export class SubjectAssessmentComponent implements OnInit {

  constructor(private assessmentService: AssessmestsService, public bsModalRef: BsModalRef,  private toastr: ToastrService, private authService: AuthenticationService) { }
  CurrentUser: User = this.authService.currentUserValue;
  ClassAssessment: AssessMentModel[] = [];
  error: Error;
  loading;
  // initialState
  classId: 0;
  subjectId: 0;
  className: string;
  subjectName: string;
  
  GetSubjectClassAssesmts(classId, subjectId) {
    let _user: User = this.CurrentUser;
    return this.assessmentService.GetSubjectClassAssessment(classId, subjectId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.className = data.payload.className;
          this.subjectName = data.payload.subjectName
          this.ClassAssessment = data.payload.assesments;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  SubmitAssessment(input) {
    return this.assessmentService.SubmitAssessmentRecord(input).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.access = data.payload
          this.toastr.success("Assessment submitted successfully", "Success!");
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  saveRecord(input: AssessMentModel) {
    let _user: User = this.CurrentUser;
    input.RecordedBy = _user.teacherId;
    return this.assessmentService.SaveAssessmentRecord(input).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.access = data.payload
          this.toastr.success("Assessment Saved", "Success!");
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });

  }

  CloseModal() {
    this.bsModalRef.hide();
  }

  ngOnInit() {
    this.GetSubjectClassAssesmts(this.classId, this.subjectId);
  }
}
