import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { AssessmestsService } from '../../../Services/assessmests.service';
import { User } from '../../../_models/UserModel';
import { GradeClassService } from '../../../Services/grade-class.service';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import { SubjectTeacherModel } from '../../../_models/SubjectGradeClassModel';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { SubjectAssessmentComponent } from './subject-assessment/subject-assessment.component';

@Component({
  selector: 'app-record-assesment',
  templateUrl: './record-assesment.component.html',
  styleUrls: ['./record-assesment.component.scss']
})
export class RecordAssesmentComponent implements OnInit {
  
  constructor(private modalService: BsModalService, private gradeClassService: GradeClassService, private assessmentService: AssessmestsService, private toastr: ToastrService, private authService: AuthenticationService) { }
  CurrentUser: User = this.authService.currentUserValue;
  ClassSections: ClassSectionModel[] = [];
  error: Error;
  loading;
  TeacherSubjects: SubjectTeacherModel[] = [];
  bsModalRef: BsModalRef;

  addAssessmentModal(input) {

    this.bsModalRef = this.modalService.show(SubjectAssessmentComponent, Object.assign({}, { class: 'modal-lg', backdrop: true, ignoreBackdropClick: true, initialState: input }))
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  GetClasses() {
    let _user: User = this.CurrentUser;
    return this.gradeClassService.SubjectTeacherClasses(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSections = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  GetTeacherSubjects(classId) {
    let _user: User = this.CurrentUser;
    return this.assessmentService.GetTeacherSubjects(_user.teacherId, classId.id).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.TeacherSubjects = data.payload;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  ngOnInit() {
    this.GetClasses();
  }

}
