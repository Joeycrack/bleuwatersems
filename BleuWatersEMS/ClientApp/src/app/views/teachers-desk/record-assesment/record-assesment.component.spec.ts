import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecordAssesmentComponent } from './record-assesment.component';

describe('RecordAssesmentComponent', () => {
  let component: RecordAssesmentComponent;
  let fixture: ComponentFixture<RecordAssesmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecordAssesmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecordAssesmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
