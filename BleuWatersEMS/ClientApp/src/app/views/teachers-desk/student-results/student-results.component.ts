import { Component, OnInit } from '@angular/core';
import { AssessmestsService } from '../../../Services/assessmests.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { GradeClassService } from '../../../Services/grade-class.service';
import { User } from '../../../_models/UserModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';
import { SubjectTeacherModel } from '../../../_models/SubjectGradeClassModel';

@Component({
  selector: 'app-student-results',
  templateUrl: './student-results.component.html',
  styleUrls: ['./student-results.component.scss']
})
export class StudentResultsComponent implements OnInit {

  constructor(private modalService: BsModalService, private gradeClassService: GradeClassService, private assessmentService: AssessmestsService, private toastr: ToastrService, private authService: AuthenticationService) { }

  CurrentUser: User = this.authService.currentUserValue;
  ClassSections: ClassSectionModel[] = [];
  error: Error;
  loading;
  ClassSubjects: SubjectTeacherModel[] = [];
  bsModalRef: BsModalRef;

  //addAssessmentModal(input) {

  //  this.bsModalRef = this.modalService.show(SubjectAssessmentComponent, Object.assign({}, { class: 'modal-lg', backdrop: true, ignoreBackdropClick: true, initialState: input }))
  //  this.bsModalRef.content.closeBtnName = 'Close';
  //}

  GetClasses() {
    let _user: User = this.CurrentUser;
    return this.gradeClassService.GetClasses(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSections = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  GetClassSubjects(classId) {
    return this.assessmentService.GetClassSubjects(classId.id).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.ClassSubjects = data.payload;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  ngOnInit() {
    this.GetClasses();

  }

}
