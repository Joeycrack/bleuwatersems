import { Component, OnInit } from '@angular/core';
import { SubjectService } from '../../../Services/subject.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../../../Services/authentication.service';
import { User } from '../../../_models/UserModel';
import { ClassSectionModel } from '../../../_models/ClassSectionModel';

@Component({
  selector: 'app-my-subjects',
  templateUrl: './my-subjects.component.html',
  styleUrls: ['./my-subjects.component.scss']
})
export class MySubjectsComponent implements OnInit {

  constructor(private subjectService: SubjectService, private toastr: ToastrService, private authService: AuthenticationService) { }
  CurrentUser: User = this.authService.currentUserValue;
  TeacherSubjects: [] = [];
  error: Error;
  loading;

  GetTeacherSubject() {
    let _user: User = this.CurrentUser;
    return this.subjectService.GetTeacherSubject(_user.teacherId).subscribe(
      data => {
        if (data.isSuccess == true) {
          this.TeacherSubjects = data.payload;

          //this.AddUser = true;
          return;
        } else if (data.isSuccess == false) {
          this.toastr.error(data.errorDetails, "Error!");
        }
      },
      error => {
        this.error = error;
        this.loading = false;
      });
  }

  ngOnInit() {
    this.GetTeacherSubject();

  }

}
