import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffQueryComponent } from './staff-query.component';

describe('StaffQueryComponent', () => {
  let component: StaffQueryComponent;
  let fixture: ComponentFixture<StaffQueryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffQueryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffQueryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
