import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaffManagementRoutingModule } from './staff-management-routing.module';
import { StaffAttendanceComponent } from './staff-attendance/staff-attendance.component';
import { TeacherPerformanceComponent } from './teacher-performance/teacher-performance.component';
import { AnnouncementComponent } from './announcement/announcement.component';
import { StaffQueryComponent } from './staff-query/staff-query.component';
import { TeacherProfilesComponent } from './teacher-profiles/teacher-profiles.component';
import { LeaveManagementComponent } from './leave-management/leave-management.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { BsDatepickerModule } from 'ngx-bootstrap';



@NgModule({
  declarations: [
    StaffAttendanceComponent,
    TeacherPerformanceComponent,
    AnnouncementComponent,
    StaffQueryComponent,
    TeacherProfilesComponent,
    LeaveManagementComponent,],

  imports: [
    CommonModule,
    StaffManagementRoutingModule,
    BsDropdownModule,
    BsDatepickerModule

  ]
})
export class StaffManagementModule { }
