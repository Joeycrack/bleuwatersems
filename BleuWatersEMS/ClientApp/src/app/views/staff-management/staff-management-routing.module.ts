import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StaffAttendanceComponent } from './staff-attendance/staff-attendance.component';
import { TeacherPerformanceComponent } from './teacher-performance/teacher-performance.component';
import { AnnouncementComponent } from './announcement/announcement.component';
import { StaffQueryComponent } from './staff-query/staff-query.component';
import { TeacherProfilesComponent } from './teacher-profiles/teacher-profiles.component';
import { LeaveManagementComponent } from './leave-management/leave-management.component';
import { AdminGuard } from '../../Services/AuthGuard/role-guard.service';


const routes: Routes = [
  {
    path: '',
    data: {
      title: ' Human Resource Desk (HR)',
      isAdmin: true
    },
    canActivate: [AdminGuard],
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
      },
      {
        path: 'attendance',
        component: StaffAttendanceComponent,
        data: {
          title: 'Staff Attendance'
        }
      },
      {
        path: 'teacher-performance',
        component: TeacherPerformanceComponent,
        data: {
          title: 'Teacher performance'
        }
      },
      {
        path: 'announcements',
        component: AnnouncementComponent,
        data: {
          title: 'Manage Announcements'
        }
      },
      {
        path: 'staff-query',
        component: StaffQueryComponent,
        data: {
          title: 'Manage Staff Query'
        }
      },
      {
        path: 'Staff-Profile',
        component: TeacherProfilesComponent,
        data: {
          title: 'Staff Profile Management'
        }
      },
      {
        path: 'staff-leave',
        component: LeaveManagementComponent,
        data: {
          title: 'Manage Staff Leave'
        }
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StaffManagementRoutingModule { }
