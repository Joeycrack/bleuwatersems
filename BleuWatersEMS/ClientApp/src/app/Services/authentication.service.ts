import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from '../../app/_models/UserModel'
import { LoginModel } from '../_models/LoginModel';
import { HandleErrorsService } from './handle-errors.service';
import { RegisterModel } from '../_models/RegisterModel';
import { __await } from 'tslib';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient, private _handleError: HandleErrorsService) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public get UserToken(): User {
    return this.currentUserSubject.value;
  }


  login(input: LoginModel): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}Account/Login`, input)
      .pipe(map(user => {
        // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
        //user.authdata = window.btoa(Payload.user.Username + ':' + input.Password);
        if (user.isSuccess == true) {
          sessionStorage.setItem('currentUser', JSON.stringify(user.payload));
          localStorage.setItem('currentUser', JSON.stringify(user.payload));
          this.currentUserSubject.next(user.payload);
          return user;
        }

        if (user.isSuccess == false) {
          return user;
        }
      })
      //catchError(this._handleError.handleErrors('login', input))
    );
  }

  register(input: RegisterModel) {
    return this.http.post<any>(`${environment.apiUrl}Account/Register`, input)
      .pipe(map(res => {
        return res;
      })
        //catchError(this._handleError.handleErrors('login', input))
      );
  }

  addUser(input) {
    return this.http.post<any>(`${environment.apiUrl}Account/AddUser`, input)
      .pipe(map(res => {
        return res;
      })
        //catchError(this._handleError.handleErrors('login', input))
      );
  }

  logout(): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}Account/Logout`)
      .pipe(map(user => {
        // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
        //user.authdata = window.btoa(Payload.user.Username + ':' + input.Password);
        localStorage.removeItem('currentUser');
        sessionStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
        return user;
      })
      );


    //let logout = this.http.get(`${environment.apiUrl}Account/Logout`);
    //  localStorage.removeItem('currentUser');
    //this.currentUserSubject.next(null);
    //return logout;
    //// remove user from local storage to log user out
    ////return this.http.get(`${environment.apiUrl}Account/Logout`);
  }
}
