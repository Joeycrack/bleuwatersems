import { TestBed } from '@angular/core/testing';

import { AssessmestsService } from './assessmests.service';

describe('AssessmestsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssessmestsService = TestBed.get(AssessmestsService);
    expect(service).toBeTruthy();
  });
});
