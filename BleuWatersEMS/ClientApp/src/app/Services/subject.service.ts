import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleErrorsService } from './handle-errors.service';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  constructor(private http: HttpClient, private _handleError: HandleErrorsService) { }

  GetTeacherSubject(input): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}Subject/TeacherSubjectsList/` + input)
      .pipe(map(sections => {
        return sections
      }));
  }
}
