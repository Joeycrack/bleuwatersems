import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../authentication.service';
import { User } from '../../_models/UserModel';


@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  constructor(private _authService: AuthenticationService, private _router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const CurrentUser = this._authService.currentUserValue;

    if (CurrentUser == null || CurrentUser.isAdmin === null) {
      this._router.navigate(['/login']);
      return false;
    }

    if (CurrentUser.isAdmin === next.data.isAdmin) {
      //this._router.navigate(['/dashboard']);
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/login']);
    return false;
  }
}


@Injectable({
  providedIn: 'root'
})
export class TeacherGuard implements CanActivate {

  constructor(private _authService: AuthenticationService, private _router: Router) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    const CurrentUser: User = this._authService.currentUserValue;

    if (CurrentUser == null || CurrentUser.isTeacher === null) {
      this._router.navigate(['/login']);
      return false;
    }

    if (CurrentUser.isTeacher === next.data.isTeacher) {
      //this._router.navigate(['/teacher/teacher-dashboard']);
      return true;
    }

    // navigate to not found page
    this._router.navigate(['/login']);
    return false;
  }
}
