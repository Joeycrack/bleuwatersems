import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HandleErrorsService } from './handle-errors.service';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
import { AttendanceModel } from '../_models/AttendanceModel';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(private http: HttpClient, private _handleError: HandleErrorsService) { }


  AddStudentAttendance

  GetStudentsForAttendance(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}StudentAttendance/GetStudentsForAttendance`)
      .pipe(map(data => {
        return data
      }),
      catchError(this._handleError.handleErrors('GetStudentsForAttendance'))
      );
  }

  AddAttendance(input): Observable<any> {

    return this.http.post<any>(`${environment.apiUrl}StudentAttendance/AddStudentAttendance`, input)
      .pipe(map(attendance => {
        return attendance;
      }),
        catchError(this._handleError.handleErrors('AddStudent', input))
      );
  }

  GetTodayAttendance(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}StudentAttendance/GetTodayAttendance`)
      .pipe(map(data => {
        return data
      }),
        catchError(this._handleError.handleErrors('GetStudentsForAttendance'))
      );
  }

  TeacherAttendanceToday(teacherId): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}StudentAttendance/TeacherAttendanceToday/` + teacherId)
      .pipe(map(data => {
        return data
      }))
  }

  ClassStudentsAttendance(input): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}StudentAttendance/ClassStudentsAttendance/` + input)
      .pipe(map(sections => {
        return sections
      }));
  }
}
