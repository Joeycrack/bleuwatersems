import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HandleErrorsService } from './handle-errors.service';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient, private authenticationService: AuthenticationService, private _handleError: HandleErrorsService) { }
  headers = new Headers();
  
  authToken = this.authenticationService.UserToken;
  //this.headers.append('Authorization', `Bearer ${authToken}`);


  AddStudent(input: any): Observable<any> {

    return this.http.post<any>(`${environment.apiUrl}Student/AddStudent`, input)
      .pipe(map(gradeClass => {
        return gradeClass
      }),
      catchError(this._handleError.handleErrors('AddStudent', input))
      );
  }

  StudentInitialLists(): Observable<any> {
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json');
    let authToken = this.authenticationService.UserToken;
    headers.set('Authorization', `Bearer ${authToken}`);
    //this.headers.append('Authorization', `Bearer ${this.authToken}`);
    return this.http.get<any>(`${environment.apiUrl}Student/AddStudentInitial`, { headers })
      .pipe(map(initialList => {
        return initialList
      }),
        catchError(this._handleError.handleErrors('AddStudent', ''))
      );
  }

  GetStudents() {
    return this.http.get<any>(`${environment.apiUrl}Student/GetStudentList/`)
      .pipe(map(student => {
        return student
      }));
  }
}
