import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HandleErrorsService } from './handle-errors.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AssessmestsService {

  constructor(private http: HttpClient, private _handleError: HandleErrorsService) { }

  GetTeacherSubjects(teacherId, classId): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}Subject/GetTeacherSubjects/` + teacherId + '/' + classId)
      .pipe(map(sections => {
        return sections
      }));
  }

  GetClassSubjects(classId): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}Subject/GetClassSubjects/` + classId)
      .pipe(map(sections => {
        return sections
      }));
  }

  GetSubjectClassAssessment(classId, subjectId): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}StudentAssessment/GetClassSubjectAssessment/` + classId + '/' + subjectId)
      .pipe(map(assessment => {
        return assessment
      }));
  }

  SaveAssessmentRecord(input): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}StudentAssessment/AddStudentAssesmts`, input)
      .pipe(map(assessment => {
        return assessment;
      }),
      catchError(this._handleError.handleErrors('SaveAssessmentRecord', input))
      );
  }

  SubmitAssessmentRecord(input): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}StudentAssessment/SubmitStudentAssesmts`, input)
      .pipe(map(assessment => {
        return assessment;
      }),
      catchError(this._handleError.handleErrors('SubmitAssessmentRecord', input))
      );
  }
}
