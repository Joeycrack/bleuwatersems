import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HandleErrorsService } from './handle-errors.service';

@Injectable({
  providedIn: 'root'
})
export class GradeClassService {

  constructor(private http: HttpClient, private _handleError: HandleErrorsService) { }

  GetClasses(input): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}ClassSection/GetTeacherClassSections/` + input)
      .pipe(map(sections => {
        return sections
      }));
  }

  SubjectTeacherClasses(input): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}ClassSection/TeacherClassDropDwn/` + input)
      .pipe(map(sections => {
        return sections
      }));
  }
}
