import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ClassModel } from '../_models/ClassModel';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HandleErrorsService } from './handle-errors.service';
import { Subject } from 'rxjs';
import { SessionModel } from '../_models/SessionModel';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {


  constructor(private http: HttpClient, private _handleError: HandleErrorsService) { }

 
  AddClass(input: ClassModel): Observable<ClassModel> {

    return this.http.post<any>(`${environment.apiUrl}GradeClass/AddGradeClass`, input)
      .pipe(map(gradeClass => {
        return gradeClass
      }),
        catchError(this._handleError.handleErrors('AddClass', input))
     );
  }

  GetGradeClasses(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}GradeClass/GetGradeClasses`,)
      .pipe(map(gradeClass => {
        return gradeClass
      }),
        catchError(this._handleError.handleErrors('AddClass'))
      );
  }

  GetSession(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}Session/GetSessions`)
      .pipe(map(sessions => {
        return sessions
      }),
        catchError(this._handleError.handleErrors('GetSession'))
      );
  }

  AddSession(input: SessionModel): Observable<SessionModel> {

    return this.http.post<any>(`${environment.apiUrl}Session/AddSession`, input)
      .pipe(map(sessions => {
        return sessions
      }),
      catchError(this._handleError.handleErrors('AddSession'))
      );
  }

  AddSubject(input): Observable<any> {

    return this.http.post<any>(`${environment.apiUrl}Subject/AddSubject`, input)
      .pipe(map(subjects => {
        return subjects
      }),
      catchError(this._handleError.handleErrors('AddSubject'))
      );
  }

  GetSubject(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}Subject/GetSubjects`)
      .pipe(map(subjects => {
        return subjects
      }),
      catchError(this._handleError.handleErrors('GetSubject'))
      );
  }

  GetGradeClassSubjects(input): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}Subject/GetGradeClassSubjects/` + input)
      .pipe(map(subjects => {
        return subjects
      }),
      catchError(this._handleError.handleErrors('GetGradeClassSubject'))
      );
  }

  AddClassSection(input): Observable<any> {

    return this.http.post<any>(`${environment.apiUrl}ClassSection/AddClassSection`, input)
      .pipe(map(sections => {
        return sections
      }),
      catchError(this._handleError.handleErrors('AddClassSection'))
      );
  }

  GetClassSections(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}ClassSection/GetClassSections`)
      .pipe(map(sections => {
        return sections
      }),
      catchError(this._handleError.handleErrors('GetClassSections'))
      );
  }

  GetGradeClassAndTeachers(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}GradeClass/GetGradeClassesAndTeachers`)
      .pipe(map(GradeClassAndTeachers => {
        return GradeClassAndTeachers
      }),
        catchError(this._handleError.handleErrors('GetGradeClassSubject'))
      );
  }

  // Observable: watches for changes to interface tables
  // And refreshes the tables accordingly upon closing modal

  private ClassAdded = new Subject<boolean>();
  private SessionAdded = new Subject<boolean>();
  private SubjectAdded = new Subject<boolean>();
  private ClassSectionAdded = new Subject<boolean>();

  // the various components subscribes to these Observables
  // And use their values to to update the tables accordingly

  ClassAdd$ = this.ClassAdded.asObservable();
  SessionAdd$ = this.SessionAdded.asObservable();
  SubjectAdd$ = this.SubjectAdded.asObservable();
  ClassSectionAdd$ = this.ClassSectionAdded.asObservable();

  // Service message commands

  onClassChange(input: boolean) {
    this.ClassAdded.next(input);
  }

  onAddSesion(input: boolean) {

    this.SessionAdded.next(input);
}

  onAddSubject(input: boolean) {

    this.SubjectAdded.next(input);
  }

  onAddClassSection(input: boolean) {

    this.ClassSectionAdded.next(input);
  }

  // User Settings

  GetUsers(): Observable<any> {

    return this.http.get<any>(`${environment.apiUrl}Account/GetUsers`)
      .pipe(map(data => {
        return data
      }),
        catchError(this._handleError.handleErrors('GetUsers'))
      );
  }
    // Observable: watches for changes to interface tables
    // And refreshes the tables accordingly upon closing modal

  private UserAdded = new Subject<boolean>();
  private ViewUser = new Subject<{}>();


    // the various components subscribes to these Observables
   // And use their values to to update the tables accordingly

  UserAdd$ = this.UserAdded.asObservable();
  ViewUser$ = this.ViewUser.asObservable();

  onAddUser(input: boolean) {

    this.UserAdded.next(input);
  }

  onViewUser(input) {

    this.ViewUser.next(input);
  }
}
