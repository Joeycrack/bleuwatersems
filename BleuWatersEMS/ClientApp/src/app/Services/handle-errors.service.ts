import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class HandleErrorsService {

  constructor(private toastr: ToastrService) { }

  handleErrors<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error.message)
      this.errror = error.message;
      if (error.status == 401) {
        this.toastr.error("You not are authorised to access this resources", error.statusText)
        return;
      }

      this.error = error.message;
      if (error.error.message === undefined || error.error.message === null) {
        this.Error();
      }
      else {
        this.showError();
      }
      return of(result as T);

    };
  };

  public error: string
  public errror: string;

  showError() {
    this.toastr.error(`${this.errror}`, 'Error', {
      timeOut: 3000
    });
  }

  Error() {
    this.toastr.error(`${this.error}`, 'Error', {
      timeOut: 3000
    });
  }
}
