import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { Observable } from 'rxjs';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor  {

  constructor(private authenticationService: AuthenticationService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (request.url == "api/Account/Login") {
      return next.handle(request);
    }
    // add authorization header with basic auth credentials if available
    const UserToken = this.authenticationService.UserToken;
    if (UserToken.token || UserToken != null) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${UserToken.token}`,
          TenantId: UserToken.tenantId.toString()
        }
      });
    }

    return next.handle(request);
  }
}
