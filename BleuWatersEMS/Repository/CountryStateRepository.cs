﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class CountryStateRepository : ICountryStateRepository
    {
        private DataContext _context;
        public CountryStateRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Country>> GetCountries()
        {
            var countries = await _context.Countries.AsNoTracking().ToListAsync();
            return countries;
        }

        public async Task AddCountry(Country country)
        {
            _context.Countries.Add(country);
            await _context.SaveChangesAsync();
        }


        // start state Repoo
        public async Task AddState(State state)
        {
            _context.States.Add(state);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<State>> GetStates()
        {
            var states = await _context.States.ToListAsync();
            return states;
        }
    }

    public interface ICountryStateRepository
    {
        Task AddCountry(Country country);
        Task<IEnumerable<Country>> GetCountries();
        Task<IEnumerable<State>> GetStates();
        Task AddState(State state);
    }
}
