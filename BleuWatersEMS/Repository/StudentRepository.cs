﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Infrastructure.Extentions;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private DataContext _context;
        public StudentRepository(DataContext context)
        {
            _context = context;
        }

        public async Task AddStudent (StudentRegistrationViewModel studentRegView, int TenantId)
        {
            studentRegView.TenantId = TenantId;
            Parent parent = studentRegView.ConvertToParent();
            _context.Parents.Add(parent);
            await _context.SaveChangesAsync();

            Guardian guardian = new Guardian();
            if (studentRegView.GuardianFirstName != "")
            {
                 guardian = studentRegView.ConvertToGuardian();
                _context.Guardians.Add(guardian);
                await _context.SaveChangesAsync();
            }

            Student student = studentRegView.ConvertToStudent();
            student.ParentId = parent.ID;
            if (guardian != null && guardian.ID > 0)
            {
                student.GuardianId = guardian.ID;
            }

            _context.Students.Add(student);
            await _context.SaveChangesAsync();
            
            var subjects = await _context.SubjectClasses.Where(e => e.ClassId == student.ClassId).ToListAsync();

            List<StudentSubjectViewModel> studentSubjectVM = new List<StudentSubjectViewModel>().ConvertToStudentSubjectVM(subjects);
            studentSubjectVM.ForEach(r => r.StudentId = student.ID);

            List<StudentSubject> studentSubject = new List<StudentSubject>().ConvertToStudentSubject(studentSubjectVM, student);
            studentSubject.ForEach(r => r.TenantId = TenantId);

            //foreach (int element in fibNumbers)
            //{
            //    count++;
            //    Console.WriteLine($"Element #{count}: {element}");
            //}

            _context.StudentSubjects.AddRange(studentSubject);
        }

        public async Task<IEnumerable<Student>> GetStudents(int TenantId)
        {
            var students = await _context.Students.Where(e => e.Active == true && e.TenantId == TenantId).ToListAsync();
            return students;
        }

        public async Task<IEnumerable<StudentViewModel>> GetStudentList(int TenantId)
        {
            var students = await _context.Students.Where(e => e.Active == true && e.TenantId == TenantId)
                .Select(y => new StudentViewModel()
                {
                    StudentName = y.FirstName + " " + y.MiddleName + " " + y.LastName,
                    Sex = y.Sex,
                    ClassName = _context.Classes.AsNoTracking().Where(r => r.ID == y.ClassId && r.TenantId == TenantId && r.Status == true).Select(t => t.ClassName).FirstOrDefault(),
                    StudentId = y.ID,
                    GradeClassId =y.GradeClassId,
                    ClassId = y.ClassId,
                    RegistrationDate = DateTime.UtcNow,
                    ClassTeacherName = _context.Teachers.AsNoTracking().Where(r => r.ID == _context.Classes.AsNoTracking().
                                        Where(p => p.ID == y.ClassId && p.TenantId == TenantId && p.Status == true).Select(t => t.TeacherId).FirstOrDefault() 
                                        && r.TenantId == TenantId).Select(u => u.FirstName +" "+ u.LastName).FirstOrDefault(),
                    DateOfBirth = y.DateOfBirth
                }).ToListAsync();

            foreach (StudentViewModel std in students)
            {
                var today = DateTime.Today;
                var age = today.Year - std.DateOfBirth.Year;
                if (std.DateOfBirth > today.AddYears(-age))
                {
                    std.Age = age--;
                }
                else
                {
                    std.Age = age;
                }

            }
            return students;
        }

        public async Task<IEnumerable<Student>> GetClassStudents(int classId, int TenantId)
        {
            var students = await _context.Students.Where(e => e.Active == true && e.ClassId == classId).ToListAsync();
            return students;
        }
        
    }

    public interface IStudentRepository
    {
        Task AddStudent(StudentRegistrationViewModel studentRegView, int TenantId);
        Task<IEnumerable<Student>> GetStudents(int TenantId);
        Task<IEnumerable<Student>> GetClassStudents(int classId, int TenantId);
        Task<IEnumerable<StudentViewModel>> GetStudentList(int TenantId);
    }
}
