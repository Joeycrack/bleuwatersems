﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class SujectRepository : ISubjectRepository
    {
        private DataContext _context;
        public SujectRepository(DataContext context)
        {
            _context = context;
        }

        public async Task AddSubject(SubjectViewModel subjectview, int TenantId)
        {

            Subject subject = new Subject();
            subject.ID = subjectview.Id;
            subject.subjectName = subjectview.subjectName;
            subject.Status = subjectview.Status;
            subject.TenantId = TenantId;
            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();

            subjectview.SubjectGradeClass.ForEach(r => r.SubjectId = subject.ID);
            _context.SubjectGradeClasses.AddRange(subjectview.SubjectGradeClass);
            subjectview.SubjectGradeClass.ForEach(r => r.TenantId = TenantId);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Subject>> GetSubjects(int TenantId)
        {
            var subjectss = await _context.Subjects.Where(e => e.Status == true && e.TenantId == TenantId).ToListAsync();
            return subjectss;
        }

        public async Task<IEnumerable<SubjectGradeClassViewModel>> GetGradeClassSubjects(int Id, int TenantId)
        {
            var subjectss = await _context.SubjectGradeClasses.Where(e => e.GradeClassId == Id && e.TenantId == TenantId)
                .Select(r => new SubjectGradeClassViewModel
                { 
                    SubjectId = r.SubjectId,
                    SubjectName = _context.Subjects.Where(m => m.ID == r.SubjectId).Select(y => y.subjectName).FirstOrDefault(),
                    GradeClassId = r.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(m => m.ID == r.GradeClassId).Select(y => y.GradeName).FirstOrDefault()
                }).ToListAsync();
            return subjectss;
        }

        public async Task<IEnumerable<SubjectTeacherViewModel>> GetTeacherClassSubjects(int teacherId, int classId, int TenantId)
        {
            var subjectss = await _context.SubjectClasses.Where(e => e.TeacherId == teacherId && e.ClassId == classId && e.TenantId == TenantId)
                .Select(r => new SubjectTeacherViewModel
                {
                    SubjectId = r.SubjectId,
                    SubjectName = _context.Subjects.Where(m => m.ID == r.SubjectId).Select(y => y.subjectName).FirstOrDefault(),
                    ClassId = r.ClassId,
                    ClassName = _context.Classes.Where(m => m.ID == r.ClassId).Select(y => y.ClassName).FirstOrDefault(),
                    NumOfStudent = _context.Students.Where(q => q.ClassId == r.ClassId && q.TenantId == TenantId).Count(),
                    SessionTerm = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p => 
                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " + 
                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault() 
                    && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")"
                }).ToListAsync();
            return subjectss;
        }

        class ItemEqualityComparer : IEqualityComparer<SubjectTeacherViewModel>
        {
            public bool Equals(SubjectTeacherViewModel x, SubjectTeacherViewModel y)
            {
                // Two items are equal if their keys are equal.
                return x.SubjectName == y.SubjectName;
            }

            public int GetHashCode(SubjectTeacherViewModel obj)
            {
                return obj.SubjectName.GetHashCode();
            }
        }

        public async Task<IEnumerable<SubjectTeacherViewModel>> GetClassSectionSubjects(int classId, int TenantId)
        {
            var subjectss = await _context.SubjectClasses.Where(e => e.ClassId == classId && e.TenantId == TenantId)
                .Select(r => new SubjectTeacherViewModel
                {
                    SubjectId = r.SubjectId,
                    SubjectName = _context.Subjects.Where(m => m.ID == r.SubjectId).Select(y => y.subjectName).FirstOrDefault(),
                    ClassId = r.ClassId,
                    ClassName = _context.Classes.Where(m => m.ID == r.ClassId).Select(y => y.ClassName).FirstOrDefault(),
                    NumOfStudent = _context.Students.Where(q => q.ClassId == classId && q.TenantId == TenantId).Count(),
                    SessionTerm = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p =>
                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " +
                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault()
                    && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")"
                }).ToListAsync();

            var result = subjectss.Distinct(new ItemEqualityComparer());
            return result;
        }

        public async Task<IEnumerable<SubjectTeacherViewModel>> TeacherSubjectsList(int teacherId, int TenantId)
        {
            var subjectss = await _context.SubjectClasses.Where(e => e.TeacherId == teacherId && e.TenantId == TenantId)
                .Select(r => new SubjectTeacherViewModel
                {
                    SubjectId = r.SubjectId,
                    SubjectName = _context.Subjects.Where(m => m.ID == r.SubjectId).Select(y => y.subjectName).FirstOrDefault(),
                    ClassId = r.ClassId,
                    ClassName = _context.Classes.Where(m => m.ID == r.ClassId).Select(y => y.ClassName).FirstOrDefault(),
                    NumOfStudent = _context.StudentSubjects.Where(q => q.TeacherId == teacherId && q.SubjectId == r.SubjectId && q.TenantId == TenantId).Count(),
                    SessionTerm = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p =>
                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " +
                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault()
                    && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")"
                }).ToListAsync();
            return subjectss;
        }

    }

    public interface ISubjectRepository
    {
        Task AddSubject(SubjectViewModel subjectview, int TenantId);
        Task<IEnumerable<Subject>> GetSubjects(int TenantId);
        Task<IEnumerable<SubjectGradeClassViewModel>> GetGradeClassSubjects(int Id, int TenantId);
        Task<IEnumerable<SubjectTeacherViewModel>> GetTeacherClassSubjects(int teacherId, int classId, int TenantId);
        Task<IEnumerable<SubjectTeacherViewModel>> TeacherSubjectsList(int teacherId, int TenantId);
        Task<IEnumerable<SubjectTeacherViewModel>> GetClassSectionSubjects(int classId, int TenantId);
    }
}
