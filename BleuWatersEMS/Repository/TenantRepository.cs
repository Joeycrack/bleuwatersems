﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class TenantRepository: ITenantRepository
    {
        private DataContext _context;
        public TenantRepository(DataContext context)
        {
            _context = context;

        }

        public async Task AddTenant(Tenant tenant)
        {
            _context.Tenants.Add(tenant);
            await _context.SaveChangesAsync();
        }

        public async Task GetTenant(int tenantId)
        {
            var planId = _context.Tenants.Where(e => e.ID == tenantId).Select(r => r.PlanTypeId);
            await _context.SaveChangesAsync();
        }

        public string GetTenantPlanName(int tenantId)
        {
            var planId = _context.Tenants.Where(e => e.ID == tenantId).Select(r => r.PlanTypeId).FirstOrDefault();
            var planName = _context.PlanTypes.Where(r => r.ID == planId).Select(q => q.PlanName).FirstOrDefault();
            return planName;
        }

        public int GetTenantPlanId(int tenantId)
        {
            var planId = _context.Tenants.Where(e => e.ID == tenantId).Select(r => r.PlanTypeId).FirstOrDefault();
            var planName = _context.PlanTypes.Where(r => r.ID == planId).Select(q => q.PlanName).FirstOrDefault();
            return planId;
        }
    }

    public interface ITenantRepository
    {
        Task AddTenant(Tenant tenant);
        string GetTenantPlanName(int tenantId);
        Task GetTenant(int tenantId);
        int GetTenantPlanId(int tenantId);

    }
}
