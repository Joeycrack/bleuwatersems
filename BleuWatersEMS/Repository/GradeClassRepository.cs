﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class GradeClassRepository : IGradeClassRepository
    {
        private DataContext _context;
        public GradeClassRepository (DataContext context)
        {
            _context = context;
        }

        public async Task AddGradeClass(GradeClass gradeClass, int TenantId)
        {
            gradeClass.TenantId = TenantId;
            _context.GradeClasses.Add(gradeClass);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<GradeClass>> GetGradeClasses(int TenantId)
        {
           var gradeClasses = await _context.GradeClasses.Where(e => e.Status == true && e.TenantId == TenantId).ToListAsync();
           return gradeClasses;
        }

        //public async Task<IEnumerable<GradeClass>> GetGradeClass()
        //{
        //    var gradeClasses = await _context.GradeClasses.ToListAsync();
        //    return gradeClasses;
        //}

        
    }

    public interface IGradeClassRepository
    {
        Task AddGradeClass(GradeClass gradeClass, int TenantId);
        Task<IEnumerable<GradeClass>> GetGradeClasses(int TenantId);
    }
}
