﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class StudentAttendanceRepository : IStudentAttendanceRepository
    {
        private DataContext _context;
        public StudentAttendanceRepository(DataContext context)
        {
            _context = context;
        }

        public async Task AddAttendance(StudentAttendance attendance, int TenantId)
        {
            attendance.TenantId = TenantId;
            _context.StudentAttendances.Add(attendance);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<StudentAttendance>> GetAttendances(int TenantId)
        {
            var attendances = await _context.StudentAttendances.Where(e => e.TenantId == TenantId).ToListAsync();
            return attendances;
        }

        public async Task<IEnumerable<StudentAttendanceViewModel>> GetClassAttendances(int classId, int TenantId)
        {
            var attendances = await _context.StudentAttendances.Where(e => e.ClassId == classId && e.Date.Date == DateTime.Today && e.TenantId == TenantId)
                .Select(m => new StudentAttendanceViewModel
                {
                    StudentName = _context.Students.Where(w => w.ID == m.StudentId && w.TenantId == TenantId).Select(q => q.FirstName + ' ' + q.MiddleName + ' ' + q.LastName).FirstOrDefault(),
                    StudentId = m.StudentId,
                    GradeClassId = m.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(w => w.ID == m.GradeClassId && w.TenantId == TenantId).Select(q => q.GradeName).FirstOrDefault(),
                    Date = m.Date,
                    ClassId = m.ClassId,
                    ClassSectionName = _context.Classes.Where(w => w.ID == m.ClassId && w.TenantId == TenantId).Select(q => q.ClassName).FirstOrDefault(),
                    ClassName = "",
                    CurrentSessionId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault(),
                    CurrentSessionName = _context.Sessions.Where(f => f.ID == _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault() && f.TenantId == TenantId).Select(y => y.SessionName).FirstOrDefault(),
                    CurrentTermId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault(),
                    CurrentTermName = _context.Terms.Where(e => e.ID == _context.Settings.Where(f => f.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault() && e.TenantId == TenantId).Select(r => r.TermName).FirstOrDefault(),
                    Present = m.Present,
                    Sex = _context.Students.Where(w => w.ID == m.StudentId).Select(q => q.Sex).FirstOrDefault(),
                }).ToListAsync();
            return attendances;
        }

        public async Task<StudentAttendance> GetAttendance(StudentAttendanceViewModel model, int TenantId)
        {
            var attendances = await _context.StudentAttendances.Where(e => e.StudentId == model.StudentId && e.Date == model.Date && e.TenantId == TenantId).FirstOrDefaultAsync();
            if (attendances != null)
            {
                attendances.TenantId = TenantId;
                attendances.Present = model.Present;
                _context.StudentAttendances.Update(attendances);
                await _context.SaveChangesAsync();
            }
            else
            {
                attendances = null;
            }
            return attendances;
        }

        public async Task AddAttendance(StudentAttendanceViewModel model, int TenantId)
        {
            StudentAttendance attendance = new StudentAttendance();
            attendance.Present = model.Present;
            attendance.StudentId = model.StudentId;
            attendance.TakenBy = model.TeacherId;
            attendance.GradeClassId = model.GradeClassId;
            attendance.Date = model.Date;
            attendance.ClassId = model.ClassId;
            attendance.SessionId = await _context.Settings.Where(e => e.TenantId == TenantId).Select(m => m.SessionId).FirstOrDefaultAsync();
            attendance.TermId = await _context.Settings.Where(e => e.TenantId == TenantId).Select(m => m.TermId).FirstOrDefaultAsync(); ;
            attendance.TenantId = TenantId;

            _context.Add(attendance);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<StudentAttendanceViewModel>> GetAttendance(int TenantId)
        {
            var students = await _context.StudentAttendances.AsNoTracking().Where(t => t.Date.Date == DateTime.Today && t.TenantId == TenantId)
                .Select(m => new StudentAttendanceViewModel
                {
                    StudentName = _context.Students.Where(w => w.ID == m.StudentId && w.TenantId == TenantId).Select(q => q.FirstName + ' ' + q.MiddleName + ' ' + q.LastName).FirstOrDefault(),
                    StudentId = m.StudentId,
                    GradeClassId = m.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(w => w.ID == m.GradeClassId && w.TenantId == TenantId).Select(q => q.GradeName).FirstOrDefault(),
                    Date = m.Date,
                    ClassId = m.ClassId,
                    ClassSectionName = _context.Classes.Where(w => w.ID == m.ClassId && w.TenantId == TenantId).Select(q => q.ClassName).FirstOrDefault(),
                    ClassName = "",
                    TenantId = TenantId,
                    CurrentSessionId =  _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault(),
                    CurrentSessionName = _context.Sessions.Where(f => f.ID == _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault() &&  f.TenantId == TenantId).Select(y => y.SessionName).FirstOrDefault(),
                    CurrentTermId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault(),
                    CurrentTermName = _context.Terms.Where(e => e.ID == _context.Settings.Where(f => f.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault() && e.TenantId == TenantId).Select(r => r.TermName).FirstOrDefault(),
                    Present = m.Present,
                    Sex = _context.Students.Where(w => w.ID == m.StudentId).Select(q => q.Sex).FirstOrDefault(),
                }).ToListAsync();
            return students;
        }

        public async Task<IEnumerable<StudentAttendanceViewModel>> ClassStudentsAttendance(int classId, int TenantId)
        {
            var students = await _context.Students.Where(e => e.Active == true && e.ClassId == classId && e.TenantId == TenantId)
                .Select(m => new StudentAttendanceViewModel
                {
                    StudentName = m.FirstName + ' ' + m.MiddleName + ' ' + m.LastName,
                    StudentId = m.ID,
                    GradeClassId = m.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(w => w.ID == m.GradeClassId && w.TenantId == TenantId).Select(q => q.GradeName).FirstOrDefault(),
                    Date = DateTime.UtcNow,
                    ClassId = m.ClassId,
                    ClassSectionName = _context.Classes.Where(w => w.ID == m.ClassId && w.TenantId == TenantId).Select(q => q.ClassName).FirstOrDefault(),
                    ClassName = "",
                    CurrentSessionId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault(),
                    CurrentSessionName = _context.Sessions.Where(f => f.ID == _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault() && f.TenantId == TenantId).Select(y => y.SessionName).FirstOrDefault(),
                    CurrentTermId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault(),
                    CurrentTermName = _context.Terms.Where(e => e.ID == _context.Settings.Where(f => f.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault() && e.TenantId == TenantId).Select(r => r.TermName).FirstOrDefault(),
                    Present = false,
                    Sex = m.Sex,
                    TenantId = m.TenantId
                }).ToListAsync();
                
            return students;
        }

        public async Task<IEnumerable<StudentAttendanceViewModel>> TeacherAttendanceToday(int teacherId, int TenantId)
        {
            var students = await _context.StudentAttendances.AsNoTracking().Where(t => t.Date.Date == DateTime.Today && t.TakenBy == teacherId && t.TenantId == TenantId)
                .Select(m => new StudentAttendanceViewModel
                {
                    StudentName = _context.Students.Where(w => w.ID == m.StudentId && w.TenantId == TenantId).Select(q => q.FirstName + ' ' + q.MiddleName + ' ' + q.LastName).FirstOrDefault(),
                    StudentId = m.StudentId,
                    GradeClassId = m.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(w => w.ID == m.GradeClassId && w.TenantId == TenantId).Select(q => q.GradeName).FirstOrDefault(),
                    Date = m.Date,
                    ClassId = m.ClassId,
                    ClassSectionName = _context.Classes.Where(w => w.ID == m.ClassId && w.TenantId == TenantId).Select(q => q.ClassName).FirstOrDefault(),
                    ClassName = "",
                    CurrentSessionId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault(),
                    CurrentSessionName = _context.Sessions.Where(f => f.ID == _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault() && f.TenantId == TenantId).Select(y => y.SessionName).FirstOrDefault(),
                    CurrentTermId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault(),
                    CurrentTermName = _context.Terms.Where(e => e.ID == _context.Settings.Where(f => f.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault() && e.TenantId == TenantId).Select(r => r.TermName).FirstOrDefault(),
                    Present = m.Present,
                    Sex = _context.Students.Where(w => w.ID == m.StudentId).Select(q => q.Sex).FirstOrDefault(),
                }).ToListAsync();
            return students;
        }

        public async Task<IEnumerable<StudentAttendanceViewModel>> GetStudentsAtt(int TenantId)
        {
            var students = await _context.Students.AsNoTracking().Where(t => t.Active == true && t.TenantId == TenantId)
                .Select(m => new StudentAttendanceViewModel
                {
                    StudentName = m.FirstName + ' ' + m.MiddleName + ' ' + m.LastName,
                    StudentId = m.ID,
                    GradeClassId = m.GradeClassId,
                    GradeClassName = _context.GradeClasses.Where(w => w.ID == m.GradeClassId && w.TenantId == TenantId).Select(q => q.GradeName).FirstOrDefault(),
                    Date = DateTime.UtcNow,
                    ClassId = m.ClassId,
                    ClassSectionName = _context.Classes.Where(w => w.ID == m.ClassId && w.TenantId == TenantId).Select(q => q.ClassName).FirstOrDefault(),
                    ClassName = "",
                    CurrentSessionId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault(),
                    CurrentSessionName = _context.Sessions.Where(f => f.ID == _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.SessionId).FirstOrDefault() && f.TenantId == TenantId).Select(y => y.SessionName).FirstOrDefault(),
                    CurrentTermId = _context.Settings.Where(e => e.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault(),
                    CurrentTermName = _context.Terms.Where(e => e.ID == _context.Settings.Where(f => f.TenantId == TenantId).Select(r => r.TermId).FirstOrDefault() && e.TenantId == TenantId).Select(r => r.TermName).FirstOrDefault(),
                    Present = false,
                    Sex = m.Sex
                }).ToListAsync();
            return students;
        }

    }

    public interface IStudentAttendanceRepository
    {
        Task<IEnumerable<StudentAttendance>> GetAttendances(int TenantId);
        Task AddAttendance(StudentAttendance attendance, int TenantId);
        Task<IEnumerable<StudentAttendanceViewModel>> GetStudentsAtt(int TenantId);
        Task<StudentAttendance> GetAttendance(StudentAttendanceViewModel model, int TenantId);
        Task AddAttendance(StudentAttendanceViewModel model, int TenantId);
        Task<IEnumerable<StudentAttendanceViewModel>> GetAttendance(int TenantId);
        Task<IEnumerable<StudentAttendanceViewModel>> TeacherAttendanceToday(int teacherId, int TenantId);
        Task<IEnumerable<StudentAttendanceViewModel>> ClassStudentsAttendance(int classId, int TenantId);
        Task<IEnumerable<StudentAttendanceViewModel>> GetClassAttendances(int classId, int TenantId);
    }
}
