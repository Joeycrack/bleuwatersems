﻿using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class ErrorRepository : RepositoryBase<Error>, IErrorRepository
    {
        public ErrorRepository()
            : base()
        { }
    }

    public interface IErrorRepository : IRepository<Error>
    {

    }
}
