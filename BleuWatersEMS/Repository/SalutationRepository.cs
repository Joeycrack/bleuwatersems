﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class SalutationRepository : ISalutationRepository
    {
        private DataContext _context;
        public SalutationRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Salutation>> GetSalutations(int TenantId)
        {
            var salutations = await _context.Salutations.ToListAsync();
            return salutations;
        }

        public async Task AddSalutation (Salutation salutation, int TenantId)
        {
            salutation.TenantId = TenantId;
            _context.Salutations.Add(salutation);
            await _context.SaveChangesAsync();
        }
    }

    public interface ISalutationRepository
    {
        Task AddSalutation (Salutation salutation, int TenantId);
        Task<IEnumerable<Salutation>> GetSalutations(int TenantId);
    }
}
