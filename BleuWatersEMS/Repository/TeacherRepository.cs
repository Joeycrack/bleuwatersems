﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class TeacherRepository : ITeacherRepository
    {
        private DataContext _context;
        public TeacherRepository(DataContext context)
        {
            _context = context;

        }

        public async Task AddTeacher (Teacher teacher, int TenantId)
        {
            teacher.TenantId = TenantId;
            _context.Teachers.Add(teacher);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Teacher>> GetTeachers(int TenantId)
        {
            var subjectss = await _context.Teachers.Where(e => e.Status == true && e.TenantId == TenantId).ToListAsync();
            return subjectss;
        }

        public async Task<IEnumerable<TeacherViewModel>> GetTeacherList(int TenantId)
        {
            var subjectss = await _context.Teachers.Where(e => e.Status == true && e.TenantId == TenantId)
                .Select(r => new TeacherViewModel
                {
                    FullName = r.LastName +' '+ r.FirstName,
                    Id = r.ID,
                    MaritalSatus = r.MaritalSatus,
                    Status = r.Status,
                    Sex = r.Sex
                }).ToListAsync();
            return subjectss;
        }

        public async Task<TeacherClassViewModel> GetTeacherStudent(int Id, int TenantId)
        {
            TeacherClassViewModel teacher = new TeacherClassViewModel();
            var classes = await _context.SubjectClasses.Where(e => e.SubjectId == Id && e.TenantId == TenantId)
                .Select(r => new ClassSectionViewModel
                {
                    Id = r.ClassId,
                    ClassName = _context.Classes.Where(m => m.ID == r.ClassId).Select(k => k.ClassName).FirstOrDefault(),

                }).ToListAsync();
            //return subjectss;
            teacher.Classes = classes;
            foreach (var item in classes)
            {
                teacher.Students = await _context.Students.Where(e => e.ClassId == item.Id).ToListAsync();
            }

            return teacher;
        }

        public async Task<IEnumerable<TeacherClassSectionsViewModel>> GetTeacherClasses(int teacherId, int TenantId)
        {
            var classes = await _context.Classes.Where(e => e.TeacherId == teacherId && e.TenantId == TenantId && e.Status == true)
                .Select(r => new TeacherClassSectionsViewModel
                {
                    Id = r.ID,
                    ClassName = r.ClassName,
                    GradeClassId = r.GradeClassId,                    
                    ClassTeacherId = r.TeacherId,
                    NumOfStudents = _context.Students.Where(q => q.ClassId == r.ID && q.TenantId == TenantId).Count(),
                    MaxNumber = r.MaxNumber,
                    Status = r.Status,
                    SessionTerm = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p =>
                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " +
                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault()
                    && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")"
                }).ToListAsync();

            return classes;
            
        }

        public async Task<IEnumerable<TeacherStudentViewModel>> GetTeacherClassStudents(int classId, int teacherId, int TenantId)
        {
            var students = await (from r in _context.Students where r.TenantId == TenantId && r.ClassId == classId
                                 join s in _context.Classes on classId equals s.ID
                                  where s.TeacherId == teacherId && r.TenantId == TenantId
                                 select new TeacherStudentViewModel
                                 {
                                     StudentName = r.FirstName +" "+ r.MiddleName +" "+ r.LastName,
                                     Sex = r.Sex,
                                     ClassName = s.ClassName,
                                     StudentId = r.ID,
                                     GradeClassId = r.GradeClassId,
                                     ClassId = r.ClassId,
                                     DateOfBirth = r.DateOfBirth,
                                     TermSession = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p =>
                                                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " +
                                                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault()
                                                     && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")",
                                     

                                 }).ToListAsync();

             foreach (TeacherStudentViewModel std in students)
             {
                var today = DateTime.Today;
                var age = today.Year - std.DateOfBirth.Year;
                if (std.DateOfBirth > today.AddYears(-age))
                {
                    std.Age = age--;
                }else
                {
                    std.Age = age;
                }

             }
                            return students;

        }

    }

    public interface ITeacherRepository
    {
        Task AddTeacher (Teacher teacher, int TenantId);
        Task<IEnumerable<Teacher>> GetTeachers(int TenantId);
        Task<IEnumerable<TeacherViewModel>> GetTeacherList(int TenantId);
        Task<TeacherClassViewModel> GetTeacherStudent(int Id, int TenantId);
        Task<IEnumerable<TeacherClassSectionsViewModel>> GetTeacherClasses(int teacherId, int TenantId);
        Task<IEnumerable<TeacherStudentViewModel>> GetTeacherClassStudents(int classId, int teacherId, int TenantId);
    }
}
