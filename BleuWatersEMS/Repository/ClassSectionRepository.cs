﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class ClassSectionRepository : IClassSectionRepository
    {
        private DataContext _context;
        public ClassSectionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task AddClassSection(ClassSectionViewModel classSectionview, int TenantId)
        {

            Class classSection = new Class();
            classSection.ID = classSectionview.Id;
            classSection.ClassName = _context.GradeClasses.Where(r => r.ID == classSectionview.GradeClassId).Select(m => m.GradeName + ' ' + '('+ classSectionview.ClassName +')').FirstOrDefault() ;
            classSection.Status = classSectionview.Status;
            classSection.TeacherId = classSectionview.ClassTeacherId;
            classSection.GradeClassId = classSectionview.GradeClassId;
            classSection.MaxNumber = classSectionview.MaxNumber;
            classSection.TenantId = TenantId;
            _context.Classes.Add(classSection);
            await _context.SaveChangesAsync();

            classSectionview.SubjectClass.ForEach(r => r.ClassId = classSection.ID);
            classSectionview.SubjectClass.ForEach(r => r.TenantId = TenantId);
            _context.SubjectClasses.AddRange(classSectionview.SubjectClass);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<GetClassSectionsViewModel>> GetClassSections(int TenantId)
        {
            var sections = await _context.Classes.Where(e => e.Status == true && e.TenantId == TenantId)
                .Select(r => new GetClassSectionsViewModel {
                    ClassName = r.ClassName,
                    ClassTeacherId = r.TeacherId,
                    ClassTeacherName = _context.Teachers.Where(a => a.ID == r.TeacherId).Select(u => u.FirstName + ' ' + u.LastName).FirstOrDefault(),
                    MaxNumber = r.MaxNumber,
                    Status = r.Status,
                    GradeClassId = r.GradeClassId,
                    Id = r.ID
                }).ToListAsync();
            return sections;
        }

        public async Task<IEnumerable<GetClassSectionsViewModel>> GetTeacherClassSections(int teacherId, int TenantId)
        {
            var sections = await _context.Classes.Where(e => e.Status == true && e.TeacherId == teacherId && e.TenantId == TenantId)
                .Select(r => new GetClassSectionsViewModel
                {
                    ClassName = r.ClassName,
                    ClassTeacherId = r.TeacherId,
                    ClassTeacherName = _context.Teachers.Where(a => a.ID == r.TeacherId).Select(u => u.FirstName + ' ' + u.LastName).FirstOrDefault(),
                    MaxNumber = r.MaxNumber,
                    Status = r.Status,
                    GradeClassId = r.GradeClassId,
                    Id = r.ID
                }).ToListAsync();
            return sections;
        }

        

        class ItemEqualityComparer : IEqualityComparer<GetClassSectionsViewModel>
        {
            public bool Equals(GetClassSectionsViewModel x, GetClassSectionsViewModel y)
            {
                // Two items are equal if their keys are equal.
                return x.ClassName == y.ClassName;
            }

            public int GetHashCode(GetClassSectionsViewModel obj)
            {
                return obj.ClassName.GetHashCode();
            }
        }
        // gets all class section where the current teacher
        // teaches at list a subject
        public async Task<IEnumerable<GetClassSectionsViewModel>> TeacherClassDropDwn(int teacherId, int TenantId)
        {
            var sections = await _context.SubjectClasses.Where(e => e.TeacherId == teacherId && e.TenantId == TenantId)
                .Select(r => new GetClassSectionsViewModel
                {
                    ClassName = _context.Classes.Where(c => c.ID == r.ClassId && c.TenantId == r.TenantId).Select(e => e.ClassName).FirstOrDefault(),
                    ClassTeacherId = r.TeacherId,
                    ClassTeacherName = _context.Teachers.Where(a => a.ID == r.TeacherId).Select(u => u.FirstName + ' ' + u.LastName).FirstOrDefault(),
                    Id = r.ClassId
                }).ToListAsync();

                var result = sections.Distinct(new ItemEqualityComparer());

                return result;
        }
    }

    public interface IClassSectionRepository
    {
        Task<IEnumerable<GetClassSectionsViewModel>> GetClassSections(int TenantId);
        Task<IEnumerable<GetClassSectionsViewModel>> GetTeacherClassSections(int teacherId, int TenantId);
        Task AddClassSection(ClassSectionViewModel classSectionview, int TenantId);
        Task<IEnumerable<GetClassSectionsViewModel>> TeacherClassDropDwn(int teacherId, int TenantId);
    }
}
