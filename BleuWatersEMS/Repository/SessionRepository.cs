﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class SessionRepository: ISessionRepository
    {
        private DataContext _context;
        public SessionRepository(DataContext context)
        {
            _context = context;
        }

        public async Task AddSession(SessionViewModel model, int TenantId)
        {
            Session session = new Session();
            session.IsCurrentSession = model.IsCurrentSession;
            session.SessionName = model.SessionName;
            session.StartDate = model.StartDate;
            session.EndDate = model.StartDate;
            session.TenantId = TenantId;
            
            await _context.Sessions.AddAsync(session);
            await _context.SaveChangesAsync();
            var currentSessionId = await _context.Sessions.Where(e => e.TenantId == TenantId && e.IsCurrentSession == true).Select(m => m.ID).FirstOrDefaultAsync();
            
            //var _activTermId = await _context.Terms.Where(e => e.IsCurrent && e.TenantId == TenantId).Select(m => m.ID).FirstOrDefaultAsync();

            if (model.Terms.Count > 0)
            {
                model.Terms.ForEach(r => r.SessionId = session.ID);
                model.Terms.ForEach(r => r.TenantId = TenantId);
                //model.Terms.ForEach(r => r. = TenantId);
                await _context.Terms.AddRangeAsync(model.Terms);
                await _context.SaveChangesAsync();
            }
            
            var _currentTermId = await _context.Terms.Where(e => e.IsCurrent && e.TenantId == TenantId).Select(m => m.ID).FirstOrDefaultAsync();

            var settings = await _context.Settings.Where(e => e.TenantId == TenantId).FirstOrDefaultAsync();

            if (settings == null)
            {
                Settings newSettings = new Settings();
                newSettings.SessionId = currentSessionId;
                newSettings.TenantId = TenantId;
                newSettings.TermId = _currentTermId;
                _context.Settings.Add(newSettings);
                await _context.SaveChangesAsync();
            }
            else
            {

                settings.SessionId = currentSessionId;
                settings.TermId = _currentTermId;
                settings.TenantId = TenantId;
                _context.Settings.Update(settings);
                await _context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<Session>> GetSessions(int TenantId)
        {
            var sessions = await _context.Sessions.Where(e => e.TenantId == TenantId).ToListAsync();
            return sessions;
        }

        public async Task<IEnumerable<Term>> GetTerms(int TenantId)
        {
            var terms = await _context.Terms.Where(e => e.TenantId == TenantId).ToListAsync();
            return terms;
        }
    }

    public interface ISessionRepository
    {
        Task<IEnumerable<Session>> GetSessions(int TenantId);
        Task AddSession(SessionViewModel session, int TenantId);
        Task<IEnumerable<Term>> GetTerms(int TenantId);
    }
}
