﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class UserRepository : IUserRepository
    {
        private DataContext _context;
        public UserRepository(DataContext context)
        {
            _context = context;

        }

        public async Task AddUser(User user, int TenantId)
        {
            user.TenantId = TenantId;
            _context.User.Add(user);
            await _context.SaveChangesAsync();
        }

        public LoginResponseData GetByAppUserId(string AppUserId)
        {
            LoginResponseData userTeacher = new LoginResponseData();
            var _user = _context.User.Where(e => e.ApplicationUserId == AppUserId)
                .Select(y => new UserTeacherViewModel {
                    User = y

                }).FirstOrDefault();
            UserTeacherViewModel _teacher = new UserTeacherViewModel();
            if (_user.User.IsTeacher == true)
            {
                _teacher = _context.Teachers.Where(e => e.ApplicationUserId == AppUserId)
               .Select(y => new UserTeacherViewModel
               {
                   Teacher = y
               }).FirstOrDefault();

                
                userTeacher.TeacherId = _teacher.Teacher.ID;
                userTeacher.User = _user.User;
                userTeacher.IsTeacher = _user.User.IsTeacher;
                userTeacher.IsAdmin = _user.User.IsAdmin;
            }
            else
            {

                //LoginResponseData userTeacher = new LoginResponseData();
                //userTeacher.TeacherId = _teacher.Teacher.ID;
                userTeacher.User = _user.User;
                userTeacher.IsTeacher = _user.User.IsTeacher;
                userTeacher.IsAdmin = _user.User.IsAdmin;
            }

            return userTeacher;
        }

        public async Task<IEnumerable<User>> GetUsers(int TenantId)
        {
            var _user = await _context.User.Where(e => e.TenantId == TenantId).ToListAsync();
                
            return _user;
        }
    }

    public interface IUserRepository
    {
        Task AddUser(User user, int TenantId);
        LoginResponseData GetByAppUserId(string AppUserId);
        Task<IEnumerable<User>> GetUsers(int TenantId);
    }
}
