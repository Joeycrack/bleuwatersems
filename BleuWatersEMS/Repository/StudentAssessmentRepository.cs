﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Repository
{
    public class StudentAssessmentRepository : IStudentAssessmentRepository
    {
        private DataContext _context;
        public StudentAssessmentRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<AssessmentViewModel>> GetClassStudentsAssesment(int classId, int subjectId, int TenantId)
        {
            var gradeClassId = await _context.Classes.Where(v => v.ID == classId && v.TenantId == TenantId).Select(c => c.GradeClassId).FirstOrDefaultAsync();
            var asses = await _context.StudentAssessments.Where(i => i.ClassId == classId && i.SubjectId == subjectId && i.TenantId == TenantId).ToListAsync();
            var students = await _context.Students.Where(e => e.Active == true && e.ClassId == classId)
                .Select(y => new AssessmentViewModel()
                {
                    ID = asses.Where(t => t.StudentId == y.ID).Select(o => o.ID).FirstOrDefault(),
                    StudentId = y.ID,
                    StudentName = y.FirstName + " " + y.MiddleName + " " + y.LastName,
                    ClassName = _context.Classes.Where(s => s.ID == classId && s.TenantId == TenantId).Select(d => d.ClassName).FirstOrDefault(),
                    SubjectId = subjectId,
                    ClassId = classId,
                    SubjectName = _context.Subjects.Where(k => k.ID == subjectId && k.TenantId == TenantId).Select(p => p.subjectName).FirstOrDefault(),
                    ExamsScore = asses.Where(t => t.StudentId == y.ID).Select(o => o.ExamsScore).FirstOrDefault(),
                    CAScore = asses.Where(t => t.StudentId == y.ID).Select(o => o.CAScore).FirstOrDefault(),
                    TermId = _context.Settings.Where(r => r.TenantId == TenantId).Select(q => q.TermId).FirstOrDefault(),
                    SessionId = _context.Settings.Where(r => r.TenantId == TenantId).Select(q => q.SessionId).FirstOrDefault(),
                    TenantId = TenantId,
                    SessionTerm = _context.Terms.Where(t => t.TenantId == TenantId && t.ID == _context.Settings.Where(p =>
                    p.TenantId == TenantId).Select(x => x.TermId).FirstOrDefault()).Select(x => x.TermName).FirstOrDefault() + " " +
                    "(" + _context.Sessions.Where(t => t.ID == _context.Settings.Where(u => u.TenantId == TenantId).Select(k => k.SessionId).FirstOrDefault()
                    && t.TenantId == TenantId).Select(v => v.SessionName).FirstOrDefault() + ")",
                    GradeClassId = gradeClassId,
                    RecordedBy = asses.Where(t => t.StudentId == y.ID).Select(o => o.RecordedBy).FirstOrDefault(),
                    TotalScore = asses.Where(t => t.StudentId == y.ID).Select(o => o.TotalScore).FirstOrDefault(),
                }).ToListAsync();

            return students;
        }

        public async Task AddStudentAssessment (AssessmentViewModel model, int TenantId)
        {
            if (model.ID > 0)
            {
                var asss = await _context.StudentAssessments.Where(y => y.ID == model.ID && y.TenantId == TenantId).FirstOrDefaultAsync();
                asss.ExamsScore = model.ExamsScore;
                asss.CAScore = model.CAScore;
                asss.TotalScore = model.CAScore + model.ExamsScore;
                _context.Update(asss);
                await _context.SaveChangesAsync();
            }
            else
            {
                StudentAssessment assessment = new StudentAssessment
                {
                    CAScore = model.CAScore,
                    ClassId = model.ClassId,
                    ExamsScore = model.ExamsScore,
                    GradeClassId = model.GradeClassId,
                    SessionId = model.SessionId,
                    StudentId = model.StudentId,
                    SubjectId = model.SubjectId,
                    TenantId = TenantId,
                    TermId = model.TermId,
                    RecordedBy = model.RecordedBy,
                    Submitted = false,
                    Approved = false,
                    TotalScore = model.CAScore + model.ExamsScore
                };
                await _context.AddAsync(assessment);
                _context.SaveChanges();
            }
        }

        public async Task SubmittedStudentAssessment(List<AssessmentViewModel> model, int TenantId)
        {
            foreach (AssessmentViewModel std in model)
            {
                var ass = await _context.StudentAssessments.Where(e => e.ID == std.ID && e.TenantId == TenantId).FirstOrDefaultAsync();
                ass.Submitted = true;
                _context.Update(ass);
                _context.SaveChanges();
            }
        }
    }

    public interface IStudentAssessmentRepository
    {
        Task<IEnumerable<AssessmentViewModel>> GetClassStudentsAssesment(int classId, int subjectId, int TenantId);
        Task AddStudentAssessment(AssessmentViewModel model, int TenantId);
        Task SubmittedStudentAssessment(List<AssessmentViewModel> model, int TenantId);
    }
}
