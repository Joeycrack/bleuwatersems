﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class multiTenant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Users",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "User",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Term",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Teacher",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "SubjectGradeClasses",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "SubjectClasses",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Subject",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "StudentSubjects",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "StudentAttendance",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Student",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Session",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Salutation",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Parent",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Guardian",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "GradeClass",
                nullable: true,
                defaultValue: null);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Class",
                nullable: true,
                defaultValue: null);

            migrationBuilder.CreateTable(
                name: "PlanType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PlanName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Tenant",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    SchoolName = table.Column<string>(nullable: true),
                    CountryId = table.Column<int>(nullable: false),
                    StateId = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumber2 = table.Column<string>(nullable: true),
                    BadgeUrl = table.Column<string>(nullable: true),
                    Motto = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Website = table.Column<string>(nullable: true),
                    PlanTypeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tenant", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Tenant_Country_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tenant_PlanType_PlanTypeId",
                        column: x => x.PlanTypeId,
                        principalTable: "PlanType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Tenant_State_StateId",
                        column: x => x.StateId,
                        principalTable: "State",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Interface",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true),
                    url = table.Column<string>(nullable: true),
                    divider = table.Column<bool>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    SuperAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Interface", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Interface_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "InterfaceChildren",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    name = table.Column<string>(nullable: true),
                    url = table.Column<string>(nullable: true),
                    icon = table.Column<string>(nullable: true),
                    InterfaceId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    SuperAdmin = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InterfaceChildren", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InterfaceChildren_Interface_InterfaceId",
                        column: x => x.InterfaceId,
                        principalTable: "Interface",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_InterfaceChildren_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PlanTypeInterfaces",
                columns: table => new
                {
                    PlanTypeId = table.Column<int>(nullable: false),
                    InterfaceId = table.Column<int>(nullable: false),
                    TenantID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlanTypeInterfaces", x => new { x.InterfaceId, x.PlanTypeId });
                    table.ForeignKey(
                        name: "FK_PlanTypeInterfaces_Interface_InterfaceId",
                        column: x => x.InterfaceId,
                        principalTable: "Interface",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlanTypeInterfaces_PlanType_PlanTypeId",
                        column: x => x.PlanTypeId,
                        principalTable: "PlanType",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PlanTypeInterfaces_Tenant_TenantID",
                        column: x => x.TenantID,
                        principalTable: "Tenant",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RoleInterfaces",
                columns: table => new
                {
                    RoleId = table.Column<int>(nullable: false),
                    InterfaceId = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleInterfaces", x => new { x.InterfaceId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_RoleInterfaces_Interface_InterfaceId",
                        column: x => x.InterfaceId,
                        principalTable: "Interface",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RoleInterfaces_Tenant_TenantId",
                        column: x => x.TenantId,
                        principalTable: "Tenant",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Users_TenantId",
                table: "Users",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_User_TenantId",
                table: "User",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Term_TenantId",
                table: "Term",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Teacher_TenantId",
                table: "Teacher",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectGradeClasses_TenantId",
                table: "SubjectGradeClasses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectClasses_TenantId",
                table: "SubjectClasses",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Subject_TenantId",
                table: "Subject",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentSubjects_TenantId",
                table: "StudentSubjects",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAttendance_TenantId",
                table: "StudentAttendance",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Student_TenantId",
                table: "Student",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Settings_TenantId",
                table: "Settings",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Session_TenantId",
                table: "Session",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Salutation_TenantId",
                table: "Salutation",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Parent_TenantId",
                table: "Parent",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Guardian_TenantId",
                table: "Guardian",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_GradeClass_TenantId",
                table: "GradeClass",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Class_TenantId",
                table: "Class",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Interface_TenantId",
                table: "Interface",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_InterfaceChildren_InterfaceId",
                table: "InterfaceChildren",
                column: "InterfaceId");

            migrationBuilder.CreateIndex(
                name: "IX_InterfaceChildren_TenantId",
                table: "InterfaceChildren",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTypeInterfaces_PlanTypeId",
                table: "PlanTypeInterfaces",
                column: "PlanTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_PlanTypeInterfaces_TenantID",
                table: "PlanTypeInterfaces",
                column: "TenantID");

            migrationBuilder.CreateIndex(
                name: "IX_RoleInterfaces_TenantId",
                table: "RoleInterfaces",
                column: "TenantId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenant_CountryId",
                table: "Tenant",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenant_PlanTypeId",
                table: "Tenant",
                column: "PlanTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Tenant_StateId",
                table: "Tenant",
                column: "StateId");

            migrationBuilder.AddForeignKey(
                name: "FK_Class_Tenant_TenantId",
                table: "Class",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_GradeClass_Tenant_TenantId",
                table: "GradeClass",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Guardian_Tenant_TenantId",
                table: "Guardian",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Parent_Tenant_TenantId",
                table: "Parent",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Salutation_Tenant_TenantId",
                table: "Salutation",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Session_Tenant_TenantId",
                table: "Session",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Student_Tenant_TenantId",
                table: "Student",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAttendance_Tenant_TenantId",
                table: "StudentAttendance",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentSubjects_Tenant_TenantId",
                table: "StudentSubjects",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Tenant_TenantId",
                table: "Subject",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectClasses_Tenant_TenantId",
                table: "SubjectClasses",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SubjectGradeClasses_Tenant_TenantId",
                table: "SubjectGradeClasses",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Teacher_Tenant_TenantId",
                table: "Teacher",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Term_Tenant_TenantId",
                table: "Term",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Tenant_TenantId",
                table: "User",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Tenant_TenantId",
                table: "Users",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Class_Tenant_TenantId",
                table: "Class");

            migrationBuilder.DropForeignKey(
                name: "FK_GradeClass_Tenant_TenantId",
                table: "GradeClass");

            migrationBuilder.DropForeignKey(
                name: "FK_Guardian_Tenant_TenantId",
                table: "Guardian");

            migrationBuilder.DropForeignKey(
                name: "FK_Parent_Tenant_TenantId",
                table: "Parent");

            migrationBuilder.DropForeignKey(
                name: "FK_Salutation_Tenant_TenantId",
                table: "Salutation");

            migrationBuilder.DropForeignKey(
                name: "FK_Session_Tenant_TenantId",
                table: "Session");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Student_Tenant_TenantId",
                table: "Student");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentAttendance_Tenant_TenantId",
                table: "StudentAttendance");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentSubjects_Tenant_TenantId",
                table: "StudentSubjects");

            migrationBuilder.DropForeignKey(
                name: "FK_Subject_Tenant_TenantId",
                table: "Subject");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectClasses_Tenant_TenantId",
                table: "SubjectClasses");

            migrationBuilder.DropForeignKey(
                name: "FK_SubjectGradeClasses_Tenant_TenantId",
                table: "SubjectGradeClasses");

            migrationBuilder.DropForeignKey(
                name: "FK_Teacher_Tenant_TenantId",
                table: "Teacher");

            migrationBuilder.DropForeignKey(
                name: "FK_Term_Tenant_TenantId",
                table: "Term");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Tenant_TenantId",
                table: "User");

            migrationBuilder.DropForeignKey(
                name: "FK_Users_Tenant_TenantId",
                table: "Users");

            migrationBuilder.DropTable(
                name: "InterfaceChildren");

            migrationBuilder.DropTable(
                name: "PlanTypeInterfaces");

            migrationBuilder.DropTable(
                name: "RoleInterfaces");

            migrationBuilder.DropTable(
                name: "Interface");

            migrationBuilder.DropTable(
                name: "Tenant");

            migrationBuilder.DropTable(
                name: "PlanType");

            migrationBuilder.DropIndex(
                name: "IX_Users_TenantId",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_User_TenantId",
                table: "User");

            migrationBuilder.DropIndex(
                name: "IX_Term_TenantId",
                table: "Term");

            migrationBuilder.DropIndex(
                name: "IX_Teacher_TenantId",
                table: "Teacher");

            migrationBuilder.DropIndex(
                name: "IX_SubjectGradeClasses_TenantId",
                table: "SubjectGradeClasses");

            migrationBuilder.DropIndex(
                name: "IX_SubjectClasses_TenantId",
                table: "SubjectClasses");

            migrationBuilder.DropIndex(
                name: "IX_Subject_TenantId",
                table: "Subject");

            migrationBuilder.DropIndex(
                name: "IX_StudentSubjects_TenantId",
                table: "StudentSubjects");

            migrationBuilder.DropIndex(
                name: "IX_StudentAttendance_TenantId",
                table: "StudentAttendance");

            migrationBuilder.DropIndex(
                name: "IX_Student_TenantId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Settings_TenantId",
                table: "Settings");

            migrationBuilder.DropIndex(
                name: "IX_Session_TenantId",
                table: "Session");

            migrationBuilder.DropIndex(
                name: "IX_Salutation_TenantId",
                table: "Salutation");

            migrationBuilder.DropIndex(
                name: "IX_Parent_TenantId",
                table: "Parent");

            migrationBuilder.DropIndex(
                name: "IX_Guardian_TenantId",
                table: "Guardian");

            migrationBuilder.DropIndex(
                name: "IX_GradeClass_TenantId",
                table: "GradeClass");

            migrationBuilder.DropIndex(
                name: "IX_Class_TenantId",
                table: "Class");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "User");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Term");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Teacher");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "SubjectGradeClasses");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "SubjectClasses");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Subject");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "StudentSubjects");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "StudentAttendance");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Session");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Salutation");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Parent");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Guardian");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "GradeClass");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Class");
        }
    }
}
