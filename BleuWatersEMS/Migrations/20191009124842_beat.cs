﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class beat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings");

            migrationBuilder.AlterColumn<int>(
                name: "TermId",
                table: "Settings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "Settings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SessionId",
                table: "Settings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "StudentAssessment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<int>(nullable: false),
                    CAScore = table.Column<int>(nullable: false),
                    ExamsScore = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    GradeClassId = table.Column<int>(nullable: false),
                    ClassId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    TermId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAssessment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_GradeClass_GradeClassId",
                        column: x => x.GradeClassId,
                        principalTable: "GradeClass",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentAssessment_Term_TermId",
                        column: x => x.TermId,
                        principalTable: "Term",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_ClassId",
                table: "StudentAssessment",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_GradeClassId",
                table: "StudentAssessment",
                column: "GradeClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_SessionId",
                table: "StudentAssessment",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_StudentId",
                table: "StudentAssessment",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_SubjectId",
                table: "StudentAssessment",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAssessment_TermId",
                table: "StudentAssessment",
                column: "TermId");

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings",
                column: "SessionId",
                principalTable: "Session",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings",
                column: "TermId",
                principalTable: "Term",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings");

            migrationBuilder.DropTable(
                name: "StudentAssessment");

            migrationBuilder.AlterColumn<int>(
                name: "TermId",
                table: "Settings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "TenantId",
                table: "Settings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SessionId",
                table: "Settings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings",
                column: "SessionId",
                principalTable: "Session",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Tenant_TenantId",
                table: "Settings",
                column: "TenantId",
                principalTable: "Tenant",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings",
                column: "TermId",
                principalTable: "Term",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
