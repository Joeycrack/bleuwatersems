﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings");

            migrationBuilder.AlterColumn<int>(
                name: "TermId",
                table: "Settings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "SessionId",
                table: "Settings",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "Settings",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings",
                column: "SessionId",
                principalTable: "Session",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings",
                column: "TermId",
                principalTable: "Term",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings");

            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "Settings");

            migrationBuilder.AlterColumn<int>(
                name: "TermId",
                table: "Settings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SessionId",
                table: "Settings",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Session_SessionId",
                table: "Settings",
                column: "SessionId",
                principalTable: "Session",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Term_TermId",
                table: "Settings",
                column: "TermId",
                principalTable: "Term",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
