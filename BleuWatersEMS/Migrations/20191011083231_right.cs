﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class right : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ClassTeacherId",
                table: "StudentResult",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ScoreGradeId",
                table: "StudentAssessment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TotalScore",
                table: "StudentAssessment",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Remark",
                table: "ScoreGrade",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClassTeacherId",
                table: "StudentResult");

            migrationBuilder.DropColumn(
                name: "ScoreGradeId",
                table: "StudentAssessment");

            migrationBuilder.DropColumn(
                name: "TotalScore",
                table: "StudentAssessment");

            migrationBuilder.DropColumn(
                name: "Remark",
                table: "ScoreGrade");
        }
    }
}
