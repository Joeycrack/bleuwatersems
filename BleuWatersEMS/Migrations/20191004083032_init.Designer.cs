﻿// <auto-generated />
using System;
using BleuWatersEMS.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BleuWatersEMS.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20191004083032_init")]
    partial class init
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("BleuWatersEMS.Models.Class", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClassName")
                        .IsRequired();

                    b.Property<int>("GradeClassId");

                    b.Property<int>("MaxNumber");

                    b.Property<bool>("Status");

                    b.Property<int>("TeacherId");

                    b.HasKey("ID");

                    b.HasIndex("GradeClassId");

                    b.HasIndex("TeacherId");

                    b.ToTable("Class");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Country", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CountryName")
                        .IsRequired();

                    b.HasKey("ID");

                    b.ToTable("Country");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.GradeClass", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("GradeName")
                        .IsRequired();

                    b.Property<int>("MaxNumber");

                    b.Property<bool>("Status");

                    b.HasKey("ID");

                    b.ToTable("GradeClass");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Guardian", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("EmailAddress");

                    b.Property<string>("FirstName");

                    b.Property<string>("HomeAddress");

                    b.Property<string>("LastName");

                    b.Property<string>("OfficeAddress");

                    b.Property<string>("PhoneNo");

                    b.Property<int?>("SalutationId");

                    b.HasKey("ID");

                    b.ToTable("Guardian");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Parent", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CountryID");

                    b.Property<string>("FatherAddress");

                    b.Property<int>("FatherCountryId");

                    b.Property<string>("FatherEmailAddress");

                    b.Property<string>("FatherFirstName")
                        .IsRequired();

                    b.Property<string>("FatherLastName")
                        .IsRequired();

                    b.Property<string>("FatherLocalGovt");

                    b.Property<string>("FatherOccupation")
                        .IsRequired();

                    b.Property<string>("FatherOfficeAddress");

                    b.Property<string>("FatherOfficePhoneNo");

                    b.Property<string>("FatherPhoneNo")
                        .IsRequired();

                    b.Property<string>("FatherReligion")
                        .IsRequired();

                    b.Property<int>("FatherSalutationId");

                    b.Property<int>("FatherStateId");

                    b.Property<string>("MotherAddress");

                    b.Property<int>("MotherCountryId");

                    b.Property<string>("MotherEmailAddress")
                        .IsRequired();

                    b.Property<string>("MotherFirstName")
                        .IsRequired();

                    b.Property<string>("MotherLastName")
                        .IsRequired();

                    b.Property<string>("MotherLocalGovt");

                    b.Property<string>("MotherOccupation")
                        .IsRequired();

                    b.Property<string>("MotherOfficeAddress");

                    b.Property<string>("MotherPhoneNo")
                        .IsRequired();

                    b.Property<string>("MotherReligion")
                        .IsRequired();

                    b.Property<int>("MotherSalutationId");

                    b.Property<int>("MotherStateId");

                    b.Property<string>("MotherofficePhoneNo");

                    b.Property<bool>("ParentTogether");

                    b.Property<int?>("SalutationID");

                    b.Property<int?>("StateID");

                    b.HasKey("ID");

                    b.HasIndex("CountryID");

                    b.HasIndex("SalutationID");

                    b.HasIndex("StateID");

                    b.ToTable("Parent");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Salutation", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Title")
                        .IsRequired();

                    b.HasKey("ID");

                    b.ToTable("Salutation");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Session", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EndDate");

                    b.Property<bool>("IsCurrentSession");

                    b.Property<string>("SessionName")
                        .IsRequired();

                    b.Property<DateTime>("StartDate");

                    b.HasKey("ID");

                    b.ToTable("Session");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Settings", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("SessionId");

                    b.Property<int?>("TenantId");

                    b.Property<int?>("TermId");

                    b.HasKey("ID");

                    b.HasIndex("SessionId");

                    b.HasIndex("TermId");

                    b.ToTable("Settings");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.State", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("CountryId");

                    b.Property<string>("StateName")
                        .IsRequired();

                    b.HasKey("ID");

                    b.HasIndex("CountryId");

                    b.ToTable("State");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Student", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Active");

                    b.Property<string>("Allegies")
                        .IsRequired();

                    b.Property<int>("ClassId");

                    b.Property<int>("CountryId");

                    b.Property<DateTime>("DateOfBirth");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("Genotype")
                        .IsRequired();

                    b.Property<int>("GradeClassId");

                    b.Property<int?>("GuardianId");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("LocalGovt");

                    b.Property<string>("MiddleName")
                        .IsRequired();

                    b.Property<int>("ParentId");

                    b.Property<string>("PlaceOfBirth")
                        .IsRequired();

                    b.Property<int>("PreviousGradeClass");

                    b.Property<string>("Religion")
                        .IsRequired();

                    b.Property<string>("SchoolLastAttended");

                    b.Property<int?>("SessionID");

                    b.Property<string>("Sex")
                        .IsRequired();

                    b.Property<int>("StartingSession");

                    b.Property<int>("StateId");

                    b.HasKey("ID");

                    b.HasIndex("ClassId");

                    b.HasIndex("CountryId");

                    b.HasIndex("GuardianId");

                    b.HasIndex("ParentId");

                    b.HasIndex("SessionID");

                    b.HasIndex("StateId");

                    b.ToTable("Student");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.StudentAttendance", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("ClassId");

                    b.Property<DateTime>("Date");

                    b.Property<int>("GradeClassId");

                    b.Property<bool>("Present");

                    b.Property<int>("SessionId");

                    b.Property<int>("StudentId");

                    b.Property<int>("TakenBy");

                    b.Property<int>("TermId");

                    b.HasKey("ID");

                    b.HasIndex("ClassId");

                    b.HasIndex("GradeClassId");

                    b.HasIndex("SessionId");

                    b.HasIndex("StudentId");

                    b.HasIndex("TermId");

                    b.ToTable("StudentAttendance");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.StudentSubject", b =>
                {
                    b.Property<int>("SubjectId");

                    b.Property<int>("TeacherId");

                    b.Property<int>("StudentId");

                    b.HasKey("SubjectId", "TeacherId", "StudentId");

                    b.HasIndex("StudentId");

                    b.HasIndex("TeacherId");

                    b.ToTable("StudentSubjects");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Subject", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Status");

                    b.Property<int?>("TeacherID");

                    b.Property<string>("subjectName")
                        .IsRequired();

                    b.HasKey("ID");

                    b.HasIndex("TeacherID");

                    b.ToTable("Subject");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.SubjectClass", b =>
                {
                    b.Property<int>("ClassId");

                    b.Property<int>("SubjectId");

                    b.Property<int>("TeacherId");

                    b.HasKey("ClassId", "SubjectId");

                    b.HasIndex("SubjectId");

                    b.ToTable("SubjectClasses");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.SubjectGradeClass", b =>
                {
                    b.Property<int>("GradeClassId");

                    b.Property<int>("SubjectId");

                    b.HasKey("GradeClassId", "SubjectId");

                    b.HasIndex("SubjectId");

                    b.ToTable("SubjectGradeClasses");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Teacher", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ApplicationUserId")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("MaritalSatus")
                        .IsRequired();

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.Property<string>("Sex")
                        .IsRequired();

                    b.Property<bool>("Status");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("ApplicationUserId");

                    b.ToTable("Teacher");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Term", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("EndDate");

                    b.Property<bool>("IsCurrent");

                    b.Property<int>("SessionId");

                    b.Property<DateTime>("StartDate");

                    b.Property<string>("TermName")
                        .IsRequired();

                    b.HasKey("ID");

                    b.HasIndex("SessionId");

                    b.ToTable("Term");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<bool>("Activated");

                    b.Property<string>("ApplicationUserId");

                    b.Property<int?>("CountryId");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<bool>("IsAcademicStaff");

                    b.Property<bool>("IsAdmin");

                    b.Property<bool>("IsApproved");

                    b.Property<bool>("IsNewUser");

                    b.Property<bool>("IsNonAcademicStaff");

                    b.Property<bool>("IsParent");

                    b.Property<bool>("IsStudent");

                    b.Property<bool>("IsTeacher");

                    b.Property<string>("LastName");

                    b.Property<string>("MaritalStatus");

                    b.Property<string>("PhoneNumber");

                    b.Property<string>("PhoneNumber2");

                    b.Property<int?>("SalutationID");

                    b.Property<string>("Sex");

                    b.Property<int?>("StateId");

                    b.HasKey("ID");

                    b.HasIndex("CountryId");

                    b.HasIndex("SalutationID");

                    b.HasIndex("StateId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole<string>", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Name");

                    b.Property<string>("NormalizedName");

                    b.HasKey("Id");

                    b.ToTable("IdentityRole<string>");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityRole<string>");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId");

                    b.HasKey("Id");

                    b.ToTable("RoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Email");

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail");

                    b.Property<string>("NormalizedUserName");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("Users");

                    b.HasDiscriminator<string>("Discriminator").HasValue("IdentityUser");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId");

                    b.HasKey("Id");

                    b.ToTable("UserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("ProviderKey");

                    b.HasKey("UserId");

                    b.ToTable("UserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("RoleId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("UserId");

                    b.HasKey("RoleId");

                    b.ToTable("UserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId");

                    b.ToTable("UserTokens");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.IdentityRole<string>");

                    b.HasDiscriminator().HasValue("IdentityRole");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.ApplicationUser", b =>
                {
                    b.HasBaseType("Microsoft.AspNetCore.Identity.IdentityUser");

                    b.HasDiscriminator().HasValue("ApplicationUser");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Class", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.GradeClass", "GradeClass")
                        .WithMany()
                        .HasForeignKey("GradeClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Teacher", "Teacher")
                        .WithMany()
                        .HasForeignKey("TeacherId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Parent", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryID");

                    b.HasOne("BleuWatersEMS.Models.Salutation", "Salutation")
                        .WithMany()
                        .HasForeignKey("SalutationID");

                    b.HasOne("BleuWatersEMS.Models.State", "State")
                        .WithMany()
                        .HasForeignKey("StateID");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Settings", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Session", "Session")
                        .WithMany()
                        .HasForeignKey("SessionId");

                    b.HasOne("BleuWatersEMS.Models.Term", "Term")
                        .WithMany()
                        .HasForeignKey("TermId");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.State", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Country", "Countries")
                        .WithMany("States")
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Student", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Class", "Class")
                        .WithMany("Students")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Country", "Country")
                        .WithMany()
                        .HasForeignKey("CountryId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Guardian")
                        .WithMany("Students")
                        .HasForeignKey("GuardianId");

                    b.HasOne("BleuWatersEMS.Models.Parent", "Parent")
                        .WithMany("Students")
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Session", "Session")
                        .WithMany()
                        .HasForeignKey("SessionID");

                    b.HasOne("BleuWatersEMS.Models.State", "State")
                        .WithMany()
                        .HasForeignKey("StateId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.StudentAttendance", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Class", "Class")
                        .WithMany()
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.GradeClass", "GradeClass")
                        .WithMany()
                        .HasForeignKey("GradeClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Session", "Session")
                        .WithMany()
                        .HasForeignKey("SessionId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Student", "Student")
                        .WithMany()
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Term", "Term")
                        .WithMany()
                        .HasForeignKey("TermId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.StudentSubject", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Student", "Student")
                        .WithMany("StudentSubjects")
                        .HasForeignKey("StudentId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Subject", "Subject")
                        .WithMany("StudentSubjects")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Teacher", "Teacher")
                        .WithMany("StudentSubjects")
                        .HasForeignKey("TeacherId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Subject", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Teacher")
                        .WithMany("Subjects")
                        .HasForeignKey("TeacherID");
                });

            modelBuilder.Entity("BleuWatersEMS.Models.SubjectClass", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Class", "Class")
                        .WithMany("SubjectClasses")
                        .HasForeignKey("ClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Subject", "Subject")
                        .WithMany("SubjectClasses")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.SubjectGradeClass", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.GradeClass", "GradeClass")
                        .WithMany("SubjectGradeClasses")
                        .HasForeignKey("GradeClassId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BleuWatersEMS.Models.Subject", "Subject")
                        .WithMany("SubjectGradeClasses")
                        .HasForeignKey("SubjectId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Teacher", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.ApplicationUser", "ApplicationUser")
                        .WithMany()
                        .HasForeignKey("ApplicationUserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.Term", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Session")
                        .WithMany("Terms")
                        .HasForeignKey("SessionId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BleuWatersEMS.Models.User", b =>
                {
                    b.HasOne("BleuWatersEMS.Models.Country", "Countries")
                        .WithMany()
                        .HasForeignKey("CountryId");

                    b.HasOne("BleuWatersEMS.Models.Salutation", "Salutation")
                        .WithMany()
                        .HasForeignKey("SalutationID");

                    b.HasOne("BleuWatersEMS.Models.State", "States")
                        .WithMany()
                        .HasForeignKey("StateId");
                });
#pragma warning restore 612, 618
        }
    }
}
