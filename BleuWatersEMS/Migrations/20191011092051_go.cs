﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class go : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AdminRemarks",
                table: "StudentAssessment",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "Submitted",
                table: "StudentAssessment",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdminRemarks",
                table: "StudentAssessment");

            migrationBuilder.DropColumn(
                name: "Submitted",
                table: "StudentAssessment");
        }
    }
}
