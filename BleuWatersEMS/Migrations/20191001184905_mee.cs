﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class mee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SessionId",
                table: "StudentAttendance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TermId",
                table: "StudentAttendance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_StudentAttendance_SessionId",
                table: "StudentAttendance",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAttendance_TermId",
                table: "StudentAttendance",
                column: "TermId");

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAttendance_Session_SessionId",
                table: "StudentAttendance",
                column: "SessionId",
                principalTable: "Session",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_StudentAttendance_Term_TermId",
                table: "StudentAttendance",
                column: "TermId",
                principalTable: "Term",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StudentAttendance_Session_SessionId",
                table: "StudentAttendance");

            migrationBuilder.DropForeignKey(
                name: "FK_StudentAttendance_Term_TermId",
                table: "StudentAttendance");

            migrationBuilder.DropIndex(
                name: "IX_StudentAttendance_SessionId",
                table: "StudentAttendance");

            migrationBuilder.DropIndex(
                name: "IX_StudentAttendance_TermId",
                table: "StudentAttendance");

            migrationBuilder.DropColumn(
                name: "SessionId",
                table: "StudentAttendance");

            migrationBuilder.DropColumn(
                name: "TermId",
                table: "StudentAttendance");
        }
    }
}
