﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BleuWatersEMS.Migrations
{
    public partial class damn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RecordedBy",
                table: "StudentAssessment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ScoreGrade",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StartScore = table.Column<int>(nullable: false),
                    EndScore = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    ScoreGradeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScoreGrade", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "StudentResult",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    StudentId = table.Column<int>(nullable: false),
                    SessionId = table.Column<int>(nullable: false),
                    TermId = table.Column<int>(nullable: false),
                    CAScore = table.Column<int>(nullable: false),
                    ExamsScore = table.Column<int>(nullable: false),
                    TenantId = table.Column<int>(nullable: false),
                    GradeClassId = table.Column<int>(nullable: false),
                    ClassId = table.Column<int>(nullable: false),
                    Total = table.Column<int>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    ScoregradeId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false),
                    RecordedBy = table.Column<int>(nullable: false),
                    SubjectTeacherId = table.Column<int>(nullable: false),
                    TeacherID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentResult", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StudentResult_Class_ClassId",
                        column: x => x.ClassId,
                        principalTable: "Class",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentResult_GradeClass_GradeClassId",
                        column: x => x.GradeClassId,
                        principalTable: "GradeClass",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentResult_ScoreGrade_ScoregradeId",
                        column: x => x.ScoregradeId,
                        principalTable: "ScoreGrade",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentResult_Session_SessionId",
                        column: x => x.SessionId,
                        principalTable: "Session",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentResult_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentResult_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentResult_Teacher_TeacherID",
                        column: x => x.TeacherID,
                        principalTable: "Teacher",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StudentResult_Term_TermId",
                        column: x => x.TermId,
                        principalTable: "Term",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_ClassId",
                table: "StudentResult",
                column: "ClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_GradeClassId",
                table: "StudentResult",
                column: "GradeClassId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_ScoregradeId",
                table: "StudentResult",
                column: "ScoregradeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_SessionId",
                table: "StudentResult",
                column: "SessionId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_StudentId",
                table: "StudentResult",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_SubjectId",
                table: "StudentResult",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_TeacherID",
                table: "StudentResult",
                column: "TeacherID");

            migrationBuilder.CreateIndex(
                name: "IX_StudentResult_TermId",
                table: "StudentResult",
                column: "TermId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "StudentResult");

            migrationBuilder.DropTable(
                name: "ScoreGrade");

            migrationBuilder.DropColumn(
                name: "RecordedBy",
                table: "StudentAssessment");
        }
    }
}
