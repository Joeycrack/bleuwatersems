﻿using BleuWatersEMS.Helpers;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        //private readonly IDbFactory dbFactory;
        private DataContext dbContext;

        private IDbContextTransaction transaction;


        //public UnitOfWork(IDbFactory dbFactory)
        //{
        //    this.dbFactory = dbFactory;
        //}

        public DataContext DbContext
        {
            get { return dbContext ?? (dbContext); }
        }

        public void SaveChanges()
        {
            DbContext.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            try
            {
                await DbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine("{0} Exception caught.", e);
            }

        }

        public void BeginTransaction()
        {
            transaction = DbContext.Database.BeginTransaction();
        }

        public void Rollback()
        {
            if (transaction != null)
                transaction.Rollback();
            transaction = null;
        }


        public void Commit()
        {
            if (transaction != null)
                transaction.Commit();
            transaction = null;
        }

        public IDbContextTransaction GetCurrentTransaction()
        {
            return transaction ?? DbContext.Database.CurrentTransaction;
        }
    }
}
