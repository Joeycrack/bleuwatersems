﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Infrastructure
{
    public static class IDbSetExtension
    {
        public static Task<TEntity> FindAsync<TEntity>(this DbSet<TEntity> set, params object[] keyValues)
       where TEntity : class
        {
            return Task.Run(() =>
            {
                var entity = set.Find(keyValues);
                return entity;
            });
        }
    }
}
