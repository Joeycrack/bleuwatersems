﻿using BleuWatersEMS.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Infrastructure
{
    //For Local Deployment
    //public class DbFactory : Disposable, IDbFactory
    //{

    //    DataContext dbContext = new DataContext();

    //    public DataContext Init()
    //    {
    //        return dbContext;
    //    }

    //    /// <summary>
    //    /// Set custom DbContext useful during unit testing so that the mock CuzaEnterpriseContext can be passed
    //    /// </summary>
    //    /// <param name="dbContext"></param>
    //    public virtual void SetContext(DataContext dbContext)
    //    {
    //        this.dbContext = dbContext;
    //    }
        
    //    protected override void DisposeCore()
    //    {
    //        if (dbContext != null)
    //            dbContext.Dispose();
    //    }
    //}


    //For Azure Deployment
    //public class DbFactory : Disposable, IDbFactory
    //{

    //    CuzaEnterpriseContext dbContext;

    //    public CuzaEnterpriseContext Init()
    //    {
    //        return dbContext ?? (dbContext = HttpContext.Current.GetOwinContext().Get<CuzaEnterpriseContext>());

    //    }

    //    /// <summary>
    //    /// Set custom DbContext useful during unit testing so that the mock CuzaEnterpriseContext can be passed
    //    /// </summary>
    //    /// <param name="dbContext"></param>
    //    public virtual void SetContext(CuzaEnterpriseContext dbContext)
    //    {
    //        this.dbContext = dbContext;
    //    }

    //    protected override void DisposeCore()
    //    {
    //        if (dbContext != null)
    //            dbContext.Dispose();
    //    }
    //}
}
