﻿using BleuWatersEMS.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Infrastructure
{
    public interface IDbFactory : IDisposable
    {
        DataContext Init();

        void SetContext(DataContext dbContext);
    }
}
