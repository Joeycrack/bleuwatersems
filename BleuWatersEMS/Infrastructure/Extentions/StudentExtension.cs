﻿using BleuWatersEMS.Models;
using BleuWatersEMS.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Infrastructure.Extentions
{
    public static class StudentExtension
    {
        public static Student ConvertToStudent(this StudentRegistrationViewModel vm)
        {
            var student = new Student()
            {
                LastName = vm.StudentLastName,
                ID = vm.ID,
                MiddleName = vm.StudentMiddleName,
                FirstName = vm.StudentFirstName,
                PlaceOfBirth = vm.PlaceOfBirth,
                Religion = vm.Religion,
                Sex = vm.Sex,
                DateOfBirth = vm.DateOfBirth,
                LocalGovt = vm.StudentLocalGovt,
                CountryId = vm.StudentNationalityId,
                StateId = vm.StudentStateId,
                StartingSession = vm.StartingSessionId,
                SchoolLastAttended = vm.SchoolLastAttended,
                PreviousGradeClass = vm.PreviousGradeClassId,
                Genotype = vm.Genotype,
                Allegies = vm.Allergies,
                Active = vm.Active,
                GradeClassId = vm.GradeClassId,
                ClassId = vm.ClassSectionId,
                ParentId = vm.ParentId,
                GuardianId = vm.GuardianId,
                TenantId = vm.TenantId
            };

            return student;
        }

        public static Parent ConvertToParent(this StudentRegistrationViewModel vm)
        {
            var parent = new Parent()
            {
                FatherAddress = vm.FatherAddress,
                ID = vm.ID,
                FatherCountryId = vm.FatherNationalityId,
                FatherEmailAddress = vm.FatherEmailAddress,
                FatherFirstName = vm.FatherFirstName,
                FatherLastName = vm.FatherLastName,
                FatherLocalGovt = vm.FatherLocalGovt,
                FatherOccupation = vm.FatherOccupation,
                FatherOfficeAddress = vm.FatherOfficeAddress,
                FatherOfficePhoneNo = vm.FatherOfficePhoneNo,
                FatherPhoneNo = vm.FatherPhoneNo,
                FatherSalutationId = vm.FatherSalutationId,
                FatherReligion = vm.FatherReligion,
                FatherStateId = vm.FatherStateId,
                MotherAddress = vm.MotherAddress,
                MotherCountryId = vm.MotherNationalityId,
                MotherEmailAddress = vm.MotherEmailAddress,
                MotherFirstName = vm.MotherFirstName,
                MotherLastName = vm.MotherLastName,
                MotherLocalGovt = vm.MotherLocalGovt,
                MotherOccupation = vm.MotherOccupation,
                MotherOfficeAddress = vm.MotherOfficeAddress,
                MotherofficePhoneNo = vm.MotherofficePhoneNo,
                MotherPhoneNo = vm.MotherPhoneNo,
                MotherReligion = vm.MotherReligion,
                MotherSalutationId = vm.MotherSalutationId,
                MotherStateId = vm.MotherStateId,
                ParentTogether = vm.ParentTogether,
                TenantId = vm.TenantId

            };

            return parent;
        }

        public static Guardian ConvertToGuardian(this StudentRegistrationViewModel vm)
        {
            var guardian = new Guardian()
            {
                SalutationId = vm.GuardianSalutationId,
                ID = vm.ID,
                LastName = vm.GuardianLastName,
                FirstName = vm.GuardianFirstName,
                OfficeAddress = vm.GuardianOfficeAddress,
                PhoneNo = vm.GuardianPhoneNo,
                HomeAddress = vm.GuardianHomeAddress,
                EmailAddress = vm.GuardianEmailAddress,
                TenantId = vm.TenantId
            };

            return guardian;
        }

        public static List<StudentSubjectViewModel> ConvertToStudentSubjectVM(this List<StudentSubjectViewModel> StudentSubjectVM, List<SubjectClass> vm)
        {
            List<StudentSubjectViewModel> _list = new List<StudentSubjectViewModel>();
            vm.ForEach(y =>
            {
                StudentSubjectViewModel StudentSubjectVm = new StudentSubjectViewModel()
                {

                    StudentId = 0,
                    SubjectId = y.SubjectId,
                    TeacherId = y.TeacherId,
                };
                _list.Add(StudentSubjectVm);
            });

            return _list;
        }

        public static List<StudentSubject> ConvertToStudentSubject(this List<StudentSubject> StudentSubject, List<StudentSubjectViewModel> StudentSubjectVm, Student student)
        {
            List<StudentSubject> _list = new List<StudentSubject>();
            StudentSubjectVm.ForEach(y =>
            {
                StudentSubject StudentSub = new StudentSubject()
                {

                    StudentId = student.ID,
                    SubjectId = y.SubjectId,
                    TeacherId = y.TeacherId,
                };
                _list.Add(StudentSub);
            });

            return _list;
        }
    }
    
}
