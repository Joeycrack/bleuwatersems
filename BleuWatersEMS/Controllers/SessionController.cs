﻿using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class SessionController : BaseApiController
    {
        private ISessionRepository _sessionRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public SessionController(ISessionRepository sessionRepo, IErrorRepository errorsRepository, IHttpContextAccessor httpContextAccessor)
            : base(errorsRepository)
        {
            _sessionRepo = sessionRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        [AllowAnonymous]
        [HttpPost, Route("AddSession")]
        public async Task<ResponseBase> AddSession([FromBody]SessionViewModel model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gC = await _sessionRepo.GetSessions(TenantId);

                if (_gC.Any(e => e.SessionName == model.SessionName))
                {
                    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a session with the name " + model.SessionName + ". please enter a session with another name" };
                }

                if (model.IsCurrentSession == true && _gC.Any(e => e.IsCurrentSession == true))
                {
                    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a current running session please End it to continue"};
                }

                if (model.Terms.Count > 0)
                {
                    var currTerm = model.Terms.Where(e => e.IsCurrent == true).FirstOrDefault();
                    if (currTerm != null)
                    {
                        var DbCurrTerm = await _sessionRepo.GetTerms(TenantId);
                        if (DbCurrTerm.Any(e => e.IsCurrent == true))
                        {

                            return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a current running Term please End it to continue" };
                        }
                    }
                }
                
                await _sessionRepo.AddSession(model, TenantId);
                
                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetSessions")]
        public async Task<ResponseBase<IEnumerable<Session>>> GetSessions()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var gradeClass = await _sessionRepo.GetSessions(TenantId);

                return new ResponseBase<IEnumerable<Session>>() { IsSuccess = true, Payload = gradeClass.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<Session>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

    }
}