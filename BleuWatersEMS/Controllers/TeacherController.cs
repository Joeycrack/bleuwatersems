﻿using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class TeacherController : BaseApiController
    {
        private ITeacherRepository _teacherRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public TeacherController(ITeacherRepository teacherRepo, IErrorRepository errorsRepository, IHttpContextAccessor httpContextAccessor)
            : base(errorsRepository)
        {
            _teacherRepo = teacherRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        [Authorize]
        [HttpGet, Route("GetTeachers")]
        public async Task<ResponseBase<IEnumerable<Teacher>>> GetTeachers()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _teacherRepo.GetTeachers(TenantId);

                return new ResponseBase<IEnumerable<Teacher>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<Teacher>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet, Route("GetTeacherClasses/{TeacherId}")]
        public async Task<ResponseBase<IEnumerable<TeacherClassSectionsViewModel>>> GetTeacherClasses(int TeacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var classes = await _teacherRepo.GetTeacherClasses(TeacherId, TenantId);

                return new ResponseBase<IEnumerable<TeacherClassSectionsViewModel>>() { IsSuccess = true, Payload = classes.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<TeacherClassSectionsViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet, Route("GetTeacherStudents/{classId}/{teacherId}")]
        public async Task<ResponseBase<IEnumerable<TeacherStudentViewModel>>> GetTeacherStudents(int classId, int TeacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var classes = await _teacherRepo.GetTeacherClassStudents(classId, TeacherId, TenantId);

                return new ResponseBase<IEnumerable<TeacherStudentViewModel>>() { IsSuccess = true, Payload = classes.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<TeacherStudentViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }
    }
}
