﻿using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class StudentAssessmentController : BaseApiController
    {
        private IStudentAssessmentRepository _studentAssessmentRepo;
        private IHttpContextAccessor _httpContextAccessor;

        public StudentAssessmentController(IStudentAssessmentRepository studentAssessmentRepo, IHttpContextAccessor httpContextAccessor, IErrorRepository errorRepository)
           : base(errorRepository)
        {
            _studentAssessmentRepo = studentAssessmentRepo;
            _httpContextAccessor = httpContextAccessor;
        }
        
        [Authorize]
        [HttpGet, Route("GetClassSubjectAssessment/{classId}/{subjectId}")]
        public async Task<ResponseBase<AssesmentRetunViewModel>> GetClassSubjectAssessment(int classId, int subjectId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var students = await _studentAssessmentRepo.GetClassStudentsAssesment(classId, subjectId, TenantId);
                var subject = students.Select(e => e.SubjectName).FirstOrDefault();
                var className = students.Select(e => e.ClassName).FirstOrDefault();

                AssesmentRetunViewModel assesment = new AssesmentRetunViewModel
                {
                    SubjectName = subject,
                    ClassName = className,
                    Assesments = students
                };

                return new ResponseBase<AssesmentRetunViewModel>() { IsSuccess = true, Payload = assesment };
            }
            catch (Exception ex)
            {
                return new ResponseBase<AssesmentRetunViewModel>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        
        [HttpPost, Route("AddStudentAssesmts")]
        public async Task<ResponseBase> AddStudentAssesmts([FromBody]AssessmentViewModel model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                //var _gC = await _sessionRepo.GetSessions(TenantId);

                //if (_gC.Any(e => e.SessionName == model.SessionName))
                //{
                //    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a session with the name " + model.SessionName + ". please enter a session with another name" };
                //}

                //if (model.IsCurrentSession == true && _gC.Any(e => e.IsCurrentSession == true))
                //{
                //    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a current running session please End it to continue" };
                //}
                
                await _studentAssessmentRepo.AddStudentAssessment(model, TenantId);

                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [HttpPost, Route("SubmitStudentAssesmts")]
        public async Task<ResponseBase> SubmitStudentAssesmts([FromBody]List<AssessmentViewModel> model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                
                await _studentAssessmentRepo.SubmittedStudentAssessment(model, TenantId);

                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }
    }
}
