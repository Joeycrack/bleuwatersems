﻿using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class SubjectController : BaseApiController
    {
        private ISubjectRepository _subjectRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public SubjectController(ISubjectRepository subjectRepo, IHttpContextAccessor httpContextAccessor, IErrorRepository errorsRepository)
            : base(errorsRepository)
        {
            _subjectRepo = subjectRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        
        [HttpPost, Route("AddSubject")]
        public async Task<ResponseBase> AddSubject([FromBody]SubjectViewModel model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gC = await _subjectRepo.GetSubjects(TenantId);

                if (_gC.Any(e => e.subjectName.ToUpper() == model.subjectName.ToUpper()))
                {
                    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a subject with the name " + model.subjectName + ". please enter a class with another name" };
                }

                
                await _subjectRepo.AddSubject(model, TenantId);
                //_unitOfWork.SaveChanges();
                //_unitOfWork.Commit();

                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetSubjects")]
        public async Task<ResponseBase<IEnumerable<Subject>>> GetSubjects()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _subjectRepo.GetSubjects(TenantId);

                return new ResponseBase<IEnumerable<Subject>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<Subject>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet("GetGradeClassSubjects/{id}")]
        public async Task<ActionResult<ResponseBase<IEnumerable<SubjectGradeClassViewModel>>>>  GetGradeClassSubjects(int id)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _subjectRepo.GetGradeClassSubjects(id, TenantId);

                return new ResponseBase<IEnumerable<SubjectGradeClassViewModel>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<SubjectGradeClassViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet("GetTeacherSubjects/{teacherId}/{classId}")]
        public async Task<ActionResult<ResponseBase<IEnumerable<SubjectTeacherViewModel>>>> GetTeacherSubjects(int teacherId, int classId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _subjectRepo.GetTeacherClassSubjects(teacherId, classId, TenantId);

                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet("GetClassSubjects/{classId}")]
        public async Task<ActionResult<ResponseBase<IEnumerable<SubjectTeacherViewModel>>>> GetClassSubjects(int classId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _subjectRepo.GetClassSectionSubjects(classId, TenantId);

                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet("TeacherSubjectsList/{teacherId}/")]
        public async Task<ActionResult<ResponseBase<IEnumerable<SubjectTeacherViewModel>>>> TeacherSubjectsList (int teacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var subjects = await _subjectRepo.TeacherSubjectsList(teacherId, TenantId);

                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { IsSuccess = true, Payload = subjects.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<SubjectTeacherViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

    }
}
