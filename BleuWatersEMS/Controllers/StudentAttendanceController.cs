﻿using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class StudentAttendanceController : BaseApiController
    {
        private IStudentAttendanceRepository _studentAttendanceRepo;
        private IStudentRepository _studentRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public StudentAttendanceController(IStudentAttendanceRepository studentAttendanceRepo, IStudentRepository studentRepo, IHttpContextAccessor httpContextAccessor, IErrorRepository errorsRepository)
            : base(errorsRepository)
        {
            _studentAttendanceRepo = studentAttendanceRepo;
            _studentRepo = studentRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        [AllowAnonymous]
        [HttpPost, Route("AddStudentAttendance")]
        public async Task<ResponseBase> AddStudentAttendance([FromBody]StudentAttendanceViewModel model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gC = await _studentAttendanceRepo.GetAttendance(model, TenantId);

                if (_gC == null) {
                    await _studentAttendanceRepo.AddAttendance(model, TenantId);
                }
                
                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetStudentsForAttendance")]
        public async Task<ResponseBase<IEnumerable<StudentAttendanceViewModel>>> GetStudentsForAttendance()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _studentAtt = await _studentAttendanceRepo.GetStudentsAtt(TenantId);

                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { Payload = _studentAtt, IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { ErrorDetails = ex.Message, Payload = null };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("ClassStudentsAttendance/{classId}")]
        public async Task<ResponseBase<IEnumerable<StudentAttendanceViewModel>>>ClassStudentsAttendance(int classId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gc = await _studentAttendanceRepo.GetClassAttendances(classId, TenantId);
                IEnumerable<StudentAttendanceViewModel> students;
                if (_gc.Count() <= 0 )
                {
                    students = await _studentAttendanceRepo.ClassStudentsAttendance(classId, TenantId);

                    return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { IsSuccess = true, Payload = students.ToList() };
                } else
                {
                    return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { IsSuccess = true, Payload = _gc.ToList() };
                }

                

                
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }


        [AllowAnonymous]
        [HttpGet("TeacherAttendanceToday/{teacherId}")]
        public async Task<ResponseBase<IEnumerable<StudentAttendanceViewModel>>> TeacherAttendanceToday(int teacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _studentAtt = await _studentAttendanceRepo.TeacherAttendanceToday(teacherId, TenantId);

                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { Payload = _studentAtt, IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { ErrorDetails = ex.Message, Payload = null };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetTodayAttendance")]
        public async Task<ResponseBase<IEnumerable<StudentAttendanceViewModel>>> GetTodayAttendance()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _studentAtt = await _studentAttendanceRepo.GetAttendance(TenantId);

                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { Payload = _studentAtt, IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<StudentAttendanceViewModel>>() { ErrorDetails = ex.Message, Payload = null };
            }
        }
    }
}
