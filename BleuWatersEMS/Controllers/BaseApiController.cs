﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Primitives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace BleuWatersEMS.Controllers
{
    public class BaseApiController
    {
        protected readonly IErrorRepository _errorsRepository;
        protected readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DataContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private StringValues tenant;
        //public  _tenant;

        //public string Tenant()
        //{
        //    //For Azure
        //    // get { return ClaimsPrincipal.Current.TenantId(); }

        //    //For local
        //    //get { return int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value); }
        //    var _tenant = _httpContextAccessor.HttpContext.User.FindFirst("id").Value;
        //    return _tenant;
        //    // me
        //}

        //public string TenantId {
        //    //For Azure
        //    // get { return ClaimsPrincipal.Current.TenantId(); }

        //    //For local
        //    get { return _httpContextAccessor.HttpContext.User.FindFirst("TenantId").Value; }

        //    // me


        //}

        public Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(_httpContextAccessor.HttpContext.User);
        }

        //public int User
        //{
        //   get {return _httpContextAccessor.HttpContext.User.FindFirst("TenantId")
        //}

        public BaseApiController(IErrorRepository errorsRepository)
        {
            _errorsRepository = errorsRepository;
            //_unitOfWork = unitOfWork;
        }

        protected virtual ResponseBase ExecuteRequest(Action executeMethod)
        {
            ResponseBase responseBase = new ResponseBase();
            try
            {
                executeMethod();
            }
            catch (Exception ex)
            {
                LogError(ex);
                responseBase.ErrorDetails = ex.Message;
            }
            return responseBase;
        }

        protected virtual ResponseBase<T> ExecuteRequest<T>(Func<T> executeMethod)
        {
            ResponseBase<T> responseBase = new ResponseBase<T>();
            try
            {
                responseBase.Payload = executeMethod();
            }
            catch (Exception ex)
            {
                LogError(ex);
                responseBase.ErrorDetails = ex.Message;
            }
            return responseBase;
        }

        protected virtual async Task<ResponseBase<T>> ExecuteRequestAsync<T>(Func<Task<T>> executeMethod)
        {
            ResponseBase<T> responseBase = new ResponseBase<T>();

            try
            {
                responseBase.Payload = await executeMethod();
            }
            catch (Exception ex)
            {
                LogError(ex);
                responseBase.ErrorDetails = ex.Message;
            }
            return responseBase;

        }


        private void LogError(Exception ex)
        {
            try
            {
                Error _error = new Error()
                {
                    Message = ex.Message,
                    StackTrace = ex.StackTrace,
                    DateCreated = DateTime.Now
                };
                _errorsRepository.Add(_error);
                _unitOfWork.SaveChangesAsync();
            }
            catch { }
        }
    }
}
