﻿using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class StudentController : BaseApiController
    {
        private IStudentRepository _studentRepo;
        private ISessionRepository _sessionRepo;
        private IGradeClassRepository _gradeClassRepo;
        private ISalutationRepository _salutationRepo;
        private ICountryStateRepository _countryStateRepo;
        private IClassSectionRepository _classSectionRepo;
        private IHttpContextAccessor _httpContextAccessor;
        
        public StudentController(IStudentRepository studentRepo, IClassSectionRepository classSectionRepo, IHttpContextAccessor httpContextAccessor, ISalutationRepository salutationRepo, IGradeClassRepository gradeClassRepo, ICountryStateRepository countryStateRepo, ISessionRepository sessionRepo, IErrorRepository errorsRepository)
            : base(errorsRepository)
        {
            _studentRepo = studentRepo;
            _classSectionRepo = classSectionRepo;
            _countryStateRepo = countryStateRepo;
            _salutationRepo = salutationRepo;
            _gradeClassRepo = gradeClassRepo;
            _sessionRepo = sessionRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        [AllowAnonymous]
        [HttpPost, Route("AddStudent")]
        public async Task<ResponseBase> AddStudent([FromBody]StudentRegistrationViewModel model)
        {
            try
            {
                // Here we need to consider the student registration Number

                //var _gC = await _subjectRepo.GetSubjects();

                //if (_gC.Any(e => e.subjectName.ToUpper() == model.subjectName.ToUpper()))
                //{
                //    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a subject with the name " + model.subjectName + ". please enter a class with another name" };
                //}

                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                await _studentRepo.AddStudent(model, TenantId);
                await _unitOfWork.SaveChangesAsync();

                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [Authorize]
        [HttpGet, Route("GetStudents")]
        public async Task<ResponseBase<IEnumerable<Student>>> GetStudents()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var students = await _studentRepo.GetStudents(TenantId);

                return new ResponseBase<IEnumerable<Student>>() { IsSuccess = true, Payload = students.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<Student>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet, Route("GetStudentList")]
        public async Task<ResponseBase<IEnumerable<StudentViewModel>>> GetStudentList()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var students = await _studentRepo.GetStudentList(TenantId);

                return new ResponseBase<IEnumerable<StudentViewModel>>() { IsSuccess = true, Payload = students.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<StudentViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        //[Authorize(Policy = "Basic")]
        [Authorize]
        [HttpGet, Route("AddStudentInitial")]
        public async Task<ResponseBase<AddStudentInitialList>> AddStudentInitial()
        {
            try
            {
                //var ten = Tenant();

                //var user = await GetCurrentUserAsync();
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var gradeclass = await _gradeClassRepo.GetGradeClasses(TenantId);
                var session = await _sessionRepo.GetSessions(TenantId);
                var classSection = await _classSectionRepo.GetClassSections(TenantId);
                var country = await _countryStateRepo.GetCountries();
                var state = await _countryStateRepo.GetStates();
                var salutation = await _salutationRepo.GetSalutations(TenantId);
            
                AddStudentInitialList initialList = new AddStudentInitialList();
                
                initialList.GradeClasses = gradeclass;
                initialList.sessions = session;
                initialList.classSections = classSection;
                initialList.countries = country;
                initialList.states = state;
                initialList.salutations = salutation;
                
                return new ResponseBase<AddStudentInitialList>() { IsSuccess = true, Payload = initialList };
            }
            catch (Exception ex)
            {
                return new ResponseBase<AddStudentInitialList>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }



        
    }
}
