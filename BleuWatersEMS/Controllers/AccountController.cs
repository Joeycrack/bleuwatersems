﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;


namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseApiController
    {
        private const string LocalLoginProvider = "Local";
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEmailSender _emailSender;
        private readonly IAccountRepository _accountRepo;
        private readonly IUserRepository _userRepo;
        private readonly ITeacherRepository _teacherRepo;
        private readonly ITenantRepository _tenantRepo;
        private readonly IJwtFactory _jwtFactory;
        private readonly JwtIssuerOptions _jwtOptions;
        private StringValues tenant;

        public AccountController(IHttpContextAccessor httpContextAccessor, IUserRepository userRepo, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions, ITenantRepository tenantRepo, ITeacherRepository teacherRepo, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IEmailSender emailSender, IErrorRepository errorsRepository, IAccountRepository accountRepository)
            : base(errorsRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _emailSender = emailSender;
            _accountRepo = accountRepository;
            _userManager = userManager;
            _signInManager = signInManager;
            _userRepo = userRepo;
            _teacherRepo = teacherRepo;
            _tenantRepo = tenantRepo;
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;

        }

        private void SetAccessTokenCookie(string accessToken)
        {
            string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies["key"];
            //read cookie from Request object  
            string cookieValueFromReq = _httpContextAccessor.HttpContext.Request.Cookies["Key"];
            //set the key value in Cookie

            _httpContextAccessor.HttpContext.Response.Cookies.Append(
            "AccessToken",
            accessToken,
            new CookieOptions()
            {

            });

        }

        [AllowAnonymous]
        [HttpPost, Route("Login")]
        public async Task<ResponseBase<LoginResponseData>> Login([FromBody]LoginRequestData model)
        {
            try
            {
                
                await _signInManager.SignOutAsync();

                var lrd = new LoginResponseData();
                ApplicationUser user = await _userManager.FindByNameAsync(model.username);
                if (user == null)
                {
                    return new ResponseBase<LoginResponseData>() { ErrorDetails = "Login details entered is incorrect", Payload = null };
                }

                var result = await _signInManager.PasswordSignInAsync(user, model.password, false, false);
                var _user = _userRepo.GetByAppUserId(user.Id);

                // BUG: This shouldn't allow login if email still requires verification
                if (result.Succeeded)
                {
                    
                    //var header = _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("TenantId", out tenant);
                    //if (header == true)
                    //{
                    //    var hed = int.Parse(tenant);
                    //}
                    //var nw = int.Parse(tenant);
                    //var bw = TenantId;
                   
                    var identity = await GetClaimsIdentity(model.username, model.password);
                    if (identity == null)
                    {
                        return new ResponseBase<LoginResponseData>() { ErrorDetails = "Login details entered is incorrect", Payload = null };
                    }
                    
                    var jwt = await Tokens.GenerateJwt(identity, _jwtFactory, model.username, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
                    Token Token = JsonConvert.DeserializeObject<Token>(jwt);
                    _user.TenantId = Token.TenantId;
                    _user.Token = Token.auth_Token;
                    //Tenant(_user.TenantId);
                     

                    if (result.RequiresTwoFactor)
                    {
                        //return RedirectToPage("./SendCode", new { returnUrl, RememberMe });
                    }
                    if (result.IsLockedOut)
                    {
                        
                        return new ResponseBase<LoginResponseData>() { ErrorDetails = "This username does not Exist check it and try again or signup", Payload = null };
                    }

                    if (_user.User.IsNewUser == true)
                    {
                        return new ResponseBase<LoginResponseData>() { ErrorDetails = "You  have not been Approved or assigned a role yet contact the Admin", Payload = null };
                    }

                    if (_user.User.Activated != true)
                    {
                        return new ResponseBase<LoginResponseData>() { ErrorDetails = "Your  account has been deactivated contact the admin", Payload = null };
                    }

                    
                    //var access_token = await _userManager.(user, LocalLoginProvider, "AuthToken");

                    //SetAccessTokenCookie(access_token);
                    //lrd.AccessToken = access_token;
                    //lrd.User.UserID = user.Id;
                    //lrd.User.Email = user.Email;
                } else
                {
                    return new ResponseBase<LoginResponseData>() { ErrorDetails = "Login details entered is incorrect", Payload = null };
                }

                //lrd.User.IsAdmin = _user.User.IsAdmin;
                //lrd.User.IsParent = _user.User.IsParent;
                //lrd.User.IsStudent = _user.User.IsStudent;
                //lrd.User.IsStaff = _user.User.IsStaff;

                //lrd.RefreshToken = response.refresh_token;

                //SetAccessTokenCookie(lrd.AccessToken);
                
                return new ResponseBase<LoginResponseData>() { Payload = _user, IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase<LoginResponseData>() { ErrorDetails = ex.Message, Payload = null };
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("Register")]
        public async Task<ResponseBase> Register([FromBody]Tenant model)
        {
            var checkUserName = _accountRepo.checkUserName(model.Email);

            if (checkUserName > 0)
            {
                return new ResponseBase() { ErrorDetails = "Email Already taken by another user", IsSuccess = false };
            }

            Tenant newSchool = new Tenant()
            {
                SchoolName = model.SchoolName,
                CountryId = model.CountryId,
                StateId = model.StateId,
                Address = model.Address,
                PhoneNumber = model.PhoneNumber,
                PhoneNumber2 = model.PhoneNumber2,
                BadgeUrl = model.BadgeUrl,
                Motto = model.Motto,
                Email = model.Email,
                Website = model.Website,
                PlanTypeId = model.PlanTypeId,
            };

            await _tenantRepo.AddTenant(newSchool);

            ApplicationUser user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                //FirstName = model.FirstName,
                //LastName = model.LastName,
                //CountryId = model.CountryId,
                // StateId = model.StateId,
                PhoneNumber = model.PhoneNumber,
                TenantId = newSchool.ID
                //IsAdmin = false,
                //IsNewUser = true,
                //IsParent = false,
                //IsStudent = false

            };

            try
            {
                var result = await _userManager.CreateAsync(user, model.PhoneNumber);
                //result = await _userManager.AddToRoleAsync(user, "ADMIN");

                if (!result.Succeeded)
                {

                    return new ResponseBase() { ErrorDetails = "Make your password stronger and try again ", IsSuccess = false };
                }
                else
                {

                    try
                    {
                       
                        await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("TenantId", newSchool.ID.ToString()));

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                        // Send an email with this link
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                        // TODO  I need To Implement email confirmation

                        //var callbackUrl = Url.Route("./ConfirmEmail", values: { userId: user.Id, Code: code }, protocol: _httpContextAccessor.HttpContext.Request.Scheme);
                        //await ConfirmEmail(user, code);


                        // HACK: Having the email body be a string in the code isn't great, we should look at making this a separate .html file,
                        //       or even a protected Razor Page that accepts the UserId so that it can have a model, be templated, etc. It could be read once and cached.

                        //await _emailSender.SendEmailAsync(user.Email, "Account Verification",
                        //$"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>link</a>");

                        //await _signInManager.SignInAsync(user, isPersistent: false);
                        await _signInManager.SignOutAsync();
                        user.LockoutEnabled = false;
                        user.EmailConfirmed = false;

                        User newUser = new User()
                        {
                            FirstName = model.SchoolName,
                            LastName = "NIL",
                            //CountryId = model.CountryId,
                            //StateId = model.StateId,
                            PhoneNumber = model.PhoneNumber,
                            Sex = "NIL",
                            MaritalStatus = "NIL",
                            Email = model.Email,
                            ApplicationUserId = user.Id,
                            IsAdmin = true,
                            IsNewUser = false,
                            IsParent = false,
                            IsStudent = false,
                            IsApproved = true,
                            IsTeacher = false,
                            IsNonAcademicStaff = false,
                            Activated = true,
                            TenantId = newSchool.ID

                        };

                        await _userRepo.AddUser(newUser, newSchool.ID);

                        return new ResponseBase() { IsSuccess = true };
                        
                    }
                    catch (Exception ex)
                    {
                        return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = true };
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AppException(ex.Message);
            }


        }

        [AllowAnonymous]
        [HttpGet, Route("Logout")]
        public async Task<ResponseBase> Logout()
        {
            
            try
            {
                await _signInManager.SignOutAsync();
                return new ResponseBase() { IsSuccess = true };

            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }

        }

        [Authorize]
        [HttpPost, Route("AddUser")]
        public async Task<ResponseBase> AddUser([FromBody]RegistrationViewModel model)
        {
            var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
            var checkUserName = _accountRepo.checkUserName(model.Email);

            if (checkUserName > 0)
            {
                return new ResponseBase() { ErrorDetails = "Email Already taken by another user", IsSuccess = false };
            }

            ApplicationUser user = new ApplicationUser()
            {
                UserName = model.Email,
                Email = model.Email,
                //FirstName = model.FirstName,
                //LastName = model.LastName,
                //CountryId = model.CountryId,
                // StateId = model.StateId,
                PhoneNumber = model.PhoneNumber,
                TenantId = TenantId
                //IsAdmin = false,
                //IsNewUser = true,
                //IsParent = false,
                //IsStudent = false

            };

            try
            {
                var result = await _userManager.CreateAsync(user, model.PhoneNumber);
                //result = await _userManager.AddToRoleAsync(user, "ADMIN");

                if (!result.Succeeded)
                {

                    return new ResponseBase() { ErrorDetails = "Make your password stronger and try again ", IsSuccess = false };
                }
                else
                {

                    try
                    {
                        await _userManager.AddClaimAsync(user, new System.Security.Claims.Claim("UserName", user.UserName));

                        // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=532713
                        // Send an email with this link
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                        // TODO  I need To Implement email confirmation

                        //var callbackUrl = Url.Route("./ConfirmEmail", values: { userId: user.Id, Code: code }, protocol: _httpContextAccessor.HttpContext.Request.Scheme);
                        //await ConfirmEmail(user, code);


                        // HACK: Having the email body be a string in the code isn't great, we should look at making this a separate .html file,
                        //       or even a protected Razor Page that accepts the UserId so that it can have a model, be templated, etc. It could be read once and cached.

                        //await _emailSender.SendEmailAsync(user.Email, "Account Verification",
                        //$"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>link</a>");

                        //await _signInManager.SignInAsync(user, isPersistent: false);
                        await _signInManager.SignOutAsync();
                        user.LockoutEnabled = false;
                        user.EmailConfirmed = false;

                        User newUser = new User()
                        {
                            FirstName = model.FirstName,
                            LastName = model.LastName,
                            //CountryId = model.CountryId,
                            //StateId = model.StateId,
                            PhoneNumber = model.PhoneNumber,
                            Sex = model.Sex,
                            MaritalStatus = model.MaritalStatus,
                            Email = model.Email,
                            ApplicationUserId = user.Id,
                            IsAdmin = model.IsAdmin,
                            IsNewUser = true,
                            IsParent = false,
                            IsStudent = false,
                            IsApproved = false,
                            IsTeacher = model.IsTeacher,
                            IsNonAcademicStaff = model.IsNonAcademicStaff,
                            Activated = true,
                            TenantId = TenantId


                        };

                        if (newUser.IsAdmin == true || newUser.IsTeacher == true || newUser.IsNonAcademicStaff == true)
                        {
                            newUser.IsNewUser = false;
                            newUser.IsApproved = true;
                        }

                        await _userRepo.AddUser(newUser, TenantId);

                        if (newUser.IsTeacher == true)
                        {
                            Teacher teacher = new Teacher()
                            {
                                Status = true,
                                FirstName = newUser.FirstName,
                                LastName = newUser.LastName,
                                UserId = newUser.ID,
                                ApplicationUserId = newUser.ApplicationUserId,
                                PhoneNumber = newUser.PhoneNumber,
                                Sex = newUser.Sex,
                                MaritalSatus = newUser.MaritalStatus
                            };

                            await _teacherRepo.AddTeacher(teacher, TenantId);
                        }
                        
                        return new ResponseBase() { IsSuccess = true };
                    }
                    catch (Exception ex)
                    {
                        return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = true };
                    }
                }
            }
            catch (Exception ex)
            {
                throw new AppException(ex.Message);
            }
            
        }

        [Authorize]
        [HttpGet, Route("GetUsers")]
        public async Task<ResponseBase<IEnumerable<User>>> GetUsers()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _users = await _userRepo.GetUsers(TenantId);

                return new ResponseBase<IEnumerable<User>>() { Payload = _users, IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<User>>() { ErrorDetails = ex.Message, Payload = null };
            }
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // check the credentials
            if (await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                var user = _userRepo.GetByAppUserId(userToVerify.Id);
                var _planName = _tenantRepo.GetTenantPlanName(user.User.TenantId);
                var _planId = _tenantRepo.GetTenantPlanId(user.User.TenantId);

                if (_planName == "Basic")
                {
                    return await Task.FromResult(_jwtFactory.GenerateClaimsIdentityBasicPlan(userName, user.User.TenantId.ToString(), _planId.ToString()));
                }

                if (_planName == "Cooperate")
                {
                    return await Task.FromResult(_jwtFactory.GenerateClaimsIdentityCooperatePlan(userName, userToVerify.Id));
                }

                if (_planName == "Enterprise")
                {
                    return await Task.FromResult(_jwtFactory.GenerateClaimsIdentityEnterprisePlan(userName, userToVerify.Id));
                }
                
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
    }
}
