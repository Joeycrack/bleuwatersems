﻿using BleuWatersEMS.Helpers;
using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Route("api/[controller]")]
    public class GradeClassController : BaseApiController
    {
        private IGradeClassRepository _gradeClassRepo;
        private ITeacherRepository _teacherRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public GradeClassController(IGradeClassRepository gradeClassRepo, ITeacherRepository teacherRepo, IHttpContextAccessor httpContextAccessor, IErrorRepository errorsRepository)
            : base(errorsRepository)
        {
            _gradeClassRepo = gradeClassRepo;
            _teacherRepo = teacherRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        [AllowAnonymous]
        [HttpPost, Route("AddGradeClass")]
        public async Task<ResponseBase> AddGradeClass([FromBody]GradeClass model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gC = await _gradeClassRepo.GetGradeClasses(TenantId);

                if (_gC.Any(e => e.GradeName == model.GradeName))
                {
                    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a class with the name " + model.GradeName + ". please enter a class with another name" };
                }

                await _gradeClassRepo.AddGradeClass(model, TenantId);
                
                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetGradeClasses")]
        public async Task<ResponseBase<IEnumerable<GradeClass>>> GetGradeClasses()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var gradeClass =  await _gradeClassRepo.GetGradeClasses(TenantId);

                return new ResponseBase<IEnumerable<GradeClass>>() { IsSuccess = true, Payload = gradeClass.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<GradeClass>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetGradeClassesAndTeachers")]
        public async Task<ResponseBase<GradeClassAndTeacherViweModel>> GetGradeClassesAndTeachers()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var gradeClass = await _gradeClassRepo.GetGradeClasses(TenantId);

                var teachers = await _teacherRepo.GetTeacherList(TenantId);

                GradeClassAndTeacherViweModel fullPackage = new GradeClassAndTeacherViweModel();
                fullPackage.GradeClasses = gradeClass;
                fullPackage.Teachers = teachers;

                return new ResponseBase<GradeClassAndTeacherViweModel>() { IsSuccess = true, Payload = fullPackage };
            }
            catch (Exception ex)
            {
                return new ResponseBase<GradeClassAndTeacherViweModel>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

    }
}
