﻿using BleuWatersEMS.Infrastructure;
using BleuWatersEMS.Models;
using BleuWatersEMS.Repository;
using BleuWatersEMS.Utilities;
using BleuWatersEMS.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class ClassSectionController : BaseApiController
    {
        private IClassSectionRepository _classSectionRepo;
        private ITeacherRepository _teacherRepo;
        private IHttpContextAccessor _httpContextAccessor;
        public ClassSectionController(IClassSectionRepository classSectionRepo, IHttpContextAccessor httpContextAccessor, IErrorRepository errorsRepository)
            : base(errorsRepository)
        {
            _classSectionRepo = classSectionRepo;
            _httpContextAccessor = httpContextAccessor;
        }

        //[AllowAnonymous]
        [HttpPost, Route("AddClassSection")]
        public async Task<ResponseBase> AddClassSection([FromBody]ClassSectionViewModel model)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var _gC = await _classSectionRepo.GetClassSections(TenantId);

                if (_gC.Any(e => e.ClassName.ToUpper() == model.ClassName.ToUpper()))
                {
                    return new ResponseBase() { IsSuccess = false, ErrorDetails = "there is already a class Section with the name " + model.ClassName + ". please enter a class with another name" };
                }


                await _classSectionRepo.AddClassSection(model, TenantId);
                return new ResponseBase() { IsSuccess = true };
            }
            catch (Exception ex)
            {
                return new ResponseBase() { ErrorDetails = ex.Message, IsSuccess = false };
            }
        }

        [Authorize]
        [HttpGet, Route("GetClassSections")]
        public async Task<ResponseBase<IEnumerable<GetClassSectionsViewModel>>> GetClassSections()
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var sections = await _classSectionRepo.GetClassSections(TenantId);

                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { IsSuccess = true, Payload = sections.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        [Authorize]
        [HttpGet, Route("GetTeacherClassSections/{teacherId}")]
        public async Task<ResponseBase<IEnumerable<GetClassSectionsViewModel>>> GetTeacherClassSections(int teacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var sections = await _classSectionRepo.GetTeacherClassSections(teacherId, TenantId);

                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { IsSuccess = true, Payload = sections.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

        // gets a list of class where a subject teacher
        // teaches at least one subject
        [HttpGet, Route("TeacherClassDropDwn/{teacherId}")]
        public async Task<ResponseBase<IEnumerable<GetClassSectionsViewModel>>> TeacherClassDropDwn(int teacherId)
        {
            try
            {
                var TenantId = int.Parse(_httpContextAccessor.HttpContext.User.FindFirst("id").Value);
                var sections = await _classSectionRepo.TeacherClassDropDwn(teacherId, TenantId);

                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { IsSuccess = true, Payload = sections.ToList() };
            }
            catch (Exception ex)
            {
                return new ResponseBase<IEnumerable<GetClassSectionsViewModel>>() { ErrorDetails = ex.Message, IsSuccess = false, Payload = null };
            }
        }

    }
}
