﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Helpers
{
    public static class Constants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifiers
            {
                public const string Rol = "rol", TenantId = "id", PlanType = "PlanType", PlanTyp = "plantyp";
            }

            public static class JwtClaims
            {
                public const string ApiAccess = "api_access";
                public const string BasicPlan = "Basic";
                public const string CooperatePlan = "Cooperate"; 
                public const string EnterprisePlan = "Enterprise";
            }
        }
    }
}
