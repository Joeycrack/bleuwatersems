﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace BleuWatersEMS.Helpers
{
    public class JwtFactory : IJwtFactory
    {
        private readonly JwtIssuerOptions _jwtOptions;
        private IConfiguration _config;

        public JwtFactory(IConfiguration config)
        {
            //_jwtOptions = jwtOptions.Value;
            //ThrowIfInvalidOptions(_jwtOptions);
            _config = config;
        }

        public async Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            //var claims = new[]
            //{
            //     new Claim(JwtRegisteredClaimNames.Sub, userName),
            //     new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
            //     new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
            //     identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Rol),
            //     identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.TenantId)
            // };

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                //new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                //new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                identity.FindFirst(Constants.Strings.JwtClaimIdentifiers.PlanTyp),
                identity.FindFirst(Constants.Strings.JwtClaimIdentifiers.TenantId),
                identity.FindFirst(Constants.Strings.JwtClaimIdentifiers.PlanType),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              claims,
              expires: DateTime.Now.AddMinutes(120),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);

            // Create the JWT security token and encode it.
            //var jwt = new JwtSecurityToken(
            //    issuer: _jwtOptions.Issuer,
            //    audience: _jwtOptions.Audience,
            //    claims: claims,
            //    notBefore: _jwtOptions.NotBefore,
            //    expires: _jwtOptions.Expiration,
            //    signingCredentials: _jwtOptions.SigningCredentials);

            //var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            //return encodedJwt;
        }

        // Generate claims Identity for users whose Tenant 
        // is on the basic plan
        public ClaimsIdentity GenerateClaimsIdentityBasicPlan(string userName, string TenantId, string PlanTypeId)
        {
            return new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
            {
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.PlanTyp, PlanTypeId),
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.TenantId, TenantId),
                new Claim(Constants.Strings.JwtClaimIdentifiers.PlanType, "Basic")
            });
        }

        // Generate claims Identity for users whose Tenant 
        // is on the enterprise plan
        public ClaimsIdentity GenerateClaimsIdentityEnterprisePlan(string userName, string TenantId)
        {
            return new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
            {
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.TenantId, TenantId),
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.PlanType, Helpers.Constants.Strings.JwtClaims.EnterprisePlan)
            });
        }

        // Generate claims Identity for users whose Tenant 
        // is on the cooperate plan
        public ClaimsIdentity GenerateClaimsIdentityCooperatePlan(string userName, string TenantId)
        {
            return new ClaimsIdentity(new GenericIdentity(userName, "Token"), new[]
            {
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.TenantId, TenantId),
                new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.PlanType, Helpers.Constants.Strings.JwtClaims.CooperatePlan)
            });
        }

        /// <returns>Date converted to seconds since Unix epoch (Jan 1, 1970, midnight UTC).</returns>
        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round((date.ToUniversalTime() -
                               new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero))
                              .TotalSeconds);

        private static void ThrowIfInvalidOptions(JwtIssuerOptions options)
        {
            if (options == null) throw new ArgumentNullException(nameof(options));

            if (options.ValidFor <= TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(JwtIssuerOptions.ValidFor));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.SigningCredentials));
            }

            if (options.JtiGenerator == null)
            {
                throw new ArgumentNullException(nameof(JwtIssuerOptions.JtiGenerator));
            }
        }
    }

    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentityBasicPlan(string userName, string id, string PlanTypeId);
        ClaimsIdentity GenerateClaimsIdentityCooperatePlan(string userName, string id);
        ClaimsIdentity GenerateClaimsIdentityEnterprisePlan(string userName, string id);
    }
}
