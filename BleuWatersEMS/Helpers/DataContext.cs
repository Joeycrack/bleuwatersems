﻿using BleuWatersEMS.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BleuWatersEMS.Helpers
{
    public class DataContext : IdentityDbContext<IdentityUser>
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public DataContext(DbContextOptions<DataContext> options, IHttpContextAccessor httpContextAccessor)
            : base(options)
        {
            _httpContextAccessor = httpContextAccessor;
        }
        //public DbSet<User> Users { get; set; }
        
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Salutation> Salutations { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<GradeClass> GradeClasses { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<SubjectGradeClass> SubjectGradeClasses { get; set; }
        public DbSet<SubjectClass> SubjectClasses { get; set; }
        public DbSet<Term> Terms { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<StudentSubject> StudentSubjects { get; set; }
        public DbSet<StudentAttendance> StudentAttendances { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<RoleInterface> RoleInterfaces { get; set; }
        public DbSet<PlanTypeInterface> PlanTypeInterfaces { get; set; }
        public DbSet<PlanType> PlanTypes { get; set; }
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<Interface> Interfaces { get; set; }
        public DbSet<InterfaceChildren> InterfaceChildrens { get; set; }
        public DbSet<StudentResult> StudentResults { get; set; }
        public DbSet<ScoreGrade> ScoreGrades { get; set; }
        public DbSet<StudentAssessment> StudentAssessments { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Salutation>().ToTable("Salutation");
            modelBuilder.Entity<Country>().ToTable("Country");
            modelBuilder.Entity<State>().ToTable("State");
            modelBuilder.Entity<GradeClass>().ToTable("GradeClass");
            modelBuilder.Entity<Subject>().ToTable("Subject");
            modelBuilder.Entity<Teacher>().ToTable("Teacher");
            modelBuilder.Entity<Class>().ToTable("Class");
            modelBuilder.Entity<Subject>().ToTable("Subject");
            modelBuilder.Entity<Term>().ToTable("Term");
            modelBuilder.Entity<Session>().ToTable("Session");
            modelBuilder.Entity<Student>().ToTable("Student");
            modelBuilder.Entity<Guardian>().ToTable("Guardian");
            modelBuilder.Entity<Parent>().ToTable("Parent");
            modelBuilder.Entity<StudentAttendance>().ToTable("StudentAttendance");
            modelBuilder.Entity<Settings>().ToTable("Settings");
            modelBuilder.Entity<PlanType>().ToTable("PlanType");
            modelBuilder.Entity<Tenant>().ToTable("Tenant");
            modelBuilder.Entity<Interface>().ToTable("Interface");
            modelBuilder.Entity<InterfaceChildren>().ToTable("InterfaceChildren");
            modelBuilder.Entity<StudentResult>().ToTable("StudentResult");
            modelBuilder.Entity<ScoreGrade>().ToTable("ScoreGrade");
            modelBuilder.Entity<StudentAssessment>().ToTable("StudentAssessment");
            

            modelBuilder.Entity<SubjectClass>()
                .HasKey(o => new { o.ClassId, o.SubjectId });
            //modelBuilder.Entity<SubjectClass>()
            //    .HasOne(bc => bc.Subject)
            //    .WithMany(b => b.SubjectClasses)
            //    .HasForeignKey(c => c.SubjectId );
            //modelBuilder.Entity<SubjectClass>()
            //    .HasOne(bc => bc.Class)
            //    .WithMany(c => c.SubjectClasses)
            //    .HasForeignKey(bc => bc.ClassId);

            modelBuilder.Entity<SubjectGradeClass>()
                .HasKey(bc => new { bc.GradeClassId, bc.SubjectId });
            modelBuilder.Entity<SubjectGradeClass>()
                .HasOne(bc => bc.Subject)
                .WithMany(b => b.SubjectGradeClasses)
                .HasForeignKey(bc => bc.SubjectId);
            modelBuilder.Entity<SubjectGradeClass>()
                .HasOne(bc => bc.GradeClass)
                .WithMany(c => c.SubjectGradeClasses)
                .HasForeignKey(bc => bc.GradeClassId);

            modelBuilder.Entity<StudentSubject>()
               .HasKey(bc => new { bc.SubjectId, bc.TeacherId, bc.StudentId });
            modelBuilder.Entity<StudentSubject>()
                .HasOne(bc => bc.Subject)
                .WithMany(b => b.StudentSubjects)
                .HasForeignKey(bc => bc.SubjectId);
            modelBuilder.Entity<StudentSubject>()
                .HasOne(bc => bc.Student)
                .WithMany(c => c.StudentSubjects)
                .HasForeignKey(bc => bc.StudentId);
            modelBuilder.Entity<StudentSubject>()
                .HasOne(bc => bc.Teacher)
                .WithMany(c => c.StudentSubjects)
                .HasForeignKey(bc => bc.TeacherId);

            modelBuilder.Entity<RoleInterface>()
                .HasKey(bc => new { bc.InterfaceId, bc.RoleId });
            modelBuilder.Entity<RoleInterface>()
                .HasOne(bc => bc.Interface)
                .WithMany(b => b.RoleInterfaces)
                .HasForeignKey(bc => bc.InterfaceId);
            //modelBuilder.Entity<Ide>()
            //    //.HasOne(bc => bc.GradeClass)
            //    //.WithMany(c => c.SubjectGradeClasses)
            //    .HasForeignKey(bc => bc.GradeClassId);

            modelBuilder.Entity<PlanTypeInterface>()
                .HasKey(bc => new { bc.InterfaceId, bc.PlanTypeId });
            modelBuilder.Entity<PlanTypeInterface>()
                .HasOne(bc => bc.Interface)
                .WithMany(b => b.PlanTypeInterfaces)
                .HasForeignKey(bc => bc.InterfaceId);
            modelBuilder.Entity<PlanTypeInterface>()
                .HasOne(bc => bc.PlanType)
                .WithMany(c => c.PlanTypeInterfaces)
                .HasForeignKey(bc => bc.PlanTypeId);



            modelBuilder.Entity<IdentityUserClaim<string>>().HasKey(p => new { p.Id });
            modelBuilder.Entity<IdentityUserRole<string>>().HasKey(p => new { p.RoleId });
            modelBuilder.Entity<IdentityRole<string>>().HasKey(p => new { p.Id });
            modelBuilder.Entity<IdentityUserLogin<string>>().HasKey(p => new { p.UserId });
            modelBuilder.Entity<IdentityUserToken<string>>().HasKey(p => new { p.UserId });

        }
    }



}
