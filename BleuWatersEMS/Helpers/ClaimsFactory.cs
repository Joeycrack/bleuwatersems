﻿//using BleuWatersEMS.Models;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.Extensions.Options;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Claims;
//using System.Threading.Tasks;

//namespace BleuWatersEMS.Helpers
//{
   
//    public class ClaimsFactory 
//    {
//        private readonly UserManager<ApplicationUser> _userManager;
//        private readonly IJwtFactory _jwtFactory;
//        private readonly JwtIssuerOptions _jwtOptions;
//        public ClaimsFactory(UserManager<ApplicationUser> userManager, IJwtFactory jwtFactory, IOptions<JwtIssuerOptions> jwtOptions)
//        {
//            _userManager = userManager;
//            _jwtFactory = jwtFactory;
//            _jwtOptions = jwtOptions.Value;
//        }
//        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName, string password)
//        {
//            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
//                return await Task.FromResult<ClaimsIdentity>(null);

//            // get the user to verifty
//            var userToVerify = await _userManager.FindByNameAsync(userName);

//            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

//            // check the credentials
//            if (await _userManager.CheckPasswordAsync(userToVerify, password))
//            {
//                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
//            }

//            // Credentials are invalid, or account doesn't exist
//            return await Task.FromResult<ClaimsIdentity>(null);
//        }
//    }
//}
